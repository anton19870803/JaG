#include "plotcontrolwidget.h"
#include "ui_plotcontrolwidget.h"
#include <QDoubleValidator>
#include "global/global_definitions.h"

PlotControlWidget::PlotControlWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlotControlWidget)
{
    ui->setupUi(this);

    ui->gridLayout->setSpacing(0);
    ui->gridLayout->setMargin(0);
    ui->verticalLayout->setSpacing(0);
    ui->verticalLayout->setMargin(0);
    ui->verticalLayout_3->setSpacing(0);
    ui->verticalLayout_3->setMargin(0);
    ui->verticalLayout_4->setSpacing(0);
    ui->verticalLayout_4->setMargin(0);
    ui->verticalLayout_5->setSpacing(0);
    ui->verticalLayout_5->setMargin(0);
    ui->verticalLayout_6->setSpacing(0);
    ui->verticalLayout_6->setMargin(0);
    ui->verticalLayout_7->setSpacing(0);
    ui->verticalLayout_7->setMargin(0);
    ui->verticalLayout_8->setSpacing(0);
    ui->verticalLayout_8->setMargin(0);
    layout()->setSpacing(0);
    layout()->setMargin(0);


    ui->scaleTypeComboBox->addItem("Linear", PlotLayoutModel::ScaleType::Linear);
    ui->scaleTypeComboBox->addItem("Logarithmic", PlotLayoutModel::ScaleType::Logarithmic);

    ui->timeFormatComboBox->addItem("Number", QCPAxis::ltNumber);
    ui->timeFormatComboBox->addItem("DateTime", QCPAxis::ltDateTime);


    ui->legendLayoutWidget->addItem("Left",           PlotLayoutModel::LegendLocation::Left);
    ui->legendLayoutWidget->addItem("Right",          PlotLayoutModel::LegendLocation::Right);
    ui->legendLayoutWidget->addItem("TopSeparate",    PlotLayoutModel::LegendLocation::TopSeparate);
    ui->legendLayoutWidget->addItem("TopCombined",    PlotLayoutModel::LegendLocation::TopCombined);
    ui->legendLayoutWidget->addItem("BottomSeparate", PlotLayoutModel::LegendLocation::BottomSeparate);
    ui->legendLayoutWidget->addItem("BottomCombined", PlotLayoutModel::LegendLocation::BottomCombined);
    ui->legendLayoutWidget->addItem("Floating", PlotLayoutModel::LegendLocation::Floating);


    connect(ui->xLowMarginLineEdit, SIGNAL(editingFinished()),
            this, SLOT(treatXLowLimitRequest()));
    connect(ui->xHighMarginLineEdit, SIGNAL(editingFinished()),
            this, SLOT(treatXHighLimitRequest()));
    connect(ui->legendVisibilityCheckBox, SIGNAL(toggled ( bool )),
            this, SIGNAL(legendChangingVisibilityRequest(bool)));
    connect(ui->unitsVisibilityCheckBox, SIGNAL(toggled ( bool )),
            this, SIGNAL(dimensionChangingVisibilityRequest(bool)));
    connect(ui->xLabelLineEdit, SIGNAL(editingFinished()),
            this, SLOT(treatXLabelChanging()));
    connect(ui->titleLineEdit, SIGNAL(editingFinished()),
            this, SLOT(treatTitleChanging()));
    connect(ui->scaleTypeComboBox, SIGNAL(valueActivated(boost::any)),
            this, SLOT(treatScaleTypeChanging()));
    connect(ui->timeFormatComboBox, SIGNAL(valueActivated(boost::any)),
            this, SLOT(treatTimeAxisTypeChanging()));
    connect(ui->xTickRotationDoubleSpinBox, SIGNAL(valueChanged(double)),
            this, SLOT(treatXTickRotationChanging()));
    connect(ui->eventLabelRotationDoubleSpinBox, SIGNAL(valueChanged(double)),
            this, SLOT(treatCommandLabelRotationChanging()));
    connect(ui->legendLayoutWidget, SIGNAL(valueActivated(boost::any)),
            this, SLOT(treatLegendLayoutChanging()));

    //NOTE: To avoid signal valueChanged emitting on each keyboard button pressing. ValueChanged signal will be emitted
    //only after Return is pressed or if widget loses focus.
    ui->xTickRotationDoubleSpinBox->setKeyboardTracking(false);
    ui->eventLabelRotationDoubleSpinBox->setKeyboardTracking(false);

    QDoubleValidator *doubleValidator = new QDoubleValidator(this);
    ui->xLowMarginLineEdit->setValidator(doubleValidator);
    ui->xHighMarginLineEdit->setValidator(doubleValidator);

}

PlotControlWidget::~PlotControlWidget()
{
    delete ui;
}

void PlotControlWidget::setLayoutModel(PlotLayoutModel *layoutModel)
{
    mLayoutModel = layoutModel;
    initializeState();
}

void PlotControlWidget::initializeState()
{
    if (mLayoutModel != NULL) {

        auto xRange = mLayoutModel->getXRange();

        ui->xLowMarginLineEdit->setText(QString::number(std::get<0>(xRange), 'g', global::gDoubleToStringConversionPrecision));
        ui->xHighMarginLineEdit->setText(QString::number(std::get<1>(xRange), 'g', global::gDoubleToStringConversionPrecision));

        ui->legendVisibilityCheckBox->setCheckState(mLayoutModel->isLegendVisible() ?  Qt::Checked : Qt::Unchecked);
        ui->unitsVisibilityCheckBox->setCheckState(mLayoutModel->areUnitsVisible() ?  Qt::Checked : Qt::Unchecked);
        ui->legendFontSizeSpinBox->setValue(mLayoutModel->legendFont().pointSize());
        ui->dimensionFontSizeSpinBox->setValue(mLayoutModel->dimensionFont().pointSize());
        ui->xLabelLineEdit->setText(mLayoutModel->xLabel());
        ui->xLabelFontSizeSpinBox->setValue(mLayoutModel->xLabelFont().pointSize());
        ui->xAxisFontSizeSpinBox->setValue(mLayoutModel->xAxisFont().pointSize());
        ui->yAxisFontSizeSpinBox->setValue(mLayoutModel->yAxisFont().pointSize());
        ui->titleFontSizeSpinBox->setValue(mLayoutModel->titleFont().pointSize());
        ui->eventFontSizeSpinBox->setValue(mLayoutModel->commandFont().pointSize());
        ui->arrowedTextFontSizeSpinBox->setValue(mLayoutModel->arrowedTextFont().pointSize());

        ui->xTickRotationDoubleSpinBox->setValue(mLayoutModel->xTickRotation());
        ui->eventLabelRotationDoubleSpinBox->setValue(mLayoutModel->commandLabelRotation());

        ui->timeFormatComboBox->setCurrentValue(mLayoutModel->timeAxisType());
        ui->scaleTypeComboBox->setCurrentValue(mLayoutModel->scaleType());

        ui->legendLayoutWidget->setCurrentValue(mLayoutModel->legendLayout());
        ui->legendFrameVisibilityCheckBox->setChecked(mLayoutModel->isLegendFrameVisible());

        connect(ui->legendFontSizeSpinBox, SIGNAL(valueChanged(int)),
                mLayoutModel, SLOT(setLegendFontSize(int)));
        connect(ui->dimensionFontSizeSpinBox, SIGNAL(valueChanged(int)),
                mLayoutModel, SLOT(setDimensionFontSize(int)));
        connect(ui->xLabelFontSizeSpinBox, SIGNAL(valueChanged(int)),
                mLayoutModel, SLOT(setXLabelFontSize(int)));
        connect(ui->xAxisFontSizeSpinBox, SIGNAL(valueChanged(int)),
                mLayoutModel, SLOT(setXAxisFontSize(int)));
        connect(ui->yAxisFontSizeSpinBox, SIGNAL(valueChanged(int)),
                mLayoutModel, SLOT(setYAxisFontSize(int)));
        connect(ui->titleFontSizeSpinBox, SIGNAL(valueChanged(int)),
                mLayoutModel, SLOT(setTitleFontSize(int)));
        connect(ui->eventFontSizeSpinBox, SIGNAL(valueChanged(int)),
                mLayoutModel, SLOT(setTimeEventFontSize(int)));
        connect(ui->arrowedTextFontSizeSpinBox, SIGNAL(valueChanged(int)),
                mLayoutModel, SLOT(setArrowedTextFontSize(int)));

        connect(ui->legendFrameVisibilityCheckBox, SIGNAL(toggled(bool)),
                mLayoutModel, SLOT(setLegendFrameVisibility(bool)));

//        void layoutModelChanged();
//        void layoutAppearanceChanged();
        connect(mLayoutModel, SIGNAL(layoutAppearanceChanged()),
                this, SLOT(updateRanges()));
        connect(mLayoutModel, SIGNAL(layoutModelChanged()),
                this, SLOT(updateRanges()));
    }
}

void PlotControlWidget::treatXLowLimitRequest()
{
    ui->xLowMarginLineEdit->clearFocus();
    ui->xHighMarginLineEdit->clearFocus();
    emit settingXRangeRequest(
                ui->xLowMarginLineEdit->text().toDouble(),
                ui->xHighMarginLineEdit->text().toDouble()
                );
}


void PlotControlWidget::treatXHighLimitRequest()
{
    ui->xLowMarginLineEdit->clearFocus();
    ui->xHighMarginLineEdit->clearFocus();
    emit settingXRangeRequest(
                ui->xLowMarginLineEdit->text().toDouble(),
                ui->xHighMarginLineEdit->text().toDouble()
                );
}

void PlotControlWidget::treatXLabelChanging()
{
    ui->xLabelLineEdit->clearFocus();
    mLayoutModel->setXLabel(ui->xLabelLineEdit->text());
}

void PlotControlWidget::treatTitleChanging()
{
    ui->titleLineEdit->clearFocus();
    QString title = ui->titleLineEdit->text();
    title.replace(QString("\\n"), QString("\n"));
    mLayoutModel->setTitle(title);
}

void PlotControlWidget::treatScaleTypeChanging()
{
    if (mLayoutModel != NULL) {
        mLayoutModel->setScaleType(ui->scaleTypeComboBox->currentValue<PlotLayoutModel::ScaleType>());
    }
}

void PlotControlWidget::treatTimeAxisTypeChanging()
{
    if (mLayoutModel != NULL) {
        mLayoutModel->setTimeAxisType(ui->timeFormatComboBox->currentValue<QCPAxis::LabelType>());

    }
}

void PlotControlWidget::setMinWidth(int newMinWidth)
{
    setMinimumWidth(newMinWidth);
}

void PlotControlWidget::treatXTickRotationChanging()
{
    if (mLayoutModel != NULL) {
        mLayoutModel->setXTickRotaion(ui->xTickRotationDoubleSpinBox->value());
    }
}

void PlotControlWidget::treatCommandLabelRotationChanging()
{
    if (mLayoutModel != NULL) {
        mLayoutModel->setTimeEventLabelRotaion(ui->eventLabelRotationDoubleSpinBox->value());
    }
}

void PlotControlWidget::treatLegendLayoutChanging()
{
    if (mLayoutModel != NULL) {
        mLayoutModel->setLegendLayout(ui->legendLayoutWidget->currentValue<PlotLayoutModel::LegendLocation>());
    }
}

void PlotControlWidget::updateRanges()
{
    if (mLayoutModel) {
        auto xRange = mLayoutModel->getXRange();
        ui->xLowMarginLineEdit->setText(QString::number(std::get<0>(xRange), 'g', global::gDoubleToStringConversionPrecision));
        ui->xHighMarginLineEdit->setText(QString::number(std::get<1>(xRange), 'g', global::gDoubleToStringConversionPrecision));
    }
}

