#ifndef MULTIMODELCONTROLWIDGET_H
#define MULTIMODELCONTROLWIDGET_H

#include <QWidget>


#include "core/graphmodel.h"
#include <QVector>
#include "core/plotlayoutmodel.h"

namespace Ui {
class MultiModelControlWidget;
}

/*!
 \brief MultiModelControlWidget is  a widget to show and change properties of several graphs simultaneously.

*/
class MultiModelControlWidget : public QWidget
{
    Q_OBJECT

public:
    explicit MultiModelControlWidget(QWidget *parent = 0);
    ~MultiModelControlWidget();

    void setModelVector(QVector<GraphModel*> newModelVector);
    void setLayoutModel(PlotLayoutModel *model);

signals:
    void widgetIsHiding();

public slots:
    void initializeStateFromModels();

private slots:
    void treatVisibilityChanging();
    void treatLegendabilityChanging();
    void treatNameChanging();
    void treatDimensionChanging();
    void treatLineWidthChanging();
    void treatLineColorChanging();
//    void showLineColorDialog();
    void treatLineStyleChanging();
    void treatInterpolationChanging();

    void showMarkerIconChoiceDialog();
    void showBrushTextureChoiceDialog();

    void treatScatterShapeChanging();
    void treatScatterSizeChanging();
    void treatScatterDecimationChanging();
    void treatBrushStyleChanging();
    void treatBrushBasisChanging();
    void treatBrushColorChanging();
    void treatStringRepresentationChanging();
//    void showBrushColorDialog();


    void treatX0Changing();
    void treatKXChanging();
    void treatDXChanging();
    void treatY0Changing();
    void treatKYChanging();
    void treatDYChanging();

//    void treatXA0Changing();
//    void treatXA1Changing();
//    void treatYB0Changing();
//    void treatYB1Changing();

    void copyGraphProperties();
    void pasteGraphProperties();

public slots:
    void treatModelDeleting();

private:
    Ui::MultiModelControlWidget *ui;

    QVector<GraphModel*> mGraphVector;
    PlotLayoutModel *mLayoutModel;

    void disconnectFromModels();
    void connectToModels();

};

#endif // MULTIMODELCONTROLWIDGET_H
