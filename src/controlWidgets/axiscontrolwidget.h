#ifndef AXISCONTROLWIDGET_H
#define AXISCONTROLWIDGET_H

#include <QWidget>
#include "core/axismodel.h"

namespace Ui {
class AxisControlWidget;
}



/*!
 \brief AxisControlWidget is  a widget to show and change axis properties.

*/
class AxisControlWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AxisControlWidget(QWidget *parent = 0);
    ~AxisControlWidget();

    void setAxisModel(AxisModel*);
signals:
    void movementRequest(AxisModel*, AxisModel::AxisMovement);
    void settingYRangeRequest(double, double);
    void deletingRequest();

    void widgetIsHiding();

public slots:
    void treatMoveUpRequest();
    void treatMoveDownRequest();
    void treatSetYMarginRequest();
    void treatScalingTypeChanging();
    void initializeState();
    void treatDeletingRequest();
    void treatStretchFactorChanging();
    void treatAxisModelRemoval();

    void treatBackgroundStyleChanging();
    void treatBackgroundColorChanging();
    void showBackgroundTextureChoiceDialog();

private:
    Ui::AxisControlWidget *ui;
    AxisModel *mAxisModel;
};

#endif // AXISCONTROLWIDGET_H
