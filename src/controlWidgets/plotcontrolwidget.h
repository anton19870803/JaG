#ifndef PLOTCONTROLWIDGET_H
#define PLOTCONTROLWIDGET_H

#include <QWidget>
#include "core/plotlayoutmodel.h"

namespace Ui {
class PlotControlWidget;
}


/*!
 \brief AxisControlWidget is  a widget to show and change properties that concern with properties of all axis (not one particular).

*/
class PlotControlWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PlotControlWidget(QWidget *parent = 0);
    ~PlotControlWidget();

    void setLayoutModel(PlotLayoutModel*);

public slots:
    void initializeState();
    void treatXLowLimitRequest();
    void treatXHighLimitRequest();
    void treatXLabelChanging();
    void treatTitleChanging();
    void treatScaleTypeChanging();
    void treatTimeAxisTypeChanging();
    void setMinWidth(int);
    void treatXTickRotationChanging();
    void treatCommandLabelRotationChanging();
    void treatLegendLayoutChanging();

    void updateRanges();

signals:
    void settingXRangeRequest(double, double);
    void legendChangingVisibilityRequest(bool);
    void dimensionChangingVisibilityRequest(bool);



private:
    Ui::PlotControlWidget *ui;
    PlotLayoutModel *mLayoutModel;
};

#endif // PLOTCONTROLWIDGET_H
