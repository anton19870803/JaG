#include "axiscontrolwidget.h"
#include "ui_axiscontrolwidget.h"
#include <QDoubleValidator>
#include <boost/any.hpp>
#include "commonWidgets/imagechoice/brushpatternchoicedialog.h"

AxisControlWidget::AxisControlWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AxisControlWidget), mAxisModel(NULL)
{
    ui->setupUi(this);

    ui->gridLayout->setSpacing(0);
    ui->gridLayout->setMargin(0);
    ui->verticalLayout->setSpacing(0);
    ui->verticalLayout->setMargin(0);
    layout()->setSpacing(0);
    layout()->setMargin(0);





    connect(ui->yLowMarginLineEdit, SIGNAL(editingFinished()),
            this, SLOT(treatSetYMarginRequest()));
    connect(ui->yHighMarginLineEdit, SIGNAL(editingFinished()),
            this, SLOT(treatSetYMarginRequest()));
    connect(ui->scaleTypeComboBox, SIGNAL(valueActivated(boost::any)),
            this, SLOT(treatScalingTypeChanging()));
    connect(ui->stretchFactorDoubleSpinBox, SIGNAL(valueChanged(double)),
            this, SLOT(treatStretchFactorChanging()));
    connect(ui->backgroundColorChoiceWidget, SIGNAL(currentColorChanged(QColor)),
            this, SLOT(treatBackgroundColorChanging()));
    connect(ui->backgroundStyleComboBox, SIGNAL(valueActivated(boost::any)),
            this, SLOT(treatBackgroundStyleChanging()));
    connect(ui->backgroundTextureChoiceToolButton, SIGNAL(clicked(bool)),
            this, SLOT(showBackgroundTextureChoiceDialog()));

    //NOTE: To avoid signal valueChanged emitting on each keyboard button pressing. ValueChanged signal will be emitted
    //only after Return is pressed or if widget loses focus.
    ui->stretchFactorDoubleSpinBox->setKeyboardTracking(false);

    ui->scaleTypeComboBox->addItem("Linear"     ,  AxisModel::ScaleType::Linear     );
    ui->scaleTypeComboBox->addItem("Logarithmic",  AxisModel::ScaleType::Logarithmic);


    ui->backgroundStyleComboBox->addItem("NoBrush", Qt::NoBrush);
    ui->backgroundStyleComboBox->addItem("Solid", Qt::SolidPattern);
    ui->backgroundStyleComboBox->addItem("Dense1", Qt::Dense1Pattern);
    ui->backgroundStyleComboBox->addItem("Dense2", Qt::Dense2Pattern);
    ui->backgroundStyleComboBox->addItem("Dense3", Qt::Dense3Pattern);
    ui->backgroundStyleComboBox->addItem("Dense4", Qt::Dense4Pattern);
    ui->backgroundStyleComboBox->addItem("Dense5", Qt::Dense5Pattern);
    ui->backgroundStyleComboBox->addItem("Dense6", Qt::Dense6Pattern);
    ui->backgroundStyleComboBox->addItem("Dense7", Qt::Dense7Pattern);
    ui->backgroundStyleComboBox->addItem("Hor", Qt::HorPattern);
    ui->backgroundStyleComboBox->addItem("Ver", Qt::VerPattern);
    ui->backgroundStyleComboBox->addItem("Cross", Qt::CrossPattern);
    ui->backgroundStyleComboBox->addItem("BDiag", Qt::BDiagPattern);
    ui->backgroundStyleComboBox->addItem("FDiag", Qt::FDiagPattern);
    ui->backgroundStyleComboBox->addItem("DiagCross", Qt::DiagCrossPattern);
    ui->backgroundStyleComboBox->addItem("TexturePattern", Qt::TexturePattern);

     QDoubleValidator *doubleValidator = new QDoubleValidator(this);
     ui->yHighMarginLineEdit->setValidator(doubleValidator);
     ui->yLowMarginLineEdit->setValidator(doubleValidator);
}

AxisControlWidget::~AxisControlWidget()
{
    delete ui;
}

void AxisControlWidget::setAxisModel(AxisModel * model)
{
    if (mAxisModel != model) {
        mAxisModel = model;
        connect(mAxisModel, SIGNAL(deletingStateIsOccuring()),
                this, SLOT(treatAxisModelRemoval()));

//        connect(mAxisModel, SIGNAL(destroyed(QObject*)),
//                this, SLOT(treatAxisModelRemoval()));
        initializeState();
    }
}

void AxisControlWidget::treatMoveUpRequest()
{
    if (mAxisModel != NULL) {
        mAxisModel->treatMovementRequest(mAxisModel,AxisModel::AxisMovement::Up);
    }
//    emit movementRequest(mAxisModel,AxisModel::AxisMovement::Up);
}

void AxisControlWidget::treatMoveDownRequest()
{
    if (mAxisModel != NULL) {
        mAxisModel->treatMovementRequest(mAxisModel,AxisModel::AxisMovement::Down);
    }
//    emit movementRequest(mAxisModel,AxisModel::AxisMovement::Down);
}

void AxisControlWidget::treatSetYMarginRequest()
{
    ui->yLowMarginLineEdit->clearFocus();
    ui->yHighMarginLineEdit->clearFocus();
    if (mAxisModel != NULL) {
        mAxisModel->setYRange(
                              ui->yLowMarginLineEdit->text().toDouble(),
                              ui->yHighMarginLineEdit->text().toDouble()
                             );
    }
//    try {
//    emit settingYRangeRequest(
//                              ui->yLowMarginLineEdit->text().toDouble(),
//                              ui->yHighMarginLineEdit->text().toDouble()
//                              );
//    } catch (...) {

//    }

}

void AxisControlWidget::treatScalingTypeChanging()
{
    if (mAxisModel != NULL) {
        mAxisModel->setScaleType(ui->scaleTypeComboBox->currentValue<AxisModel::ScaleType>());
    }
}

void AxisControlWidget::initializeState()
{
    if (mAxisModel != NULL) {
        std::tuple<double, double>  yRange = mAxisModel->getYRange();
        ui->yLowMarginLineEdit->setText(QString::number(std::get<0>(yRange)));
        ui->yHighMarginLineEdit->setText(QString::number(std::get<1>(yRange)));
        ui->stretchFactorDoubleSpinBox->setValue(mAxisModel->stretchFactor());
        ui->scaleTypeComboBox->setCurrentValue(mAxisModel->scaleType());

        auto background = mAxisModel->backgroundBrush();
        ui->backgroundStyleComboBox->setCurrentValue(background.style());
        ui->backgroundColorChoiceWidget->setColor(background.color());

    }
}

void AxisControlWidget::treatDeletingRequest()
{
    if (mAxisModel != NULL) {
        mAxisModel->treatDeletingRequest();
    }
}

void AxisControlWidget::treatStretchFactorChanging()
{
    if (mAxisModel != NULL) {
        mAxisModel->setStretchFactor(ui->stretchFactorDoubleSpinBox->value());
    }
}

void AxisControlWidget::treatAxisModelRemoval()
{
    if (mAxisModel != NULL) {
        AxisModel* model = qobject_cast<AxisModel*>(sender());
        if (model && model == mAxisModel) {
            mAxisModel = NULL;
            hide();
            emit widgetIsHiding();
        }
    }
}

void AxisControlWidget::treatBackgroundStyleChanging()
{
    if (mAxisModel != NULL) {
        mAxisModel->setBackgroundStyle(ui->backgroundStyleComboBox->currentValue<Qt::BrushStyle>());
        ui->backgroundStyleComboBox->clearFocus();
    }
}

void AxisControlWidget::treatBackgroundColorChanging()
{
    if (mAxisModel != NULL) {
        mAxisModel->setBackgroundColor(ui->backgroundColorChoiceWidget->color());
        ui->backgroundColorChoiceWidget->clearFocus();
    }
}

void AxisControlWidget::showBackgroundTextureChoiceDialog()
{
    if (mAxisModel != NULL) {
        BrushPatternChoiceDialog choiceWidget;
        if (choiceWidget.exec() == QDialog::Accepted) {
            mAxisModel->setBackgroundTexture(choiceWidget.currentPixmap());
            ui->backgroundStyleComboBox->setCurrentValue(Qt::TexturePattern);
        }
        ui->backgroundTextureChoiceToolButton->clearFocus();
    }
}
