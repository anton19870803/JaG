#ifndef GLOBAL_DEFINITIONS_H
#define GLOBAL_DEFINITIONS_H



#include <QVector>
#include <QList>
#include <QColor>
#include <QString>
#include "core/graphmodel.h"
#include "datasource/simpledatasource.h"
#include "event/timeevent.h"
#include <QMap>
#include <core/plotlayoutmodel.h>


class JagMainWindow;
class EventSource;


namespace global {


extern QVector<QString> gFavouriteParameters;
extern QVector<QString> gHateParameters;

extern int gPreviousSessionWidth;
extern int gPreviousSessionHeight;

extern int gPreviousSecProcDialogWidth;
extern int gPreviousSecProcDialogHeight;

extern int gPreviousSessionXCoord;
extern int gPreviousSessionYCoord;
extern bool gIsJaGMainWindowInitialMoveEnabled;

extern int gDataDialogWidth;
extern int gDataDialogHeight;
extern QVector<QString> gParametersSearchClipboard;
extern bool gAutoParametersFilteringDuringSearchIsEnabled;
extern QString gAutoParametersFilteringDuringSearchPattern;

extern QString gDefaultDirectory;
extern QString gApplicationStartTime;

bool areNamesRelated(const QString &name1, const QString &name2);


extern QVector<QString> gPrevSecondProcesFunctionVector;
const int gPrevSecondProcesFunctionVectorMaxSize = 8;
extern QVector<const GraphModel*> gCurrGraphVector;

extern bool gWheelZoomEnabled;
extern QString gDimensionlessParameterUnitLabel;

extern QStringList gParametersForDimensionList;
extern QStringList gUnitsForUnitList;

extern QString gParameterNameSeparator;
QString getPrameterNameFromComplexName(const QString &complexName);
QString getFileNameFromPath(const QString &complexName);

extern bool gIsAutoNanTo0ConversionEnabled;

extern quint16 gExternalDataSourcePortNumber;

extern bool gIsBootTipShowingEnabled;

extern JagMainWindow *gJagMainWindowPointer;
extern int gDoubleToStringConversionPrecision;

extern QList<QColor> gColorSceme;

extern bool gAreExtraWidgetsVisibleInDataChoiseDialog;

extern int gTracerSize;
extern double gEventLineWidth;

extern SimpleDataSource *gGlobalDataSource;
extern EventSource *gGlobalEventSource;

extern bool gIsExtraSecondaryProcessingFunctionsEnabled;
extern bool gFindStringErasingTimerEnabled;
extern double gFindStringErasingTimerInterval;
extern QMap<QString, QStringList> gPreviouslySavedParametersNames;  //QMap<stringlist name, stringlist of saved names>

extern double gXTickSpacing;
extern double gYTickSpacing;

extern int gGraphCaptureDistance;

extern QString gApplicationGUIStyle;

extern bool gShowDataSourceLoadingProgress;
extern bool gSearchByDescriptionIsEnabled;

extern std::shared_ptr<GraphModel> gGlobalGraphModel;

extern bool gAutoDimFillOnlyForEmpty;

extern bool gIsDimensionCopyable;
extern bool gIsLineColorCopyable;
extern bool gIsLineWidthCopyable;
extern bool gIsLineStyleCopyable;
extern bool gIsInterpolationCopyable;
extern bool gIsScatterShapeCopyable;
extern bool gIsScatterSizeCopyable;
extern bool gIsScatterDecimationCopyable;
extern bool gIsBrushStyleCopyable;
extern bool gIsBrushColorCopyable;
extern bool gIsBrushBasisCopyable;
extern bool gIsTransformationCopyable;
extern bool gIsFiltersCopyable;
extern bool gIsStringRepresentationCopyable;


extern bool gShowDataSourceEditingDialog;

extern double gGridPenWidth;
extern double gSubGridPenWidth;
extern double gAxisPenWidth;

extern double gWidthDeviationForExternalPlotting;

extern bool gIsDataSourceCheckingStrict;
extern int gErrorsListCapacity;


extern double gLineWidthDeviationForExternalPlotting;
extern bool gRussianLanguage;

enum class ParamNamingPolicy {
                                AlwaysAddAlias,
                                AddAliasForMultSources,
                                NeverAddAlias,
                            };
extern ParamNamingPolicy gParameterNamingPolicy;


extern QColor gDefaultPlotBackgroundColor;
extern QColor gDefaultGridColor;
extern QColor gDefaultSubGridColor;
extern QColor gDefaultAxisRectColor;
extern QColor gDefaultPlotFontColor;
extern QColor gDefaultPlotGraphicalPrimitivesColor;


class ColorScheme {
public:
    enum class Scheme {
        System,
        DarkScheme,
        DarkOrangeScheme,
    };

    ColorScheme(): mScheme(Scheme::System) { }
    ColorScheme& operator=(Scheme);
    Scheme scheme() const { return mScheme; }

private:
    Scheme mScheme;


};

extern ColorScheme gDefaultColorScheme;

void setDefaultPreferences();
void setDefaultColors();


extern QList<QColor> gGraphColorList;
extern PlotLayoutModel::LegendLocation gLegendLocation;

extern bool gDrawValuesInMeasuringMode;
}



#endif // GLOBAL_DEFINITIONS_H
