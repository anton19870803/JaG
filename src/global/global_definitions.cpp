#include "global/global_definitions.h"
#include <QApplication>

namespace global {

QVector<QString> gFavouriteParameters;
QVector<QString> gHateParameters;

int gPreviousSessionWidth;
int gPreviousSessionHeight;

int gPreviousSessionXCoord = 10;
int gPreviousSessionYCoord = 10;
bool gIsJaGMainWindowInitialMoveEnabled;

int gPreviousSecProcDialogWidth = 400;
int gPreviousSecProcDialogHeight = 600;


int gDataDialogWidth = -1;
int gDataDialogHeight = -1;

QVector<QString> gParametersSearchClipboard;


QString gDefaultDirectory;


QString gApplicationStartTime;


QVector<const GraphModel*> gCurrGraphVector;

QVector<QString> gPrevSecondProcesFunctionVector;

bool areNamesRelated(const QString &name1, const QString &name2)
{

    if (getPrameterNameFromComplexName(name1) == getPrameterNameFromComplexName(name2))
        return true;
    else
        return false;
}

bool gWheelZoomEnabled;
QString gDimensionlessParameterUnitLabel;

QStringList gParametersForDimensionList;
QStringList gUnitsForUnitList;


QString gParameterNameSeparator = "::";


QString getPrameterNameFromComplexName(const QString &complexName)
{
    int	index = complexName.lastIndexOf(global::gParameterNameSeparator);
    if (index == -1)
        return complexName;
    QString ret = complexName;
    ret.remove(0, index + global::gParameterNameSeparator.size());
    return ret;
}


bool gIsAutoNanTo0ConversionEnabled = false;

quint16 gExternalDataSourcePortNumber = 33333;
bool gAutoParametersFilteringDuringSearchIsEnabled = false;
QString gAutoParametersFilteringDuringSearchPattern = ".*";


QString getFileNameFromPath(const QString &fileName)
{
    QString outFileName = fileName;
    int index;
    if ((index = outFileName.lastIndexOf("/")) != -1) {
        outFileName.remove(0, index + 1);
    }
    if ((index = outFileName.lastIndexOf("\\")) != -1) {
        outFileName.remove(0, index + 1);
    }
    return outFileName;
}

bool gIsBootTipShowingEnabled = true;
bool gIsExtraSecondaryProcessingFunctionsEnabled = true;

JagMainWindow *gJagMainWindowPointer = NULL;

bool gAutoDimFillOnlyForEmpty;

int gDoubleToStringConversionPrecision;

QList<QColor> gColorSceme = {
                            Qt::blue,
                            Qt::green,
                            Qt::red,
                            Qt::magenta,
                            Qt::black,
                            Qt::darkGreen,
                            Qt::darkYellow,
                            Qt::darkMagenta,
                            Qt::darkCyan,
                            Qt::darkRed,
                            Qt::darkBlue,
                            Qt::cyan,
                            Qt::yellow,
                            Qt::darkGray,
                            Qt::gray,
                            Qt::lightGray,
                            Qt::white,
//                            QColor(0,0,0,0)
                            };



bool gAreExtraWidgetsVisibleInDataChoiseDialog = true;
double gEventLineWidth;

int gTracerSize;


SimpleDataSource *gGlobalDataSource = NULL;
EventSource *gGlobalEventSource = NULL;

bool gFindStringErasingTimerEnabled = true;
double gFindStringErasingTimerInterval;


QMap<QString, QStringList> gPreviouslySavedParametersNames;  //QMap<stringList name, stringList of saved names>

double gXTickSpacing;
double gYTickSpacing;

int gGraphCaptureDistance;


bool gShowDataSourceLoadingProgress;

bool gSearchByDescriptionIsEnabled = true;

std::shared_ptr<GraphModel> gGlobalGraphModel = NULL;

bool gIsDimensionCopyable;
bool gIsLineColorCopyable;
bool gIsLineWidthCopyable;
bool gIsLineStyleCopyable;
bool gIsInterpolationCopyable ;
bool gIsScatterShapeCopyable;
bool gIsScatterSizeCopyable ;
bool gIsScatterDecimationCopyable ;
bool gIsBrushStyleCopyable;
bool gIsBrushColorCopyable;
bool gIsBrushBasisCopyable;
bool gIsTransformationCopyable;
bool gIsFiltersCopyable;
bool gIsStringRepresentationCopyable;

bool gShowDataSourceEditingDialog;

double gGridPenWidth;
double gSubGridPenWidth;
double gAxisPenWidth;
double gWidthDeviationForExternalPlotting;


QString gApplicationGUIStyle;

bool gIsDataSourceCheckingStrict;


int gErrorsListCapacity;




double gLineWidthDeviationForExternalPlotting;
bool gRussianLanguage;

ParamNamingPolicy gParameterNamingPolicy;

QColor gDefaultPlotBackgroundColor;
QColor gDefaultGridColor;
QColor gDefaultSubGridColor;
QColor gDefaultAxisRectColor;
QColor gDefaultPlotFontColor;
QColor gDefaultPlotGraphicalPrimitivesColor;

ColorScheme gDefaultColorScheme;



QList<QColor> gGraphColorList;

PlotLayoutModel::LegendLocation gLegendLocation;

bool gDrawValuesInMeasuringMode;

ColorScheme& ColorScheme::operator=(Scheme scheme) {
    switch (scheme) {
        case Scheme::System:
            qApp->setStyleSheet("");
            break;
        case Scheme::DarkScheme:
            {
            QFile f(":qdarkstyle/style.qss");
            if (!f.exists())
            {
                printf("Unable to set stylesheet, file not found\n");
            }
            else
            {
                f.open(QFile::ReadOnly | QFile::Text);
                QTextStream ts(&f);
                qApp->setStyleSheet(ts.readAll());
            }
            }
            break;
        case Scheme::DarkOrangeScheme:
            {
            QFile f(":darkorange/darkorange.qss");
            if (!f.exists())
            {
                printf("Unable to set stylesheet, file not found\n");
            }
            else
            {
                f.open(QFile::ReadOnly | QFile::Text);
                QTextStream ts(&f);
                qApp->setStyleSheet(ts.readAll());
            }
            }
            break;
    }
    mScheme = scheme;
    return *this;
}




}


void global::setDefaultPreferences()
{

    using namespace global;
    gIsJaGMainWindowInitialMoveEnabled = false;

#ifdef __linux
gDefaultDirectory = "/";
#else
gDefaultDirectory = "C:/";
#endif

    gWheelZoomEnabled = true;

    gDimensionlessParameterUnitLabel = "";

    gAutoDimFillOnlyForEmpty = true;

    gDoubleToStringConversionPrecision = 6;

    gEventLineWidth = 3.0;

    gTracerSize = 10;

    gFindStringErasingTimerInterval = 4.;

    gXTickSpacing = 1.5;
    gYTickSpacing = 1.5;

    gGraphCaptureDistance = 6;

    gShowDataSourceLoadingProgress = true;

    gIsDimensionCopyable = false;
    gIsLineColorCopyable = false;
    gIsLineWidthCopyable = true;
    gIsLineStyleCopyable = true;
    gIsInterpolationCopyable  = true;
    gIsScatterShapeCopyable = true;
    gIsScatterSizeCopyable  = true;
    gIsScatterDecimationCopyable  = true;
    gIsBrushStyleCopyable = false;
    gIsBrushColorCopyable =  false;
    gIsBrushBasisCopyable =  false;
    gIsTransformationCopyable = true;
    gIsFiltersCopyable = true;
    gIsStringRepresentationCopyable = false;

    gShowDataSourceEditingDialog = true;

    gGridPenWidth = 1.1;
    gSubGridPenWidth = 1.1;
    gAxisPenWidth = 1.6;
    gWidthDeviationForExternalPlotting = 0.4;
    gLineWidthDeviationForExternalPlotting = 0.6;

#ifdef __linux
gApplicationGUIStyle = "GTK+";
#else
gApplicationGUIStyle = "Windows";
#endif

    gIsDataSourceCheckingStrict = false;

    gErrorsListCapacity = 50;
    gRussianLanguage = false;

    gDrawValuesInMeasuringMode = true;

    gParameterNamingPolicy = ParamNamingPolicy::AlwaysAddAlias;

    gLegendLocation = PlotLayoutModel::LegendLocation::Floating;

    global::setDefaultColors();

    gDefaultColorScheme = ColorScheme::Scheme::System;

    gGraphColorList.clear();
    gGraphColorList  << Qt::blue
                     << Qt::darkGreen
                     << Qt::red
                     << Qt::magenta
                     << Qt::black
                     << Qt::green
                     << Qt::darkYellow
                     << Qt::darkMagenta
                     << Qt::darkCyan
                     << Qt::darkYellow
                     << Qt::darkGray
                     << Qt::cyan
                     << Qt::yellow;
}





void global::setDefaultColors()
{
    using namespace global;
    gDefaultPlotBackgroundColor = Qt::white;
    gDefaultGridColor = Qt::darkGray;
    gDefaultSubGridColor = Qt::darkGray;
    gDefaultAxisRectColor = Qt::black;
    gDefaultPlotFontColor = Qt::black;
    gDefaultPlotGraphicalPrimitivesColor = Qt::black;
}
