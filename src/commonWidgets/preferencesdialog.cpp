#include "preferencesdialog.h"
#include "ui_preferencesdialog.h"
#include "global/global_definitions.h"
#include <QFileDialog>
#include <QStyleFactory>
#include <QMessageBox>

PreferencesDialog::PreferencesDialog(Preferences *preferences, QWidget *parent) :
    QDialog(parent), ui(new Ui::PreferencesDialog),
    mPreferences(preferences)
{
    ui->setupUi(this);

    ui->languageComboBox->addItem("English");
    ui->languageComboBox->addItem("Russian");

    mLineInterpolationMap.push_back(std::tuple<QCPGraph::LineStyle, QString>
                                (QCPGraph::LineStyle::lsLine, "Line"));
    mLineInterpolationMap.push_back(std::tuple<QCPGraph::LineStyle, QString>
                                (QCPGraph::LineStyle::lsStepLeft, "Step Left"));
    mLineInterpolationMap.push_back(std::tuple<QCPGraph::LineStyle, QString>
                                (QCPGraph::LineStyle::lsStepRight, "Step Right"));
    mLineInterpolationMap.push_back(std::tuple<QCPGraph::LineStyle, QString>
                                (QCPGraph::LineStyle::lsStepCenter, "Step Center"));
    mLineInterpolationMap.push_back(std::tuple<QCPGraph::LineStyle, QString>
                                (QCPGraph::LineStyle::lsImpulse, "Impulse"));
    mLineInterpolationMap.push_back(std::tuple<QCPGraph::LineStyle, QString>
                                (QCPGraph::LineStyle::lsNone, "None"));
    for (auto it = mLineInterpolationMap.begin(); it != mLineInterpolationMap.end(); ++it) {
        ui->lineInterpolationComboBox->addItem(std::get<1>(*it));
    }


    mParamNamingPolicyMap.push_back(std::tuple<global::ParamNamingPolicy, QString>
                                    (global::ParamNamingPolicy::AlwaysAddAlias, "Always add alias"));
    mParamNamingPolicyMap.push_back(std::tuple<global::ParamNamingPolicy, QString>
                                    (global::ParamNamingPolicy::AddAliasForMultSources, "Add alias for multiple sources"));
    mParamNamingPolicyMap.push_back(std::tuple<global::ParamNamingPolicy, QString>
                                    (global::ParamNamingPolicy::NeverAddAlias, "Never add alias"));
    for (auto it = mParamNamingPolicyMap.begin(); it != mParamNamingPolicyMap.end(); ++it) {
        ui->parameterNamingPolicyComboBox->addItem(std::get<1>(*it));
    }


    ui->colorSchemeWidget->addItem("System",               global::ColorScheme::Scheme::System);
    ui->colorSchemeWidget->addItem("Dark scheme",          global::ColorScheme::Scheme::DarkScheme);
    ui->colorSchemeWidget->addItem("Dark orange scheme",   global::ColorScheme::Scheme::DarkOrangeScheme);


    ui->legendLocationWidget->addItem("Left",              PlotLayoutModel::LegendLocation::Left);
    ui->legendLocationWidget->addItem("Right",             PlotLayoutModel::LegendLocation::Right);
    ui->legendLocationWidget->addItem("TopSeparate",       PlotLayoutModel::LegendLocation::TopSeparate);
    ui->legendLocationWidget->addItem("TopCombined",       PlotLayoutModel::LegendLocation::TopCombined);
    ui->legendLocationWidget->addItem("BottomSeparate",    PlotLayoutModel::LegendLocation::BottomSeparate);
    ui->legendLocationWidget->addItem("BottomCombined",    PlotLayoutModel::LegendLocation::BottomCombined);
    ui->legendLocationWidget->addItem("Floating",          PlotLayoutModel::LegendLocation::Floating);




    QStyleFactory styleFactory;
    ui->guiStyleComboBox->addItems(styleFactory.keys());

    initializeStateFromPreferences();

    connect(ui->applyButton, SIGNAL(clicked()),
            this, SLOT(okRequest()));
    connect(ui->cancelButton, SIGNAL(clicked()),
            this, SLOT(cancelRequest()));
    connect(ui->applyAndSaveButton, SIGNAL(clicked()),
            this, SLOT(okAndSaveRequest()));
    connect(ui->defaultDirectoryPushButton, SIGNAL(clicked()),
            this, SLOT(setChooseDefaultDirectory()));
    connect(ui->defaultPushButton, SIGNAL(clicked()),
            this, SLOT(setDefaultPreferences()));

    connect(ui->dataSourceStrictCheckingCheckBox, SIGNAL(clicked()),
            this, SLOT(errorNumberEnabilityChanged()));



}

PreferencesDialog::~PreferencesDialog()
{
    delete ui;
}

void PreferencesDialog::okRequest()
{
    applyChangesToPreferences();
    hide();
}

void PreferencesDialog::cancelRequest()
{
    initializeStateFromPreferences();
    hide();
}

void PreferencesDialog::okAndSaveRequest()
{
    okRequest();
    emit preferenceSavingRequest();
}

void PreferencesDialog::setChooseDefaultDirectory()
{
    QString newDir = QFileDialog::getExistingDirectory(this,
                                                       "Choose default directory for JaG",
                                                       ui->defaultDirectoryLineEdit->text()
                                                       );
    if (newDir != "")
        ui->defaultDirectoryLineEdit->setText(newDir);
}

void PreferencesDialog::setDefaultPreferences()
{
    *mPreferences = Preferences();
    global::setDefaultPreferences();
    initializeStateFromPreferences();
}

void PreferencesDialog::errorNumberEnabilityChanged()
{
    ui->errorsNumberSpinBox->setEnabled(!ui->dataSourceStrictCheckingCheckBox->isChecked());
}

void PreferencesDialog::initializeStateFromPreferences()
{
    using namespace global;
    ui->languageComboBox->setCurrentIndex(gRussianLanguage ? 1 : 0);

    for (int i = 0; i < ui->guiStyleComboBox->count(); ++i) {
        if (gApplicationGUIStyle == ui->guiStyleComboBox->itemText(i)) {
            ui->guiStyleComboBox->setCurrentIndex(i);
            break;
        }
    }

    ui->xTickSpacingDoubleSpinBox->setValue(gXTickSpacing);
    ui->yTickSpacingDoubleSpinBox->setValue(gYTickSpacing);

    ui->graphWidthIncreaseDoubleSpinBox->setValue(gLineWidthDeviationForExternalPlotting);

    ui->doubleToStringPrecisonSpinBox->setValue(gDoubleToStringConversionPrecision);
    ui->lineWidthDoubleSpinBox->setValue(mPreferences->mLineWidth);

    ui->scatterSizeDoubleSpinBox->setValue(mPreferences->mScatterSize);
    ui->scatterDecimationSpinBox->setValue(mPreferences->mScatterDecimation);

    ui->erasingTimerDoubleSpinBox->setValue(gFindStringErasingTimerInterval);

    ui->graphCaptureDistanceSpinBox->setValue(gGraphCaptureDistance);

    auto interpolation = mPreferences->mLineInterpolation;
    for (auto i = 0; i != mLineInterpolationMap.size(); i++)
        if (std::get<0>(mLineInterpolationMap[i]) == interpolation) {
            ui->lineInterpolationComboBox->setCurrentIndex(i);
            break;
        }

    auto namingPolicy = global::gParameterNamingPolicy;
    for (auto i = 0; i != mParamNamingPolicyMap.size(); i++)
        if (std::get<0>(mParamNamingPolicyMap[i]) == namingPolicy) {
            ui->parameterNamingPolicyComboBox->setCurrentIndex(i);
            break;
        }


    ui->xLabelLineEdit->setText(mPreferences->mXLabel);
    ui->xLabelFontComboBox->setCurrentFont(mPreferences->mXLabelFont);
    ui->xLabelFontSizeSpinBox->setValue(mPreferences->mXLabelFont.pointSize());

    ui->xAxisTickLabelsFontComboBox->setCurrentFont(mPreferences->mXAxisFont);
    ui->xAxisTickLabelFontSizeSpinBox->setValue(mPreferences->mXAxisFont.pointSize());

    ui->yAxisTickLabelsFontComboBox->setCurrentFont(mPreferences->mYAxisFont);
    ui->yAxisTickLabelFontSizeSpinBox->setValue(mPreferences->mYAxisFont.pointSize());

    ui->restorePositionCheckBox->setChecked(gIsJaGMainWindowInitialMoveEnabled);

    ui->legendVisibilityCheckBox->setCheckState(mPreferences->mLegendVisibility ?  Qt::Checked : Qt::Unchecked);
    ui->legendFontComboBox->setCurrentFont(mPreferences->mLegendFont);
    ui->legendFontSizeSpinBox->setValue(mPreferences->mLegendFont.pointSize());

    ui->titleFontComboBox->setCurrentFont(mPreferences->mTitleFont);
    ui->titleFontSizeSpinBox->setValue(mPreferences->mTitleFont.pointSize());

    ui->unitsVisibilityCheckBox->setCheckState(mPreferences->mUnitsVisibility ?  Qt::Checked : Qt::Unchecked);
    ui->dimensionFontComboBox->setCurrentFont(mPreferences->mDimensionFont);
    ui->dimensionFontSizeSpinBox->setValue(mPreferences->mDimensionFont.pointSize());

    ui->commandFontComboBox->setCurrentFont(mPreferences->mEventFont);
    ui->eventFontSizeSpinBox->setValue(mPreferences->mEventFont.pointSize());

    ui->arrowedTextFontComboBox->setCurrentFont(mPreferences->mArrowedTextFont);
    ui->arrowedTextFontSizeSpinBox->setValue(mPreferences->mArrowedTextFont.pointSize());

    ui->legendFrameVisibilityCheckBox->setChecked(mPreferences->mIsLegendFrameVisible);

    ui->wheelZoomEnableCheckBox->setCheckState(gWheelZoomEnabled ?  Qt::Checked : Qt::Unchecked);

    foreach  (QString favourite, gFavouriteParameters)
        ui->favouritePlainTextEdit->appendPlainText(favourite);
    foreach  (QString hate, gHateParameters)
        ui->hatePlainTextEdit->appendPlainText(hate);

    ui->defaultDirectoryLineEdit->setText(gDefaultDirectory);

    ui->dimensionlessUnitLineEdit->setText(gDimensionlessParameterUnitLabel);

    ui->tracerSizeSpinBox->setValue(gTracerSize);
    ui->eventLineWidthDoubleSpinBox->setValue(gEventLineWidth);

    ui->progressShowingCheckBox->setChecked(gShowDataSourceLoadingProgress);



    ui->dataSourceStrictCheckingCheckBox->setChecked(gIsDataSourceCheckingStrict);
    if (gIsDataSourceCheckingStrict) {
        ui->errorsNumberSpinBox->setEnabled(false);
    }

    //copyable graph properties
    ui->isDimensionCopyableCheckBox->setChecked(gIsDimensionCopyable);
    ui->isLineColorCopyableCheckBox->setChecked(gIsLineColorCopyable);
    ui->isLineWidthCopyableCheckBox->setChecked(gIsLineWidthCopyable);
    ui->isLineStyleCopyableCheckBox->setChecked(gIsLineStyleCopyable);
    ui->isInterpolationCopyableCheckBox->setChecked(gIsInterpolationCopyable);
    ui->isStringRepresentationCheckBox->setChecked(gIsStringRepresentationCopyable);

    ui->isScatterShapeCopyableCheckBox->setChecked(gIsScatterShapeCopyable);
    ui->isScatterSizeCopyableCheckBox->setChecked(gIsScatterSizeCopyable);
    ui->isScatterDecimationCopyableCheckBox->setChecked(gIsScatterDecimationCopyable);

    ui->isBrushStyleCopyableCheckBox->setChecked(gIsBrushStyleCopyable);
    ui->isBrushColorCopyableCheckBox->setChecked(gIsBrushColorCopyable);
    ui->isBrushBasisCopyableCheckBox->setChecked(gIsBrushBasisCopyable);

    ui->isTransformationCopyableCheckBox->setChecked(gIsTransformationCopyable);
    ui->isFiltersCopyableCheckBox->setChecked(gIsFiltersCopyable);

    ui->showDataSourceEditingDialogCheckBox->setChecked(gShowDataSourceEditingDialog);

    ui->gridPenWidthDoubleSpinBox->setValue(gGridPenWidth);
    ui->subgridPenWidthDoubleSpinBox->setValue(gSubGridPenWidth);
    ui->axisPenWidthDoubleSpinBox->setValue(gAxisPenWidth);
    ui->objectWidthIncreaseDoubleSpinBox->setValue(gWidthDeviationForExternalPlotting);


    ui->autoDimensionForEmptyCheckBox->setChecked(gAutoDimFillOnlyForEmpty);
    ui->errorsNumberSpinBox->setValue(gErrorsListCapacity);

    ui->colorSchemeWidget->setCurrentValue(global::gDefaultColorScheme.scheme());

    ui->plotBackgroundColorChoiceWidget->setColor(global::gDefaultPlotBackgroundColor);
    ui->axisRectColorChoiceWidget->setColor(global::gDefaultAxisRectColor);
    ui->gridColorChoiceWidget->setColor(global::gDefaultGridColor);
    ui->subgridColorChoiceWidget->setColor(global::gDefaultSubGridColor);
    ui->plotFontColorChoiceWidget->setColor(global::gDefaultPlotFontColor);
    ui->graphicalPrimitivesColorChoiceWidget->setColor(global::gDefaultPlotGraphicalPrimitivesColor);

    ui->graphsColorListChoiceWidget->setColorList(global::gGraphColorList);


    ui->legendLocationWidget->setCurrentValue(global::gLegendLocation);

    ui->drawValuesInMeasuringModeCheckBox->setChecked(global::gDrawValuesInMeasuringMode);


}

void PreferencesDialog::applyChangesToPreferences()
{

    using namespace global;

    if ((gRussianLanguage && ui->languageComboBox->currentIndex() == 0)
            || (!gRussianLanguage && ui->languageComboBox->currentIndex() == 1)) {
        QMessageBox::information(this, "Information",
                                 "You have changed application language. To make changes affect you should close JaG."
                                 );
    }

    gRussianLanguage = (ui->languageComboBox->currentIndex() == 1 ? true : false);


    gDoubleToStringConversionPrecision = ui->doubleToStringPrecisonSpinBox->value();

    mPreferences->mLineWidth = ui->lineWidthDoubleSpinBox->value();
    mPreferences->mLineInterpolation =
            static_cast<QCPGraph::LineStyle >(std::get<0>(mLineInterpolationMap[ui->lineInterpolationComboBox->currentIndex()]));


    global::gParameterNamingPolicy =
            static_cast<global::ParamNamingPolicy>(std::get<0>(mParamNamingPolicyMap[ui->parameterNamingPolicyComboBox->currentIndex()]));


    gApplicationGUIStyle = ui->guiStyleComboBox->currentText();
    QApplication::setStyle(gApplicationGUIStyle);

    mPreferences->mScatterSize = ui->scatterSizeDoubleSpinBox->value();
    mPreferences->mScatterDecimation = ui->scatterDecimationSpinBox->value();

    mPreferences->mXLabel = ui->xLabelLineEdit->text();
    mPreferences->mXLabelFont = QFont(ui->xLabelFontComboBox->currentFont());
    mPreferences->mXLabelFont.setPointSize(ui->xLabelFontSizeSpinBox->value());

    gIsJaGMainWindowInitialMoveEnabled = ui->restorePositionCheckBox->isChecked();

    mPreferences->mXAxisFont = QFont(ui->xAxisTickLabelsFontComboBox->currentFont());
    mPreferences->mXAxisFont.setPointSize(ui->xAxisTickLabelFontSizeSpinBox->value());

    mPreferences->mYAxisFont = QFont(ui->yAxisTickLabelsFontComboBox->currentFont());
    mPreferences->mYAxisFont.setPointSize(ui->yAxisTickLabelFontSizeSpinBox->value());

    mPreferences->mLegendVisibility = ui->legendVisibilityCheckBox->isChecked() ? true : false ;
    mPreferences->mLegendFont = QFont(ui->legendFontComboBox->currentFont());
    mPreferences->mLegendFont.setPointSize(ui->legendFontSizeSpinBox->value());
    mPreferences->mIsLegendFrameVisible = ui->legendFrameVisibilityCheckBox->isChecked();

    mPreferences->mTitleFont = QFont(ui->titleFontComboBox->currentFont());
    mPreferences->mTitleFont.setPointSize(ui->titleFontSizeSpinBox->value());



    mPreferences->mUnitsVisibility = ui->unitsVisibilityCheckBox->isChecked() ? true : false ;
    mPreferences->mDimensionFont = QFont(ui->dimensionFontComboBox->currentFont());
    mPreferences->mDimensionFont.setPointSize(ui->dimensionFontSizeSpinBox->value());

    mPreferences->mEventFont = QFont(ui->commandFontComboBox->currentFont());
    mPreferences->mEventFont.setPointSize(ui->eventFontSizeSpinBox->value());

    mPreferences->mArrowedTextFont = QFont(ui->arrowedTextFontComboBox->currentFont());
    mPreferences->mArrowedTextFont.setPointSize(ui->arrowedTextFontSizeSpinBox->value());

    gXTickSpacing = ui->xTickSpacingDoubleSpinBox->value();
    gYTickSpacing = ui->yTickSpacingDoubleSpinBox->value();

    gWheelZoomEnabled = ui->wheelZoomEnableCheckBox->isChecked() ? true : false ;

    gTracerSize = ui->tracerSizeSpinBox->value();

    gGraphCaptureDistance = ui->graphCaptureDistanceSpinBox->value();

    gShowDataSourceLoadingProgress = ui->progressShowingCheckBox->isChecked();

    gEventLineWidth = ui->eventLineWidthDoubleSpinBox->value();

    gFindStringErasingTimerInterval = ui->erasingTimerDoubleSpinBox->value();

    gIsDataSourceCheckingStrict = ui->dataSourceStrictCheckingCheckBox->isChecked();
    gErrorsListCapacity = ui->errorsNumberSpinBox->value();

    gLineWidthDeviationForExternalPlotting = ui->graphWidthIncreaseDoubleSpinBox->value();

    gDrawValuesInMeasuringMode = ui->drawValuesInMeasuringModeCheckBox->isChecked();

    QString plainText = ui->favouritePlainTextEdit->toPlainText();
    gFavouriteParameters.clear();
    foreach (QString favourite, plainText.split('\n')) {
        gFavouriteParameters.push_back(favourite);
    }

    QString plainHateText = ui->hatePlainTextEdit->toPlainText();
    gHateParameters.clear();
    foreach (QString hate, plainHateText.split('\n')) {
        gHateParameters.push_back(hate);
    }

    //copyable graph properties
    gIsDimensionCopyable = ui->isDimensionCopyableCheckBox->isChecked();
    gIsLineColorCopyable = ui->isLineColorCopyableCheckBox->isChecked();
    gIsLineWidthCopyable = ui->isLineWidthCopyableCheckBox->isChecked();
    gIsLineStyleCopyable = ui->isLineStyleCopyableCheckBox->isChecked();
    gIsInterpolationCopyable = ui->isInterpolationCopyableCheckBox->isChecked();
    gIsStringRepresentationCopyable = ui->isStringRepresentationCheckBox->isChecked();

    gIsScatterShapeCopyable = ui->isScatterShapeCopyableCheckBox->isChecked();
    gIsScatterSizeCopyable = ui->isScatterSizeCopyableCheckBox->isChecked();
    gIsScatterDecimationCopyable = ui->isScatterDecimationCopyableCheckBox->isChecked();

    gIsBrushStyleCopyable = ui->isBrushStyleCopyableCheckBox->isChecked();
    gIsBrushColorCopyable = ui->isBrushColorCopyableCheckBox->isChecked();
    gIsBrushBasisCopyable = ui->isBrushBasisCopyableCheckBox->isChecked();

    gIsTransformationCopyable = ui->isTransformationCopyableCheckBox->isChecked();
    gIsFiltersCopyable = ui->isFiltersCopyableCheckBox->isChecked();

    gShowDataSourceEditingDialog = ui->showDataSourceEditingDialogCheckBox->isChecked();

    gDefaultDirectory = ui->defaultDirectoryLineEdit->text();

    gDimensionlessParameterUnitLabel = ui->dimensionlessUnitLineEdit->text();

    gGridPenWidth = ui->gridPenWidthDoubleSpinBox->value();
    gSubGridPenWidth = ui->subgridPenWidthDoubleSpinBox->value();
    gAxisPenWidth = ui->axisPenWidthDoubleSpinBox->value();

    gWidthDeviationForExternalPlotting = ui->objectWidthIncreaseDoubleSpinBox->value();


    gAutoDimFillOnlyForEmpty = ui->autoDimensionForEmptyCheckBox->isChecked();

    gDefaultPlotBackgroundColor = ui->plotBackgroundColorChoiceWidget->color();
    gDefaultAxisRectColor = ui->axisRectColorChoiceWidget->color();
    gDefaultGridColor = ui->gridColorChoiceWidget->color();
    gDefaultSubGridColor = ui->subgridColorChoiceWidget->color();
    gDefaultPlotFontColor = ui->plotFontColorChoiceWidget->color();
    gDefaultPlotGraphicalPrimitivesColor = ui->graphicalPrimitivesColorChoiceWidget->color();

    gGraphColorList = ui->graphsColorListChoiceWidget->colorList();

    global::gDefaultColorScheme = ui->colorSchemeWidget->currentValue<global::ColorScheme::Scheme>();


    gLegendLocation = ui->legendLocationWidget->currentValue<PlotLayoutModel::LegendLocation>();


    ui->defaultDimensionWidget->applyChangesToGlobalLists();





}


