#ifndef PREFERENCESDIALOG_H
#define PREFERENCESDIALOG_H

#include "global/global_definitions.h"
#include <QDialog>
#include "core/preferences.h"
#include <QVector>
#include <tuple>
//#include "global/global_definitions.h"

namespace Ui {
class PreferencesDialog;
}

/*!
 \brief Dialog that can is used to show and set manually basic JaG settings.

*/
class PreferencesDialog : public QDialog
{
    Q_OBJECT

public:
    PreferencesDialog(Preferences*, QWidget *parent = 0);
    ~PreferencesDialog();

signals:
    void preferenceSavingRequest();

public slots:
    void okRequest();
    void cancelRequest();
    void okAndSaveRequest();
    void setChooseDefaultDirectory();
    void setDefaultPreferences();

    void errorNumberEnabilityChanged();
private:
    Ui::PreferencesDialog *ui;
    Preferences *mPreferences;
    QVector<std::tuple< QCPGraph::LineStyle, QString>> mLineInterpolationMap;
    QVector<std::tuple< global::ParamNamingPolicy, QString>> mParamNamingPolicyMap;

    void initializeStateFromPreferences();

    void applyChangesToPreferences();
};

#endif // PREFERENCESDIALOG_H
