#ifndef PLOTSHOWEDITWIDGET_H
#define PLOTSHOWEDITWIDGET_H

#include <QWidget>
#include "core/plotlayoutmodel.h"
#include "controlWidgets/plotcontrolwidget.h"
#include "core/plotwidget.h"
#include "core/preferences.h"
#include <QPixmap>
#include "event/timeevent.h"
#include "core/graphcontainer.h"

namespace Ui {
class PlotShowEditWidget;
}



/*!
 \brief PlotShowEditWidget contains PlotWidget, PlotHierarchyWidget and a set of different widgets to change properties of graphs,
 axis, anchors...

*/
class PlotShowEditWidget : public QWidget, public GraphContainer
{
    Q_OBJECT

public:
    explicit PlotShowEditWidget(QWidget *parent = 0);
    ~PlotShowEditWidget();

    QPixmap toPixmap();
    QPixmap toPixmap(int width, int height);
    void toPainter(QCPPainter *painter,  int width = 0, int height = 0);

    PlotWidget::PlotWidgetMode plotWidgetState() const;

    const PlotWidget* plotWidget() const;

//public slots:
    virtual GraphModel* addNewGraph(const QVector<double> &x, const QVector<double> &y, const QString &name);
    virtual void addNewEvent(const TimeEvent *event);

public slots:

    void treatGraphEditRequest(GraphModel*);
    void treatGraphVectorEditRequest(QVector<GraphModel*>);
    void treatAxisEditRequest(AxisModel*);
    void treatCommandEditRequest(TimeEvent*);
    void treatAnchorEditRequest(Anchor*);
    void treatArrowedTextEditRequest(ArrowedText*);
    void treatHorizontalLevelEditRequest(HorizontalLevel*);

    void autoscale();
    void setPlotWidgetState(PlotWidget::PlotWidgetMode);
    void combineGraphs();
    void separateGraphs();
    void composeIntellectually();
    void moveCurrentObject(AxisModel::AxisMovement);
    void stretchCurrentAxis(double factor);
    void increaseDecimationOfCurrentGraph(double factor);
    void deleteCurrentObject();
    void increaseWidthOfCurrentGraph(double);
    void switchLineScatterOfCurrentGraph(int);
    void switchLineColorOfCurrentGraph(int);
    void switchLineInterpolationOfCurrentGraph(int);
    bool toogleFullScreen();


    void setPreferences(const Preferences &pref);
    void emitInfoString(double, double);

    void savePng(QString fileName);
    void saveBmp(QString fileName);
    void saveJpg(QString fileName);

    void activateGraph(int index);
    void activateAxis(int index);

    void secondaryProcessing();
    void setAutoUnits();

    void stepBackInHistory();
    void stepForwardInHistory();

    QVector<QString> getGraphNames();

signals:
    void infoStringEmitted(QString);

protected:
//    void keyPressEvent(QKeyEvent *event);

private:
    Ui::PlotShowEditWidget *ui;
    PlotLayoutModel mLayoutModel;
    bool isControlsVisible;
    
    void setControlWidgetsVisible(bool);

};

#endif // PLOTSHOWEDITWIDGET_H
