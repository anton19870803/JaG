#include "plotshoweditwidget.h"
#include "ui_plotshoweditwidget.h"
#include "secondaryprocessing/datacontainer.h"
#include "secondaryprocessing/secondaryprocessing.h"
#include "secondaryprocessing/secondaryprocessingdialog.h"
#include "global/global_definitions.h"
#include <QMessageBox>
#include <QToolBar>
//#include "core/jagmainwindow.h"

PlotShowEditWidget::PlotShowEditWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlotShowEditWidget), isControlsVisible(true)
{
    ui->setupUi(this);
    ui->plotWithToolsWidget->mPlotWidget->setLayoutModel(&mLayoutModel);
    ui->plotHierarchyWidget->setPlotLayoutModel(&mLayoutModel);
    ui->plotControlWidget->setLayoutModel(&mLayoutModel);

//    QToolBar *tb = new QToolBar();
//    tb->addWidget(new QLabel("asdfsadf"));
//    ui->splitter_2->addWidget(tb);

//        QToolBar *tb = new QToolBar();
//        ui->splitter_2->addWidget(tb);
//    auto actions = global::gJagMainWindowPointer->actions();
//    for (auto action : actions)
//        tb->addAction(action);

    connect(ui->plotHierarchyWidget, SIGNAL(graphActivated(GraphModel*)),
            this, SLOT(treatGraphEditRequest(GraphModel *)));
    connect(ui->plotHierarchyWidget, SIGNAL(graphVectorActivated(QVector<GraphModel*>)),
            this, SLOT(treatGraphVectorEditRequest(QVector<GraphModel*>)));
    connect(ui->plotHierarchyWidget, SIGNAL(axisActivated(AxisModel*)),
            this, SLOT(treatAxisEditRequest(AxisModel *)));
    connect(ui->plotHierarchyWidget, SIGNAL(timeEventActivated(TimeEvent*)),
            this, SLOT(treatCommandEditRequest(TimeEvent *)));
    connect(ui->plotHierarchyWidget, SIGNAL(anchorActivated(Anchor*)),
            this, SLOT(treatAnchorEditRequest(Anchor*)));
    connect(ui->plotHierarchyWidget, SIGNAL(arrowedTextActivated(ArrowedText*)),
            this, SLOT(treatArrowedTextEditRequest(ArrowedText*)));
    connect(ui->plotHierarchyWidget, SIGNAL(horizontalLevelActivated(HorizontalLevel*)),
            this, SLOT(treatHorizontalLevelEditRequest(HorizontalLevel*)));

    connect(ui->plotControlWidget, SIGNAL(settingXRangeRequest(double, double)),
            &mLayoutModel, SLOT(setXRangeWrapper(double,double)));
    connect(&mLayoutModel, SIGNAL(layoutModelChanged()),
            ui->plotControlWidget, SLOT(initializeState()));


    connect(ui->plotControlWidget, SIGNAL(legendChangingVisibilityRequest(bool)),
            &mLayoutModel, SLOT(setLegendVisibility(bool)));
    connect(ui->plotControlWidget, SIGNAL(dimensionChangingVisibilityRequest(bool)),
            &mLayoutModel, SLOT(setDimensionVisibility(bool)));

    connect(ui->eventControlWidget, SIGNAL(arrowedTextCreationRequest(double)),
            &mLayoutModel, SLOT(addArrowedTextByTime(double)));
    connect(ui->anchorControlWidget, SIGNAL(arrowedTextCreationRequest(double)),
            &mLayoutModel, SLOT(addArrowedTextByTime(double)));

    connect(ui->plotWithToolsWidget->mPlotWidget, SIGNAL(currentCoordinates(double, double)),
            this, SLOT(emitInfoString(double,double)));

    connect(ui->plotHierarchyWidget, SIGNAL(minimumWidth(int)),
            ui->plotControlWidget, SLOT(setMinWidth(int)));

    ui->multiModelControlWidget->setLayoutModel(&mLayoutModel);

    ui->axisControlWidget->hide();
    ui->eventControlWidget->hide();
    ui->anchorControlWidget->hide();
    ui->multiModelControlWidget->hide();
    ui->arrowedTextControlwidget->hide();
    ui->horizontalLevelControlWidget->hide();
}

PlotShowEditWidget::~PlotShowEditWidget()
{
    delete ui;
}

QPixmap PlotShowEditWidget::toPixmap()
{
    return ui->plotWithToolsWidget->mPlotWidget->toPixmap();
}

QPixmap PlotShowEditWidget::toPixmap(int width, int height)
{
    return ui->plotWithToolsWidget->mPlotWidget->toPixmap(width, height);
}

void PlotShowEditWidget::toPainter(QCPPainter *painter, int width, int height)
{
    return ui->plotWithToolsWidget->mPlotWidget->toPainter(painter, width, height);
}

PlotWidget::PlotWidgetMode PlotShowEditWidget::plotWidgetState() const
{
    return ui->plotWithToolsWidget->mPlotWidget->plotWidgetState();
}

const PlotWidget *PlotShowEditWidget::plotWidget() const
{
    return ui->plotWithToolsWidget->mPlotWidget;
}

GraphModel* PlotShowEditWidget::addNewGraph(const QVector<double> &x, const QVector<double> &y, const QString &name)
{
    return mLayoutModel.addPlot(x, y, name);
}

void PlotShowEditWidget::addNewEvent(const TimeEvent *event)
{
    mLayoutModel.addCommand(event);
}

void PlotShowEditWidget::treatGraphEditRequest(GraphModel *model)
{

    setControlWidgetsVisible(false);
    ui->multiModelControlWidget->show();
    ui->multiModelControlWidget->setModelVector(QVector<GraphModel *>{model});
}

void PlotShowEditWidget::treatGraphVectorEditRequest(QVector<GraphModel *>graphVector)
{


    setControlWidgetsVisible(false);
    ui->multiModelControlWidget->show();
    ui->multiModelControlWidget->setModelVector(graphVector);

}

void PlotShowEditWidget::treatAxisEditRequest(AxisModel *model)
{

    setControlWidgetsVisible(false);
    ui->axisControlWidget->show();
    ui->axisControlWidget->setAxisModel(model);
}

void PlotShowEditWidget::treatCommandEditRequest(TimeEvent *command)
{

    setControlWidgetsVisible(false);
    ui->eventControlWidget->show();
    ui->eventControlWidget->setTimeEvent(command);
}

void PlotShowEditWidget::treatAnchorEditRequest(Anchor *anchor)
{

    setControlWidgetsVisible(false);
    ui->anchorControlWidget->show();
    ui->anchorControlWidget->setAnchor(anchor);

}

void PlotShowEditWidget::treatArrowedTextEditRequest(ArrowedText *ArrowedText)
{

    setControlWidgetsVisible(false);
    ui->arrowedTextControlwidget->show();
    ui->arrowedTextControlwidget->setArrowedText(ArrowedText);
}

void PlotShowEditWidget::treatHorizontalLevelEditRequest(HorizontalLevel *level)
{
    setControlWidgetsVisible(false);
    ui->horizontalLevelControlWidget->show();
    ui->horizontalLevelControlWidget->setHorizontalLevel(level);
}


void PlotShowEditWidget::autoscale()
{
    mLayoutModel.autoscaleXRange();
    mLayoutModel.autoscaleYRange();
}

void PlotShowEditWidget::setPlotWidgetState(PlotWidget::PlotWidgetMode state)
{
    ui->plotWithToolsWidget->mPlotWidget->setPlotWidgetState(state);
}

void PlotShowEditWidget::combineGraphs()
{
    mLayoutModel.setLayoutState(PlotLayoutModel::LayoutState::AllInOne);
}

void PlotShowEditWidget::separateGraphs()
{
    mLayoutModel.setLayoutState(PlotLayoutModel::LayoutState::EachSeparate);
}

void PlotShowEditWidget::composeIntellectually()
{
    mLayoutModel.composeIntellectually();
}

void PlotShowEditWidget::moveCurrentObject(AxisModel::AxisMovement motion)
{
    AxisModel* currentAxis = NULL;
    GraphModel* currentGraph = NULL;
    if ((currentAxis = ui->plotHierarchyWidget->currentAxis()) != NULL) {
        mLayoutModel.moveAxisModel(currentAxis, motion);
    } else if ((currentGraph = ui->plotHierarchyWidget->currentGraph()) != NULL) {
        GraphModel::GraphMovement graphMotion = (motion == AxisModel::AxisMovement::Up) ?
                    GraphModel::GraphMovement::LongUp : GraphModel::GraphMovement::LongDown;
        mLayoutModel.moveGraphModel(currentGraph, graphMotion);
    }
}

void PlotShowEditWidget::stretchCurrentAxis(double factor)
{
    AxisModel* currentAxis = NULL;
    if ((currentAxis = ui->plotHierarchyWidget->currentAxis()) != NULL) {
        double currentStretch = currentAxis->stretchFactor();
        currentAxis->setStretchFactor(currentStretch*factor);
        ui->axisControlWidget->initializeState();
    }
}

void PlotShowEditWidget::increaseDecimationOfCurrentGraph(double factor)
{

//    GraphModel* currentGraph = NULL;
//    if ((currentGraph = ui->plotHierarchyWidget->currentGraph()) != NULL) {
//        QCPScatterStyle scatter = currentGraph->getScatterStyle();
//        int currentDecimation = scatter.decimation();
//        currentDecimation *= factor;
//        if (currentDecimation <= 0)
//            currentDecimation = 1;
//        currentGraph->setScatterDecimation(currentDecimation);
//        ui->modelControlWidget->setModel(currentGraph);
//    }

    auto selectedGraphs = ui->plotHierarchyWidget->selectedGraphs();
    for (auto graph : selectedGraphs) {
        QCPScatterStyle scatter = graph->getScatterStyle();
        int currentDecimation = scatter.decimation();
        currentDecimation *= factor;
        if (currentDecimation <= 0)
            currentDecimation = 1;
        graph->setScatterDecimation(currentDecimation);
//        ui->modelControlWidget->setModel(graph);
        ui->multiModelControlWidget->setModelVector({graph});
    }
}

void PlotShowEditWidget::deleteCurrentObject()
{
    AxisModel* currentAxis = NULL;
    GraphModel* currentGraph = NULL;
    if ((currentAxis = ui->plotHierarchyWidget->currentAxis()) != NULL) {
        mLayoutModel.removeAxisModel(currentAxis);
    } else if ((currentGraph = ui->plotHierarchyWidget->currentGraph()) != NULL) {
        mLayoutModel.removeGraphModel(currentGraph);
    }
}

void PlotShowEditWidget::increaseWidthOfCurrentGraph(double factor)
{

    auto selectedGraphs = ui->plotHierarchyWidget->selectedGraphs();
    for (auto graph : selectedGraphs) {
        auto pen = graph->getPen();
        pen.setWidthF(pen.widthF() * factor);
        graph->setPen(pen);
//        ui->modelControlWidget->initializeStateFromModel();
        ui->multiModelControlWidget->initializeStateFromModels();
    }
}

void PlotShowEditWidget::switchLineScatterOfCurrentGraph(int deviation)
{

    auto selectedGraphs = ui->plotHierarchyWidget->selectedGraphs();
    for (auto graph : selectedGraphs) {
        QCPScatterStyle scatterStyle = graph->getScatterStyle();
        QCPScatterStyle::ScatterShape scatterShape = scatterStyle.shape();
        QVector<QCPScatterStyle::ScatterShape> scatterShapeVector = {
                                                    QCPScatterStyle::ScatterShape::ssNone,
                                                    QCPScatterStyle::ScatterShape::ssDot,
                                                    QCPScatterStyle::ScatterShape::ssCross,
                                                    QCPScatterStyle::ScatterShape::ssPlus,
                                                    QCPScatterStyle::ScatterShape::ssCircle,
                                                    QCPScatterStyle::ScatterShape::ssDisc,
                                                    QCPScatterStyle::ScatterShape::ssSquare,
                                                    QCPScatterStyle::ScatterShape::ssDiamond,
                                                    QCPScatterStyle::ScatterShape::ssStar,
                                                    QCPScatterStyle::ScatterShape::ssTriangle,
                                                    QCPScatterStyle::ScatterShape::ssTriangleInverted,
                                                    QCPScatterStyle::ScatterShape::ssCrossSquare,
                                                    QCPScatterStyle::ScatterShape::ssPlusSquare,
                                                    QCPScatterStyle::ScatterShape::ssCrossCircle,
                                                    QCPScatterStyle::ScatterShape::ssPlusCircle
                                                    };
        int index = scatterShapeVector.indexOf(scatterShape);
        if (deviation > 0) {
            scatterStyle.setShape(scatterShapeVector[index + 1 < scatterShapeVector.size() ? index + 1 : 0]);
        } else
            scatterStyle.setShape(scatterShapeVector[index - 1 >= 0 ? index - 1 : scatterShapeVector.size() - 1]);
        graph->setScatterStyle(scatterStyle);
//        ui->modelControlWidget->initializeStateFromModel();
        ui->multiModelControlWidget->initializeStateFromModels();
    }
}

void PlotShowEditWidget::switchLineColorOfCurrentGraph(int deviation)
{

    auto selectedGraphs = ui->plotHierarchyWidget->selectedGraphs();
    for (auto graph : selectedGraphs) {
        QPen pen = graph->getPen();
        QColor color = pen.color();
        QVector<QColor> lineColorVector = {
                                            Qt::blue,
                                            Qt::green,
                                            Qt::red,
                                            Qt::magenta,
                                            Qt::black,
                                            Qt::darkGreen,
                                            Qt::darkYellow,
                                            Qt::darkMagenta,
                                            Qt::darkCyan,
                                           // Qt::darkYellow,  //doesn't work,   why i don't know
                                            Qt::darkRed,
                                            Qt::darkGray,
                                            Qt::cyan,
                                            Qt::yellow,
                                            Qt::darkBlue
                                           };
        int index = lineColorVector.indexOf(color);
        if (deviation > 0) {
            graph->setColor(lineColorVector[(index + 1 < lineColorVector.size()) ? (index + 1) : 0]);
        } else {
            graph->setColor(lineColorVector[index - 1 >= 0 ? index - 1 : lineColorVector.size() - 1]);
        }
//        ui->modelControlWidget->initializeStateFromModel();
        ui->multiModelControlWidget->initializeStateFromModels();
    }
}

void PlotShowEditWidget::switchLineInterpolationOfCurrentGraph(int deviation)
{


    auto selectedGraphs = ui->plotHierarchyWidget->selectedGraphs();
    for (auto graph : selectedGraphs) {
        QCPGraph::LineStyle graphInterpolation = graph->getLineInterpolation();
        //reduces set of interpolation types
        QVector<QCPGraph::LineStyle> lineInterpolationVector = {
                                                        QCPGraph::LineStyle::lsLine,
                                                        QCPGraph::LineStyle::lsStepLeft
                                                        };
        int index = lineInterpolationVector.indexOf(graphInterpolation);
        if (index != -1) {
            if (deviation > 0) {
                graph->setLineInterpolation(lineInterpolationVector[(index + 1 < lineInterpolationVector.size()) ? (index + 1) : 0]);
            } else {
                graph->setLineInterpolation(lineInterpolationVector[index - 1 >= 0 ? index - 1 : lineInterpolationVector.size() - 1]);
            }
        } else {
            graph->setLineInterpolation(QCPGraph::LineStyle::lsLine);
        }
//        treatGraphEditRequest(currentGraph);
//        ui->modelControlWidget->initializeStateFromModel();
        ui->multiModelControlWidget->initializeStateFromModels();
    }
}

bool PlotShowEditWidget::toogleFullScreen()
{
//    ui->anchorControlWidget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
//    ui->modelControlWidget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
//    return ui->plotWithToolsWidget->mPlotWidget->toogleFullScreen();
    return ui->plotWithToolsWidget->toogleFullScreen();

}

void PlotShowEditWidget::setPreferences(const Preferences &pref)
{
    mLayoutModel.setPreferences(pref);
}

void PlotShowEditWidget::emitInfoString(double x, double y)
{
//    QString infoString = QString::number(x) + QString::number(y);
//    emit infoStringEmitted(infoString);
    emit infoStringEmitted(mLayoutModel.getInfoString(x, y));
}

void PlotShowEditWidget::savePng(QString fileName)
{
    ui->plotWithToolsWidget->mPlotWidget->savePng(fileName);
}

void PlotShowEditWidget::saveBmp(QString fileName)
{
    ui->plotWithToolsWidget->mPlotWidget->saveBmp(fileName);
}

void PlotShowEditWidget::saveJpg(QString fileName)
{
    ui->plotWithToolsWidget->mPlotWidget->saveJpg(fileName);
}

void PlotShowEditWidget::activateGraph(int index)
{
    ui->plotHierarchyWidget->activateGraph(index);
}

void PlotShowEditWidget::activateAxis(int index)
{
    ui->plotHierarchyWidget->activateAxis(index);
}

void PlotShowEditWidget::secondaryProcessing()
{
    using namespace global;
    mLayoutModel.prepareDataForSecondaryProcessing();

    std::string rawFunctionString;
    std::string functionString;
    QString name;
    while (1) {
        SecondaryProcessingDialog dial(&mLayoutModel, this);
        dial.setFunctionString(QString::fromStdString(rawFunctionString));
        //setting x ranges
        std::tuple<double, double> totalRAnge = mLayoutModel.getXMaXTotalRange();
        dial.setTotalRange(std::get<0>(totalRAnge),  std::get<1>(totalRAnge));
        std::tuple<double, double> visibleRange = mLayoutModel.getXRange();
        dial.setVisibleRange(std::get<0>(visibleRange),  std::get<1>(visibleRange));

        dial.exec();
        if (dial.result() == QDialog::Rejected) {
            break;
        }
        rawFunctionString = dial.functionString().toStdString();
        if (gIsAutoNanTo0ConversionEnabled)
            functionString = "nanto0(" + rawFunctionString + ")";
        else
            functionString = rawFunctionString;
        name = dial.nameString();
        expression::grammar<DataContainer, std::string::iterator> gram;
        auto beg = functionString.begin();
        DataContainer result;
        DataContainer::setConstraints(std::get<0>(dial.getXRange()), std::get<1>(dial.getXRange()));
        try {

            if (parse(beg, functionString.end(), gram, result) && beg == functionString.end()) {
                //ensuring desired x range
//                auto desiredXRange = dial.getXRange();
//                DataContainer xRangeDataContainer(QVector<double>({std::get<0>(desiredXRange), std::get<1>(desiredXRange)}),
//                                                  QVector<double>({1., 1.}));
//                result *=    xRangeDataContainer;

                addNewGraph(result.mXData, result.mYData, name);
                gPrevSecondProcesFunctionVector.push_front(QString::fromStdString(rawFunctionString));
                if (gPrevSecondProcesFunctionVector.size() > gPrevSecondProcesFunctionVectorMaxSize)
                    gPrevSecondProcesFunctionVector.remove(gPrevSecondProcesFunctionVectorMaxSize,1);
                break;
            }
        } catch (...) {
            ;
        }
        DataContainer::setDefaultConstraints();



        //DEBUG
        QString rightPart = QString::fromStdString(std::string(functionString.begin(), beg));
        QString wrongPart = QString::fromStdString(std::string(beg, functionString.end()));
        //DEBUG
        QString warningMessage("Expression is not correct");
        if (rightPart != "") {
            warningMessage += "<br><font color=\"green\">" + rightPart + "</font>";
            warningMessage += "<font color=\"red\">" + wrongPart + "</font>";
        }
        QMessageBox::warning(this, "Error", warningMessage);
    }

    gCurrGraphVector.clear();
}

void PlotShowEditWidget::setAutoUnits()
{
    mLayoutModel.setAutoDimensions();
}

void PlotShowEditWidget::stepBackInHistory()
{
    mLayoutModel.stepBackInHistory();
}

void PlotShowEditWidget::stepForwardInHistory()
{
    mLayoutModel.stepForwardInHistory();

}

QVector<QString> PlotShowEditWidget::getGraphNames()
{
    QVector<GraphModel*>  graphVector = mLayoutModel.getGraphModelVector();
    QVector<QString> ret;
    foreach (auto graph, graphVector) {
        ret.push_back(graph->getName());
    }
    return ret;
}

void PlotShowEditWidget::setControlWidgetsVisible(bool visible)
{
    if (visible) {
        ui->axisControlWidget->show();
        ui->eventControlWidget->show();
        ui->anchorControlWidget->show();
        ui->multiModelControlWidget->show();
        ui->arrowedTextControlwidget->show();
        ui->horizontalLevelControlWidget->show();
    } else {
        ui->axisControlWidget->hide();
        ui->eventControlWidget->hide();
        ui->anchorControlWidget->hide();
        ui->multiModelControlWidget->hide();
        ui->arrowedTextControlwidget->hide();
        ui->horizontalLevelControlWidget->hide();
    }
}

