#include <global/global_definitions.h>
#include "plotwidget.h"
#include "basic/bassic.h"
#include <QDebug>
#include <QApplication>
#include <cmath>
#include <algorithm>
#include <cfloat>

#include <core/horizontallevel.h>

#include "core/plotwidgetstate/plotwidgetstate.h"
#include "core/plotwidgetstate/plotwidgetarrowdrawingstate.h"
#include "core/plotwidgetstate/plotwidgetleveldrawingstate.h"
#include "core/plotwidgetstate/plotwidgetmeasuringstate.h"
#include "core/plotwidgetstate/plotwidgetmotionstate.h"
#include "core/plotwidgetstate/plotwidgetnormalstate.h"
#include "core/plotwidgetstate/plotwidgetxyzoomstate.h"
#include "core/plotwidgetstate/plotwidgetxzoomstate.h"
#include "core/plotwidgetstate/plotwidgetyzoomstate.h"
#include "core/plotwidgetstate/plotwidgetfastsecondaryprocessingstate.h"

PlotWidget::PlotWidget(QWidget *parent) :
    QCustomPlot(parent), QFullScreenAdapter(this), mLayoutModel(NULL), mTimerId(0), mUpdateCounter(0), mUpdateEverythingCounter(0),
    mUpdateAppearanceCounter(0), numberOfPreviousPlots(0), mAxisLeftRightMarginGroup(new QCPMarginGroup(this)),
    mMode(PlotWidgetMode::Common),
    mBottomRightLegendLayout(NULL), mState(NULL), mWidgetEmptyState(NULL), mWidgetArrowDrawingState(NULL),
    mWidgetFastSecondaryProcessingState(NULL), mWidgetLevelDrawingState(NULL), mWidgetMeasuringState(NULL),
    mWidgetMotionState(NULL), mWidgetNormalState(NULL), mWidgetXYZoomState(NULL), mWidgetXZoomState(NULL), mWidgetYZoomState(NULL)
{

    mWidgetEmptyState = new PlotWidgetState(this);
    mWidgetArrowDrawingState = new PlotWidgetArrowDrawingState(this);
    mWidgetFastSecondaryProcessingState = new PlotWidgetFastSecondaryProcessingState(this);
    mWidgetLevelDrawingState = new PlotWidgetLevelDrawingState(this);
    mWidgetMeasuringState = new PlotWidgetMeasuringState(this);
    mWidgetMotionState = new PlotWidgetMotionState(this);
    mWidgetXYZoomState = new PlotWidgetXYZoomState(this);
    mWidgetXZoomState = new PlotWidgetXZoomState(this);
    mWidgetYZoomState = new PlotWidgetYZoomState(this);
    mWidgetNormalState = new PlotWidgetNormalState(this);
    mState = mWidgetNormalState;

    addLayer("graph", layer("axes"));
    addLayer("tracer", layer("graph"));
}

void PlotWidget::setLayoutModel(const PlotLayoutModel *layoutModel)
{
    if (layoutModel != NULL) {
        disconnect(layoutModel, SIGNAL(layoutModelChanged()),
                   this, SLOT(updateEverything()));
    }
    mLayoutModel = layoutModel;
    connect(mLayoutModel, SIGNAL(layoutModelChanged()),
            this, SLOT(updateEverything()));
    connect(mLayoutModel, SIGNAL(layoutAppearanceChanged()),
            this, SLOT(updateAppearance()));
    connect(this, SIGNAL(xZoomRequest(double,double)),
            mLayoutModel, SLOT(setXRangeWrapper(double,double)));
    connect(this, SIGNAL(yZoomRequest(double,double,int)),
            mLayoutModel, SLOT(setYRangeForVisibleAxisWrapper(double,double,int)));
    connect(this, SIGNAL(xyZoomRequest(double,double,double,double,int)),
            mLayoutModel, SLOT(setXYRangeWrapper(double,double,double,double,int)));

}

PlotWidget::~PlotWidget()
{
    delete mAxisLeftRightMarginGroup;
}

PlotWidget::PlotWidgetMode PlotWidget::plotWidgetState() const
{
    return mMode;
}

const PlotLayoutModel *PlotWidget::plotLayoutModel() const
{
    return mLayoutModel;
}

QVector<QCPItemTracer *> PlotWidget::tracerVector() const
{
    return mTracerVector;
}

QPixmap PlotWidget::toPixmap(int width, int height, double scale)
{
    increaseWidthOfObjects(global::gWidthDeviationForExternalPlotting, global::gLineWidthDeviationForExternalPlotting);
    auto pixMap = QCustomPlot::toPixmap(width, height, scale);
    increaseWidthOfObjects(-global::gWidthDeviationForExternalPlotting, -global::gLineWidthDeviationForExternalPlotting);
    return pixMap;
}

bool PlotWidget::savePng(const QString &fileName, int width, int height, double scale, int quality)
{
    increaseWidthOfObjects(global::gWidthDeviationForExternalPlotting, global::gLineWidthDeviationForExternalPlotting);
    auto ret = QCustomPlot::savePng(fileName, width, height, scale, quality);
    increaseWidthOfObjects(-global::gWidthDeviationForExternalPlotting, -global::gLineWidthDeviationForExternalPlotting);
    return ret;
}

bool PlotWidget::saveJpg(const QString &fileName, int width, int height, double scale, int quality)
{
    increaseWidthOfObjects(global::gWidthDeviationForExternalPlotting, global::gLineWidthDeviationForExternalPlotting);
    auto ret = QCustomPlot::savePng(fileName, width, height, scale, quality);
    increaseWidthOfObjects(-global::gWidthDeviationForExternalPlotting, -global::gLineWidthDeviationForExternalPlotting);
    return ret;
}

bool PlotWidget::saveBmp(const QString &fileName, int width, int height, double scale)
{
    increaseWidthOfObjects(global::gWidthDeviationForExternalPlotting, global::gLineWidthDeviationForExternalPlotting);
    auto ret = QCustomPlot::saveBmp(fileName, width, height, scale);
    increaseWidthOfObjects(-global::gWidthDeviationForExternalPlotting, -global::gLineWidthDeviationForExternalPlotting);
    return ret;
}

void PlotWidget::toPainter(QCPPainter *painter, int width, int height)
{
    increaseWidthOfObjects(global::gWidthDeviationForExternalPlotting, global::gLineWidthDeviationForExternalPlotting);
    QCustomPlot::toPainter(painter, width, height);
    increaseWidthOfObjects(-global::gWidthDeviationForExternalPlotting, -global::gLineWidthDeviationForExternalPlotting);
}



void PlotWidget::mousePressEvent(QMouseEvent *event)
{

    if (axisRectCount() == 0) {
        QCustomPlot::mousePressEvent(event);
        return;
    }

    mState->treatMousePressEvent(event);

    QCustomPlot::mousePressEvent(event);
}

void PlotWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (axisRectCount() == 0) {
        QCustomPlot::mouseReleaseEvent(event);
        return;
    }

    mState->treatMouseReleaseEvent(event);

    QCustomPlot::mouseReleaseEvent(event);
}

void PlotWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (axisRectCount() == 0) {
        QCustomPlot::mouseMoveEvent(event);
        return;
    }

    mState->treatMouseMoveEvent(event);

    QCustomPlot::mouseMoveEvent(event);
}

void PlotWidget::wheelEvent(QWheelEvent *event)
{
    mState->treatWheelEvent(event);

    QCustomPlot::wheelEvent(event);
}

void PlotWidget::paintEvent(QPaintEvent *event)
{
    QCustomPlot::paintEvent(event);
    mState->treatPaintEvent(event);
}

void PlotWidget::resizeEvent(QResizeEvent *event)
{
    mState->treatResizeEvent(event);
    recountLastAxisStretch();
    QCustomPlot::resizeEvent(event);
}

void PlotWidget::leaveEvent(QEvent *event)
{

    mState->treatLeaveEvent(event);



    QCustomPlot::leaveEvent(event);
}




//void PlotWidget::addModel(const GraphModel *newModel)
//{
//    modelVector.push_back(newModel);
//    updateGraphics();
//}

//void PlotWidget::removeModel(const GraphModel *newModel)
//{
//    int index;
//    if ((index = modelVector.indexOf(newModel)) != -1) {
//        disconnect(newModel, SIGNAL(appearanceChanged()),
//                this, SLOT(updateGraphics()));
//        disconnect(newModel, SIGNAL(dataChanged()),
//                this, SLOT(updateGraphics()));
//        modelVector.remove(index);
//        updateGraphics();
//    } else {
//        sendLogMessage("Trying to delete a plot that doesn't exist");
//    }
//}


void PlotWidget::setPlotWidgetState(PlotWidget::PlotWidgetMode plotWidgetMode)
{
//    setCursor(Qt::ArrowCursor);

    mMode = plotWidgetMode;

    mState = mWidgetEmptyState;
    switch (plotWidgetMode) {
        case PlotWidgetMode::Common:
            mState = mWidgetNormalState;
            break;
        case PlotWidgetMode::ZoomX:
            mState = mWidgetXZoomState;
            break;
        case PlotWidgetMode::ZoomY:
            mState = mWidgetYZoomState;
            break;
        case PlotWidgetMode::ZoomXY:
            mState = mWidgetXYZoomState;
            break;
        case PlotWidgetMode::Measuring:
            mState = mWidgetMeasuringState;
            break;
        case PlotWidgetMode::Motion:
            mState = mWidgetMotionState;
            break;
        case PlotWidgetMode::ArrowDrawing:
            mState = mWidgetArrowDrawingState;
            break;
        case PlotWidgetMode::FastSecondaryProcessing:
            mState = mWidgetFastSecondaryProcessingState;
            break;
        case PlotWidgetMode::LevelDrawing:
            mState = mWidgetLevelDrawingState;
//        default:
//            break;
    }

    mState->initializeState();

//    if (mMotionState == State::On) {
//        setInteractions(QCP::iRangeDrag);
//    } else {
//        setInteractions(0);
//    }

//    if (mMeasuringState == State::On) {
//        updateTracers();
//    } else {
//        for (int i = 0; i < mTracerVector.size(); ++i) {
//            removeItem(mTracerVector[i]);
//            //removeItem will call delete and clear QCustomPlot internal containers, so the stuff below is supposed to be excessive
////            mTracerVector[i]->setParent(NULL);
////            delete mTracerVector[i];
//        }
//        mTracerVector.clear();
//    }

    update();
    replot();
}

void PlotWidget::updateEverything()
{
    if (mLayoutModel != NULL) {
       mUpdateEverythingCounter++;
       mTimerId = startTimer(100);
    }
}

void PlotWidget::updateAppearance()
{
    if (mLayoutModel != NULL) {
       mUpdateAppearanceCounter++;
       mTimerId = startTimer(100);
    }
}

void PlotWidget::setXRangeForAllAxis(QCPRange xRange)
{
    if (mMode == PlotWidgetMode::Motion) {
        for (int i = 0; i < axisRectCount(); ++i) {
            axisRect(i)->axis(QCPAxis::atBottom)->setRange(xRange);
        }
    }
}

void PlotWidget::updateTracers()
{
    if (mMode == PlotWidgetMode::Measuring) {
        for (auto tracer : mTracerVector) {
            removeItem(tracer);
            //removeItem will call delete and clear QCustomPlot internal containers, so the stuff below is supposed to be excessive
//            tracer->setParent(NULL);
//            delete tracer;
        }
        mTracerVector.clear();


//        for (auto graph : mGraphVector) {
//            QCPItemTracer *graphTracer = new QCPItemTracer(this);
//            addItem(graphTracer);
//            graphTracer->setGraph(graph);
//            graphTracer->setInterpolating(true);
//            graphTracer->setStyle(QCPItemTracer::tsCircle);
//            graphTracer->setPen(QPen(Qt::red));
//            graphTracer->setBrush(Qt::white);
//            graphTracer->setSize(gTracerSize);
//            graphTracer->setIterpolationType(graph->lineStyle());
//            for (int axisRectIndex = 0; axisRectIndex < axisRectCount(); ++axisRectIndex) {
//                if (axisRect(axisRectIndex)->graphs().indexOf(graph) != -1) {
//                    graphTracer->setClipAxisRect (axisRect(axisRectIndex));
//                    break;
//                }
//            }
//            mTracerVector.push_back(graphTracer);
//        }
    }
}

void PlotWidget::updateAnchors()
{

        for (auto tracer : mAnchorVector) {
            removeItem(tracer);
            //removeItem will call delete and clear QCustomPlot internal containers, so the stuff below is supposed to be excessive
//            tracer->setParent(NULL);
//            delete tracer;
        }
        mAnchorVector.clear();
        QVector<Anchor*> anchorVector = mLayoutModel->anchorVector();
        for (Anchor *anchor : anchorVector) {
            for (auto graph : mGraphVector) {
                QCPItemTracer *graphTracer = new QCPItemTracer(this);
                addItem(graphTracer);
                graphTracer->setGraph(graph);
                graphTracer->setInterpolating(true);
                graphTracer->setStyle(anchor->style());
                graphTracer->setPen(anchor->borderPen());
                graphTracer->setBrush(anchor->brushColor());
                graphTracer->setSize(anchor->size());
                graphTracer->setIterpolationType(graph->lineStyle());
                for (int axisRectIndex = 0; axisRectIndex < axisRectCount(); ++axisRectIndex) {
                    if (axisRect(axisRectIndex)->graphs().indexOf(graph) != -1) {
                        graphTracer->setClipAxisRect (axisRect(axisRectIndex));
                        break;
                    }
                }
                graphTracer->setGraphKey(anchor->time());
                mAnchorVector.push_back(graphTracer);

                graphTracer->setLayer("tracer");
            }
        }

}


void  PlotWidget::timerEvent(QTimerEvent *) {
    killTimer(mTimerId);
    if (mUpdateEverythingCounter != 0 || mUpdateAppearanceCounter != 0) { //VERY DIRTY TRICK TO AVOID MULTIPLE REDRAWING
          //VERY DIRTY TRICK TO AVOID MULTIPLE REDRAWING
         treatUpdateRequest();
    }
}

void PlotWidget::treatUpdateRequest()
{
    if (mUpdateEverythingCounter != 0) {
        mUpdateAppearanceCounter = 0;
        mUpdateEverythingCounter = 0;
        createGraphics();
        createAppearance();
    } else if (mUpdateAppearanceCounter != 0){
        mUpdateAppearanceCounter = 0;
        mUpdateEverythingCounter = 0;
        createAppearance();
    }
}

void PlotWidget::createGraphics()
{
    //NOTE: Very dirty trick (plotLayout()->clear() deletes items from a layout, but plots without axes
    //remains somewhere in the memory and QCustomPlot tries to draw them).
    for (auto graph : mGraphVector) {
        removePlottable(graph);
    }
    numberOfPreviousPlots = 0; // not used anymore
    mGraphVector.clear();

    QCPAxisRect *motherRect = new QCPAxisRect(this);

    auto axisModelVector = mLayoutModel->getVisibleAxisModelVector();
    for (int axisIndex = 0; axisIndex < axisModelVector.size(); ++axisIndex) {
        auto graphModelVector = axisModelVector[axisIndex]->getGraphModelVector();

        for (int modelIndex = 0; modelIndex < graphModelVector.size(); ++modelIndex) {
            QCPGraph *newGraph = new QCPGraph(motherRect->axis(QCPAxis::atBottom), motherRect->axis(QCPAxis::atLeft));
            newGraph->setData(graphModelVector[modelIndex]->getXData(), graphModelVector[modelIndex]->getYData());
            addPlottable(newGraph);
            mGraphVector.push_back(newGraph);
            numberOfPreviousPlots++;

            newGraph->setLayer("graph");

        }
    }
}



void PlotWidget::createAppearance()
{
    setBackground(QBrush(global::gDefaultPlotBackgroundColor));


    mState->setInteractions();

    mBottomRightLegendLayout = NULL;
    for (auto eventGraph : mEventsVector) {
        removePlottable(eventGraph);
    }
    mEventsVector.clear();

    for (auto textLabel : mTextLabelsVector) {
        removeItem(textLabel);
    }
    mTextLabelsVector.clear();

    for (auto arrow : mArrowsVector) {
        removeItem(arrow);
    }
    mArrowsVector.clear();

    for (auto levelGraph : mLevelsVector) {
        removePlottable(levelGraph);
    }
    mLevelsVector.clear();

    int currentGraphIndex = 0;

    plotLayout()->clear();


    //setting negative row spacing to make AxisRects closer
    if (mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Right || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Left
            || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Floating
            || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::TopCombined
            || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::BottomCombined) {

        plotLayout()->setRowSpacing(-10);
    } else {
        plotLayout()->setRowSpacing(0);
    }

    //for future
//    mGraphVector.clear();
//    mAxisRectVector.clear();
    //for future

    //to make legend closer
    plotLayout()->setColumnSpacing(-10);

    if (!mAxisLeftRightMarginGroup->isEmpty()) {
        mAxisLeftRightMarginGroup->clear();
    }

    //very dirty trick (plotLayout()->clear() deletes items from a layout, but plots without axes
    //        remains somewhere in the memory and QCustomPlot tries to draw them(WTF?)

//    for (int i = 0; i < numberOfPreviousPlots; ++i) {
//        removePlottable(0);
//    }
//    numberOfPreviousPlots = 0;








    //TITLE
//    if (mLayoutModel->title() != "") {
//        QCPPlotTitle *plotTitle = new QCPPlotTitle(this, mLayoutModel->title());
//        plotTitle->setTextColor(global::gDefaultPlotFontColor);
//        plotTitle->setFont(mLayoutModel->titleFont());
//        plotTitle->setMinimumMargins(QMargins(0, 0, 0 ,10));
//        if (mLayoutModel->areEventsVisible()) {
////            double verticalGap = mLayoutModel->commandFont().pointSize();
////            if (mLayoutModel->commandLabelRotation() != 0.) {
////                verticalGap += mLayoutModel->getMaximumCommandWidth()*abs(sin(mLayoutModel->commandLabelRotation()/57.3));
////            }
////            qDebug() << "vertical gap--------->" << verticalGap;
////            plotTitle->setMinimumMargins(QMargins(0, 0, 0 ,15 + verticalGap));

//            plotTitle->setMinimumMargins(QMargins(0, 0, 0 ,15 + mLayoutModel->commandFont().pointSize()));
//        }
////        addTitleToLayout(plotTitle);
//        plotLayout()->addElement(0, 0, plotTitle);

////        plotRowNumber++;
////        legendRowNumber++;
//    }
    addTitleToLayout();
    //TITLE


    auto axisModelVector = mLayoutModel->getVisibleAxisModelVector();
    for (int axisIndex = 0; axisIndex < axisModelVector.size(); ++axisIndex) {
//        QCPAxisRect *axisRect = mAxisRectVector[axisIndex];
        QCPAxisRect *axisRect = new QCPAxisRect(this);

        auto background = axisModelVector[axisIndex]->backgroundBrush();
        if (background.style() != Qt::NoBrush) {
            axisRect->setBackground(background);
        }

//        plotLayout()->addElement(axisIndex + (mLayoutModel->title() == "" ? 0 : 1), plotColumnNumber, axisRect);
        addAxisRectToLayout(axisIndex, axisRect);

        auto graphModelVector = axisModelVector[axisIndex]->getGraphModelVector();


        //dimension MAGIC
//        QCPLegend *arDimension = NULL;
//        if (mLayoutModel->isDimensionVisible()) {
//            arDimension = new QCPLegend;
//            arDimension->setFont(mLayoutModel->dimensionFont());
//            QCPLayoutGrid *dimensionLayout = new QCPLayoutGrid;
//            plotLayout()->addElement(axisIndex + (mLayoutModel->title() == "" ? 0 : 1), 0, dimensionLayout);
//            QCPLayoutElement *dummyElement = new QCPLayoutElement;
//            dimensionLayout->addElement(0,0,dummyElement);
//            dimensionLayout->addElement(1,0,arDimension);
//            dimensionLayout->addElement(2,0,dummyElement);
//            dimensionLayout->setRowStretchFactor(1, 0.01);
//            //To make a frame outside legend uncomment text below
////                 arLegend->setLayer("legend");
//        }
        //dimension MAGIC  END


        for (int modelIndex = 0; modelIndex < graphModelVector.size(); ++modelIndex) {
//            QCPGraph *newGraph = createGraphFromModelInAxes(graphModelVector[modelIndex], axisRect);
            QCPGraph *newGraph = mGraphVector[currentGraphIndex++];
            newGraph->setKeyAxis(axisRect->axis(QCPAxis::atBottom));
            newGraph->setValueAxis(axisRect->axis(QCPAxis::atLeft));
            const auto &model = graphModelVector[modelIndex];
            newGraph->setPen(model->getPen());
            newGraph->setVisible(model->isVisible());
            newGraph->setScatterStyle(model->getScatterStyle());
            newGraph->setLineStyle(model->getLineInterpolation());
            newGraph->setName(model->getName());
            newGraph->setBrush(model->brush());
            newGraph->setFillBasis(model->basisFillType());


//            addPlottable(newGraph);

        }


        auto xRange = axisModelVector[axisIndex]->getXRange();
        auto yRange = axisModelVector[axisIndex]->getYRange();
        axisRect->axis(QCPAxis::atBottom)->setRange(std::get<0>(xRange), std::get<1>(xRange));
        axisRect->axis(QCPAxis::atLeft)->setRange(std::get<0>(yRange), std::get<1>(yRange));

        //events
        if (mLayoutModel->areEventsVisible()) {

            QCPRange xRange = axisRect->axis(QCPAxis::atBottom)->range();
            QCPRange yRange = axisRect->axis(QCPAxis::atLeft)->range();
            double xRangeMin = xRange.lower;
            double xRangeMax = xRange.upper;
            double yCommandMin = yRange.lower;
            double yCommandMax = yRange.upper;

            double dxData = xRangeMax - xRangeMin;
            double commandDx = dxData / 10000;

            QVector<TimeEvent*> comVector = mLayoutModel->commandsVector();
            QVector<double> commandTickVector;
            QVector<QString> commandTickLabelsVector;
            for (TimeEvent* command : comVector) {
                QCPGraph *newEventGraph = new QCPGraph(axisRect->axis(QCPAxis::atBottom), axisRect->axis(QCPAxis::atLeft));
                newEventGraph->setKeyAxis(axisRect->axis(QCPAxis::atBottom));
                newEventGraph->setValueAxis(axisRect->axis(QCPAxis::atLeft));

//                newEventGraph->setData(QVector<double>({std::min(command->commandTime() - dxData, xRangeMin), command->commandTime(), command->commandTime() + commandDx, std::max(command->commandTime() + dxData, xRangeMax)}),
//                                         QVector<double>({yCommandMin - 3*(yCommandMax - yCommandMin), yCommandMin, yCommandMax, yCommandMax + 3*(yCommandMax - yCommandMin)}));

                newEventGraph->setData(QVector<double>({std::max(std::min(command->eventTime() - dxData, xRangeMin), command->eventTime() - command->brushDuration()),
                                                          command->eventTime(),
                                                          command->eventTime() + commandDx,
                                                          std::min(std::max(command->eventTime() + dxData, xRangeMax), command->eventTime() + command->brushDuration())}),
                                         QVector<double>({yCommandMin - 3*(yCommandMax - yCommandMin), yCommandMin - 3*(yCommandMax - yCommandMin), yCommandMax + 3*(yCommandMax - yCommandMin), yCommandMax + 3*(yCommandMax - yCommandMin)}));



                QPen pen;
                pen.setColor(global::gDefaultGridColor);
                pen.setWidthF(global::gEventLineWidth);
                pen.setStyle(Qt::DashDotLine);
                newEventGraph->setPen(pen);

                newEventGraph->setBrush(command->brush());
                if (command->brushBasis() == TimeEvent::Left) {
                    newEventGraph->setFillBasis(QCPGraph::HighBasis);
                } else {
                    newEventGraph->setFillBasis(QCPGraph::LowBasis);
                }

                addPlottable(newEventGraph);
                mEventsVector.push_back(newEventGraph);

                double commandLabelDeviation = command->labelDeviation()*(xRangeMax-xRangeMin) /
                        (width() - mLayoutModel->getMaximumLegendWidth() - mLayoutModel->getMaximumDimensionWidth());
                commandTickVector << (command->eventTime() + commandLabelDeviation);
                commandTickLabelsVector << command->name();
            }
            if (axisIndex == 0) {
//                axisRect->axis(QCPAxis::atTop)->setRange(std::get<0>(xRange), std::get<1>(xRange));
                axisRect->axis(QCPAxis::atTop)->setRange(xRangeMin, xRangeMax);
                axisRect->axis(QCPAxis::atTop)->setAutoTicks(false);
                axisRect->axis(QCPAxis::atTop)->setAutoTickLabels(false);
                axisRect->axis(QCPAxis::atTop)->setTickVector(commandTickVector);
                axisRect->axis(QCPAxis::atTop)->setTickVectorLabels(commandTickLabelsVector);
                axisRect->axis(QCPAxis::atTop)->setTickLabelFont(mLayoutModel->commandFont());
                axisRect->axis(QCPAxis::atTop)->setTickLabelRotation(mLayoutModel->commandLabelRotation());

            }
        }


        //levels
        if (axisModelVector[axisIndex]->areHorizontalLevelsVisible()) {
            QVector<double> levelTickVector;
            QVector<QString> levelTickLabelsVector;

            QCPRange xRange = axisRect->axis(QCPAxis::atBottom)->range();
            double dxData = xRange.upper - xRange.lower;

            QVector<HorizontalLevel*> levelVector = axisModelVector[axisIndex]->horizontalLevelVector();
            for (HorizontalLevel* level : levelVector) {

                QCPGraph *newLevelGraph = new QCPGraph(axisRect->axis(QCPAxis::atBottom), axisRect->axis(QCPAxis::atLeft));
                newLevelGraph->setKeyAxis(axisRect->axis(QCPAxis::atBottom));
                newLevelGraph->setValueAxis(axisRect->axis(QCPAxis::atLeft));

                newLevelGraph->setData(QVector<double>({xRange.lower - 2.*dxData, xRange.upper + 2.*dxData}),
                                         QVector<double>({level->level(), level->level()}));


                QPen pen;
                pen.setColor(global::gDefaultGridColor);
                pen.setWidthF(global::gEventLineWidth);
                pen.setStyle(Qt::DashDotLine);
                newLevelGraph->setPen(pen);

                addPlottable(newLevelGraph);
                mLevelsVector.push_back(newLevelGraph);

                levelTickVector << level->level();
                levelTickLabelsVector << level->label();
            }

//            axisRect->axis(QCPAxis::atTop)->setRange(std::get<0>(xRange), std::get<1>(xRange));
            axisRect->axis(QCPAxis::atRight)->setRange(axisRect->axis(QCPAxis::atLeft)->range());
            axisRect->axis(QCPAxis::atRight)->setAutoTicks(false);
            axisRect->axis(QCPAxis::atRight)->setAutoTickLabels(false);
            axisRect->axis(QCPAxis::atRight)->setTickVector(levelTickVector);
            axisRect->axis(QCPAxis::atRight)->setTickVectorLabels(levelTickLabelsVector);
            axisRect->axis(QCPAxis::atRight)->setTickLabelFont(mLayoutModel->commandFont());  //TODO: add normal treatment
            axisRect->axis(QCPAxis::atRight)->setTickLabelRotation(mLayoutModel->commandLabelRotation()); //TODO: add normal treatment

        }




        //DIMENSION
//        if (mLayoutModel->isDimensionVisible()) {
//            QString dimString("[");
//            foreach (auto dim, dimensionVector)
//                dimString += dim + ", ";
//            dimString.remove(dimString.size()-2, 2);
//            dimString += "]";
//            QCPGraph *dummyGraphic = new QCPGraph(axisRect->axis(QCPAxis::atBottom), axisRect->axis(QCPAxis::atLeft));
//            QPen dummyPen;
//            dummyPen.setStyle(Qt::NoPen);
//            dummyGraphic->setPen(dummyPen);
////                dummyGraphic->setLineStyle(Qt::NoPen);
//            dummyGraphic->setName(dimString);
//            arDimension->addItem(new QCPPlottableLegendItem(arDimension, dummyGraphic));
//         }



        //setting fonts for axis
        axisRect->axis(QCPAxis::atLeft)->setTickLabelFont(mLayoutModel->yAxisFont());
        axisRect->axis(QCPAxis::atBottom)->setTickLabelFont(mLayoutModel->xAxisFont());



        axisRect->axis(QCPAxis::atRight)->setVisible(true);
        axisRect->axis(QCPAxis::atRight)->setTickLabels(false);
        axisRect->axis(QCPAxis::atRight)->setTicks(false);
        axisRect->axis(QCPAxis::atTop)->setVisible(true);
        axisRect->axis(QCPAxis::atTop)->setTickLabels(false);
        axisRect->axis(QCPAxis::atTop)->setTicks(false);
        if ((mLayoutModel->areEventsVisible()) && (axisIndex == 0)) {
             axisRect->axis(QCPAxis::atTop)->setTickLabels(true);
             axisRect->axis(QCPAxis::atTop)->setTicks(true);
             axisRect->axis(QCPAxis::atTop)->setAutoSubTicks(false);
             axisRect->axis(QCPAxis::atTop)->setSubTickCount(0);
        }
        if (axisModelVector[axisIndex]->areHorizontalLevelsVisible()) {
             axisRect->axis(QCPAxis::atRight)->setTickLabels(true);
             axisRect->axis(QCPAxis::atRight)->setTicks(true);
             axisRect->axis(QCPAxis::atRight)->setAutoSubTicks(false);
             axisRect->axis(QCPAxis::atRight)->setSubTickCount(0);
        }
        axisRect->setMarginGroup(QCP::msLeft|QCP::msRight, mAxisLeftRightMarginGroup);

        axisRect->axis(QCPAxis::atLeft)->grid()->setSubGridVisible(true);
        axisRect->axis(QCPAxis::atBottom)->grid()->setSubGridVisible(true);

        QPen subGridPen =axisRect->axis(QCPAxis::atLeft)->grid()->subGridPen ();
        subGridPen.setWidthF(global::gSubGridPenWidth);
        subGridPen.setColor(global::gDefaultSubGridColor);
        subGridPen.setStyle(Qt::DashDotLine);
        axisRect->axis(QCPAxis::atLeft)->grid()->setSubGridPen(subGridPen);
        axisRect->axis(QCPAxis::atBottom)->grid()->setSubGridPen(subGridPen);

        QPen gridPen =axisRect->axis(QCPAxis::atLeft)->grid()->pen ();
        gridPen.setWidthF(global::gGridPenWidth);
        gridPen.setColor(global::gDefaultGridColor);
        gridPen.setStyle(Qt::SolidLine);
        axisRect->axis(QCPAxis::atLeft)->grid()->setPen(gridPen);
        axisRect->axis(QCPAxis::atBottom)->grid()->setPen(gridPen);
        axisRect->axis(QCPAxis::atBottom)->grid()->setPen(gridPen);
        axisRect->axis(QCPAxis::atLeft)->grid()->setZeroLinePen(gridPen);
        axisRect->axis(QCPAxis::atBottom)->grid()->setZeroLinePen(gridPen);



        // to put axis to the bottom layer //doesm't work
//        addLayer("belowmain", layer("main"), QCustomPlot::limBelow);
//        axisRect->axis(QCPAxis::atBottom)->setLayer("belowmain");

        //ticks
        // axisRect->axis(QCPAxis::atBottom)->setAutoTickCount(1`2);


        QFontMetrics xLabelFontMetrics(mLayoutModel->xAxisFont());
        int maxTickLabelWidth = xLabelFontMetrics.width("0") * (global::gDoubleToStringConversionPrecision+3);
        int tickXCount = (width() - mLayoutModel->getMaximumDimensionWidth() - mLayoutModel->getMaximumLegendWidth())
                / (maxTickLabelWidth*1.3*global::gXTickSpacing);
        axisRect->axis(QCPAxis::atBottom)->setAutoTickCount(tickXCount);

        //not optimal way to count it in each iteration, but it is easier to trace
        double sumStretchFactor = 0;
        foreach (auto axisModel, axisModelVector) {
            sumStretchFactor += axisModel->stretchFactor();
        }
        QFontMetrics yLabelFontMetrics(mLayoutModel->yAxisFont());
        int maxTickLabelHeight = yLabelFontMetrics.height();
        int tickYCount = (height() - mLayoutModel->getMaximumTitleHeight() - mLayoutModel->getMaximumXLabelHeight())
                * (axisModelVector[axisIndex]->stretchFactor())/sumStretchFactor
                / (maxTickLabelHeight*2.5*global::gYTickSpacing);
        axisRect->axis(QCPAxis::atLeft)->setAutoTickCount(std::max(tickYCount, 2));
//        axisRect->axis(QCPAxis::atLeft)->setAutoTickCount(
//                            std::max(int(height()*(axisModelVector[axisIndex]->stretchFactor())/sumStretchFactor/50), 2)
//                                                          );

        axisRect->axis(QCPAxis::atBottom)->setAutoSubTicks(false);
        axisRect->axis(QCPAxis::atBottom)->setSubTickCount(1);
        axisRect->axis(QCPAxis::atLeft)->setAutoSubTicks(false);
        axisRect->axis(QCPAxis::atLeft)->setSubTickCount(1);

        if (axisIndex != axisModelVector.size() - 1) {
            axisRect->axis(QCPAxis::atBottom)->setTickLabels(false);
        }
        //ticks

        auto axisPen = axisRect->axis(QCPAxis::atLeft)->basePen();
        axisPen.setWidthF(global::gAxisPenWidth);
        axisPen.setColor(global::gDefaultAxisRectColor);


//        axisRect->axis(QCPAxis::atLeft)->setBasePen(axisPen);
//        axisRect->axis(QCPAxis::atRight)->setBasePen(axisPen);
//        axisRect->axis(QCPAxis::atBottom)->setBasePen(axisPen);
//        axisRect->axis(QCPAxis::atTop)->setBasePen(axisPen);

        axisRect->axis(QCPAxis::atLeft)->setBasePen(axisPen);
        axisRect->axis(QCPAxis::atRight)->setBasePen(gridPen);
        axisRect->axis(QCPAxis::atBottom)->setBasePen(gridPen);
        axisRect->axis(QCPAxis::atTop)->setBasePen(gridPen);
        if (axisIndex == axisModelVector.size() - 1) {
            axisRect->axis(QCPAxis::atBottom)->setBasePen(axisPen);
        }




//        axisRect->axis(QCPAxis::atLeft)->setTickPen(axisPen);
//        axisRect->axis(QCPAxis::atRight)->setTickPen(axisPen);
//        axisRect->axis(QCPAxis::atBottom)->setTickPen(axisPen);
//        axisRect->axis(QCPAxis::atTop)->setTickPen(axisPen);
//        axisRect->axis(QCPAxis::atLeft)->setSubTickPen(axisPen);
//        axisRect->axis(QCPAxis::atRight)->setSubTickPen(axisPen);
//        axisRect->axis(QCPAxis::atBottom)->setSubTickPen(axisPen);
//        axisRect->axis(QCPAxis::atTop)->setSubTickPen(axisPen);

        axisRect->axis(QCPAxis::atLeft)->setTickPen(axisPen);
        axisRect->axis(QCPAxis::atRight)->setTickPen(axisPen);
        axisRect->axis(QCPAxis::atBottom)->setTickPen(gridPen);
        axisRect->axis(QCPAxis::atTop)->setTickPen(axisPen);
        axisRect->axis(QCPAxis::atLeft)->setSubTickPen(axisPen);
        axisRect->axis(QCPAxis::atRight)->setSubTickPen(axisPen);
        axisRect->axis(QCPAxis::atBottom)->setSubTickPen(gridPen);
        axisRect->axis(QCPAxis::atTop)->setSubTickPen(axisPen);
        if (axisIndex == axisModelVector.size() - 1) {
            axisRect->axis(QCPAxis::atBottom)->setTickPen(axisPen);
            axisRect->axis(QCPAxis::atBottom)->setSubTickPen(axisPen);

            axisRect->axis(QCPAxis::atBottom)->setTickLabelRotation(mLayoutModel->xTickRotation());
        }


        //debug
//        QPen aaa = axisRect->axis(QCPAxis::atTop)->subTickPen();
//        aaa.setColor(Qt::red);
//        axisRect->axis(QCPAxis::atTop)->setSubTickPen(aaa);
//        aaa = axisRect->axis(QCPAxi::atBottom)->subTickPen();
//        aaa.setColor(Qt::green);
//        axisRect->axis(QCPAxis::atBottom)->setSubTickPen(aaa);
        //debug


        //double to string conversions precision setting for tick labels
        axisRect->axis(QCPAxis::atLeft)->setNumberPrecision(global::gDoubleToStringConversionPrecision);
        axisRect->axis(QCPAxis::atBottom)->setNumberPrecision(global::gDoubleToStringConversionPrecision);

        if (axisModelVector[axisIndex]->scaleType() == AxisModel::ScaleType::Logarithmic) {
            axisRect->axis(QCPAxis::atLeft)->setScaleType(QCPAxis::stLogarithmic);
            axisRect->axis(QCPAxis::atRight)->setScaleType(QCPAxis::stLogarithmic);
        } else {
            axisRect->axis(QCPAxis::atLeft)->setScaleType(QCPAxis::stLinear);
            axisRect->axis(QCPAxis::atRight)->setScaleType(QCPAxis::stLinear);
        }

        if (mLayoutModel->scaleType() == PlotLayoutModel::ScaleType::Logarithmic) {
            axisRect->axis(QCPAxis::atBottom)->setScaleType(QCPAxis::stLogarithmic);
            axisRect->axis(QCPAxis::atTop)->setScaleType(QCPAxis::stLogarithmic);
        } else {
            axisRect->axis(QCPAxis::atBottom)->setScaleType(QCPAxis::stLinear);
            axisRect->axis(QCPAxis::atTop)->setScaleType(QCPAxis::stLinear);
        }



        //to make ticks stratch out of boxes
        axisRect->axis(QCPAxis::atBottom)->setTickLengthOut(4);



        //a piece of magic
        if (mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Right || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Left
                || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Floating
                || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::TopCombined
                || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::BottomCombined) {
            axisRect->setAutoMargins(QCP::msLeft|QCP::msRight|QCP::msBottom);
        } else {
            axisRect->setAutoMargins(QCP::msLeft|QCP::msRight|QCP::msBottom|QCP::msTop);
        }
        if (axisIndex == 0) {
            if (mLayoutModel->areEventsVisible()) {
//                axisRect->setMargins(QMargins(0, 10 + mLayoutModel->commandFont().pointSize(), 0, 0));
                double verticalGap = 0;
                if (mLayoutModel->commandLabelRotation() != 0.) {
                    verticalGap +=  mLayoutModel->getMaximumCommandWidth()*abs(sin(mLayoutModel->commandLabelRotation()/57.3));
                }
                if (mLayoutModel->title() == "") {
                    axisRect->setMargins(QMargins(0, 10 + mLayoutModel->commandFont().pointSize() + verticalGap, 0, 0));
                } else {
                    axisRect->setMargins(QMargins(0, 5 + verticalGap, 0, 0));
                }
            } else {
                axisRect->setMargins(QMargins(0, 5, 0, 0));
            }
        } else {
            axisRect->setMargins(QMargins(0, 0, 0, 0));
        }

        //xLabel
        if (axisIndex == axisModelVector.size() - 1 && mLayoutModel->xLabel() != "") {
            axisRect->axis(QCPAxis::atBottom)->setLabel(mLayoutModel->xLabel());
            axisRect->axis(QCPAxis::atBottom)->setLabelFont(mLayoutModel->xLabelFont());
            axisRect->axis(QCPAxis::atBottom)->setTickLabelType(mLayoutModel->timeAxisType());
            axisRect->axis(QCPAxis::atBottom)->setLabelColor(global::gDefaultPlotFontColor);
        }

        //DIMENSION
        if (mLayoutModel->areUnitsVisible()) {
            QString dimString = axisModelVector[axisIndex]->getUnitsString();
            axisRect->axis(QCPAxis::atLeft)->setLabel(dimString);
            axisRect->axis(QCPAxis::atLeft)->setLabelFont(mLayoutModel->dimensionFont());
            axisRect->axis(QCPAxis::atLeft)->setLabelColor(global::gDefaultPlotFontColor);
            QFontMetrics fm(mLayoutModel->dimensionFont());
            axisRect->axis(QCPAxis::atLeft)->setLabelPadding(fm.width(dimString));
         }

        //stretch y axis
//        plotLayout()->setRowStretchFactor(axisIndex + (mLayoutModel->title() == "" ? 0 : 1),
//                                          axisModelVector[axisIndex]->stretchFactor());


        //setting invisible ticks for commands
        if (mLayoutModel->areEventsVisible()) {
            if (axisIndex == 0) {
                QPen invPen(QColor(250,250,250,0));
                axisRect->axis(QCPAxis::atTop)->setTickPen(invPen);
            }
        }


        //arrowedText
        auto arrowedTextVector = axisModelVector[axisIndex]->arrowedTextVector();
        for (auto arrowedText : arrowedTextVector) {
            QCPItemText *textLabel = new QCPItemText(this);
            addItem(textLabel);
            textLabel->setClipToAxisRect (true);
            textLabel->setClipAxisRect (axisRect);
            textLabel->position->setAxes(axisRect->axis(QCPAxis::atBottom), axisRect->axis(QCPAxis::atLeft));
            textLabel->setPositionAlignment(Qt::AlignVCenter|Qt::AlignHCenter);

            textLabel->position->setType(QCPItemPosition::ptPlotCoords);
            textLabel->position->setCoords(arrowedText->textCenter());
            textLabel->setText(arrowedText->text());
            textLabel->setFont(mLayoutModel->arrowedTextFont());
            textLabel->setColor(global::gDefaultPlotFontColor);
            QPen arrowPen(global::gDefaultPlotGraphicalPrimitivesColor);
            arrowPen.setWidthF(arrowedText->lineWidthF());
            textLabel->setPen(arrowPen);
            textLabel->setSelectable(false);
            QMargins margins(arrowedText->lineWidthF() + 1, arrowedText->lineWidthF() + 1,
                             arrowedText->lineWidthF() + 1, arrowedText->lineWidthF() + 1);
            textLabel->setPadding(margins);
            textLabel->setBrush(arrowedText->backgroundColor());


            QCPItemLine *arrow = new QCPItemLine(this);
            addItem(arrow);
            arrow->setClipToAxisRect (true);
            arrow->setClipAxisRect (axisRect);
            arrow->start->setAxes(axisRect->axis(QCPAxis::atBottom), axisRect->axis(QCPAxis::atLeft));
            arrow->end->setAxes(axisRect->axis(QCPAxis::atBottom), axisRect->axis(QCPAxis::atLeft));
            arrow->start->setType(QCPItemPosition::ptPlotCoords);
            arrow->end->setType(QCPItemPosition::ptPlotCoords);
            arrow->end->setCoords(arrowedText->arrowFinish());
            arrow->setHead(arrowedText->arrowStyle());
            arrow->setPen(arrowPen);

            switch (arrowedText->arrowBasisInRanges(axisRect->axis(QCPAxis::atBottom)->range().size(), axisRect->axis(QCPAxis::atLeft)->range().size())) {
                case ArrowedText::ArrowBasis::Right:
                    arrow->start->setParentAnchor(textLabel->right);
                    break;
                case ArrowedText::ArrowBasis::Left:
                    arrow->start->setParentAnchor(textLabel->left);
                    break;
                case ArrowedText::ArrowBasis::Top:
                    arrow->start->setParentAnchor(textLabel->top);
                    break;
                case ArrowedText::ArrowBasis::Bottom:
                    arrow->start->setParentAnchor(textLabel->bottom);
                    break;
                default:
                    arrow->start->setParentAnchor(textLabel->bottom);
                    break;
            };
            arrow->setSelectable(false);


            mTextLabelsVector.push_back(textLabel);
            mArrowsVector.push_back(arrow);
        }



    }

    for (int i = 0; i < axisRectCount(); ++i) {
        axisRect(i)->axis(QCPAxis::atBottom)->setTickLabelColor(global::gDefaultPlotFontColor);
        axisRect(i)->axis(QCPAxis::atLeft)->setTickLabelColor(global::gDefaultPlotFontColor);
        axisRect(i)->axis(QCPAxis::atTop)->setTickLabelColor(global::gDefaultPlotFontColor);
        axisRect(i)->axis(QCPAxis::atBottom)->setTickLabelColor(global::gDefaultPlotFontColor);
    }


    for (int i = 0; i < axisRectCount(); ++i) {
        connect(axisRect(i)->axis(QCPAxis::atBottom), SIGNAL(rangeChanged(QCPRange)),
                this, SLOT(setXRangeForAllAxis(QCPRange)));
    }


    //NOTE: should be called after graph properties(brush, pen..) are set
    addLegendToLayout();

    //LEGEND
    //shrinking legend size
//    if (mLayoutModel->isLegendVisible() && axisModelVector.size() > 0) {
//        plotLayout()->setColumnStretchFactor(legendColumnNumber /*+ (mLayoutModel->isDimensionVisible() ? 1: 0)*/, 0.01);
//    }
    //LEGEND

    //DIMENSION
//    if (mLayoutModel->isDimensionVisible()) {
//        plotLayout()->setColumnStretchFactor(0, 0.01);
//    }
    //DIMENSION


//    if (axisRectCount() > 0) {
//        setInteractions(QCP::iRangeDrag);
//        axisRect(0)->setRangeDrag(Qt::Vertical);
//    }



    recountLastAxisStretch();

    updateTracers();
    updateAnchors();

    replot();



}

int PlotWidget::getIndexOfAxisRectByPoint(const QPoint &point)
{
    int x_coord = point.x();
    int y_coord = point.y();
    for (int axisRectIndex = 0; axisRectIndex < axisRectCount(); ++axisRectIndex) {
        QRect axisRec = axisRect(axisRectIndex)->rect();
        if (((x_coord >= axisRec.x()) && (x_coord <= axisRec.x()+ axisRec.width()))
            && ((y_coord >= axisRec.y()) && (y_coord <= axisRec.y()+ axisRec.height()))) {
                return axisRectIndex;
        }
    }
    return -1;
}

void PlotWidget::increaseWidthOfObjects(double widDev, double lineWidthDev)
{
    for (int axisIndex = 0; axisIndex < axisRectCount(); ++axisIndex) {
        QPen subGridPen =axisRect(axisIndex)->axis(QCPAxis::atLeft)->grid()->subGridPen ();
        subGridPen.setWidthF(subGridPen.widthF() + widDev);
        axisRect(axisIndex)->axis(QCPAxis::atLeft)->grid()->setSubGridPen(subGridPen);
        axisRect(axisIndex)->axis(QCPAxis::atBottom)->grid()->setSubGridPen(subGridPen);

        QPen gridPen =axisRect(axisIndex)->axis(QCPAxis::atLeft)->grid()->pen ();
        gridPen.setWidthF(gridPen.widthF() + widDev);
        axisRect(axisIndex)->axis(QCPAxis::atLeft)->grid()->setPen(gridPen);
        axisRect(axisIndex)->axis(QCPAxis::atBottom)->grid()->setPen(gridPen);

        auto axisPen = axisRect(axisIndex)->axis(QCPAxis::atLeft)->basePen();
        axisPen.setWidthF(axisPen.widthF() + widDev);
        axisRect(axisIndex)->axis(QCPAxis::atLeft)->setBasePen(axisPen);
        axisRect(axisIndex)->axis(QCPAxis::atRight)->setBasePen(gridPen);
        axisRect(axisIndex)->axis(QCPAxis::atBottom)->setBasePen(gridPen);
        axisRect(axisIndex)->axis(QCPAxis::atTop)->setBasePen(gridPen);
        if (axisIndex == axisRectCount() - 1) {
            axisRect(axisIndex)->axis(QCPAxis::atBottom)->setBasePen(axisPen);
        }
    }

    for (auto graph : mGraphVector) {
        auto graphPen = graph->pen();
        graphPen.setWidthF(graphPen.widthF() + lineWidthDev);
        graph->setPen(graphPen);
    }

    for (auto command : mEventsVector) {
        auto commandPen = command->pen();
        commandPen.setWidthF(commandPen.widthF() + lineWidthDev);
        command->setPen(commandPen);
    }


}

void PlotWidget::recountLastAxisStretch()
{
    //TODO: consider all possible cases of title and axis label position

    if (mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Right
            || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Left
            || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Floating) {

        auto axisModelVector = mLayoutModel->getVisibleAxisModelVector();
        if (axisModelVector.size() == 0)
            return;
        if (axisModelVector.size() == 1)
            return;

        QVector<double> stretchFactors;
        double sumStretch = 0;
        for (const auto & axis : axisModelVector) {
            stretchFactors.push_back(axis->stretchFactor());
            sumStretch += stretchFactors.back();
        }

        double bottomStuffSize = QFontMetrics(mLayoutModel->xAxisFont()).height();
        if (mLayoutModel->xLabel() != "") {
            bottomStuffSize += 10 + QFontMetrics(mLayoutModel->xLabelFont()).height();
        }

        double titleStuffSize = 0.;
        if (mLayoutModel->title() != "") {
            titleStuffSize += QFontMetrics(mLayoutModel->titleFont()).height();
        }

        double topStuffSize = 0.;
        if (mLayoutModel->areEventsVisible()) {
            topStuffSize += 5 + QFontMetrics(mLayoutModel->commandFont()).height();
        }

        double availableHeight = height() - titleStuffSize;

        double heightForGraphs = availableHeight - topStuffSize - bottomStuffSize;

        double desirableTopGraphHeight = heightForGraphs * stretchFactors.front() / sumStretch;
        double desirableBottomGraphHeight = heightForGraphs * stretchFactors.back() / sumStretch;

        if (stretchFactors.size() == 2) {
            plotLayout()->setRowStretchFactor(0 + (mLayoutModel->title() == "" ? 0 : 1), 1.);
            plotLayout()->setRowStretchFactor(stretchFactors.size() - 1 + (mLayoutModel->title() == "" ? 0 : 1), desirableBottomGraphHeight/desirableTopGraphHeight);
            return;
        }

        double topGraphPlusTopStuff = desirableTopGraphHeight + topStuffSize;
        double bottomGraphPlusBottomStuff = desirableBottomGraphHeight + bottomStuffSize;

        double k = bottomGraphPlusBottomStuff / topGraphPlusTopStuff;
        double ostStretch = sumStretch - stretchFactors.front() - stretchFactors.back();

        double zhelTopFactor = (topGraphPlusTopStuff)*ostStretch/(availableHeight - desirableTopGraphHeight - desirableTopGraphHeight*k);
        double zhelBottomFactor = zhelTopFactor * k;

        if (zhelTopFactor < stretchFactors.front())
            zhelTopFactor = stretchFactors.front();
        if (zhelBottomFactor < stretchFactors.back())
            zhelBottomFactor = stretchFactors.back();

    //    qDebug() << "zhelTopFactor" << zhelTopFactor;
    //    qDebug() << "zhelBottomFactor" << zhelBottomFactor;

        plotLayout()->setRowStretchFactor(0 + (mLayoutModel->title() == "" ? 0 : 1), zhelTopFactor);
        plotLayout()->setRowStretchFactor(stretchFactors.size() - 1 + (mLayoutModel->title() == "" ? 0 : 1), zhelBottomFactor);

        if (mBottomRightLegendLayout) {
            double x1 = desirableBottomGraphHeight/2.;
            double x2 = desirableBottomGraphHeight/2. + bottomStuffSize;
            mBottomRightLegendLayout->setRowStretchFactor(0, x1);
            mBottomRightLegendLayout->setRowStretchFactor(2, x2);
        }

    }



    return;
}

void PlotWidget::addAxisRectToLayout(int axisNumber, QCPLayoutElement *element)
{
    int axisRowNumber = (mLayoutModel->title() == "" ? 0 : 1);
    int plotColumnNumber = -1;
    switch (mLayoutModel->legendLayout()) {
        case PlotLayoutModel::LegendLocation::Right:
        case PlotLayoutModel::LegendLocation::Floating:
            axisRowNumber += axisNumber * 1;
            plotColumnNumber = 0;
            break;
        case PlotLayoutModel::LegendLocation::Left:
            axisRowNumber += axisNumber * 1;
            plotColumnNumber = 1;
            break;
        case PlotLayoutModel::LegendLocation::TopSeparate:
            axisRowNumber += 1 + axisNumber * 2;
            plotColumnNumber = 0;
            break;
        case PlotLayoutModel::LegendLocation::BottomSeparate:
            axisRowNumber += axisNumber * 2;
            plotColumnNumber = 0;
            break;
        case PlotLayoutModel::LegendLocation::TopCombined:
            axisRowNumber += 1 + axisNumber * 1;
            plotColumnNumber = 0;
            break;
        case PlotLayoutModel::LegendLocation::BottomCombined:
            axisRowNumber += axisNumber * 1;
            plotColumnNumber = 0;
            break;
        default:
            break;
    }
    plotLayout()->addElement(axisRowNumber, plotColumnNumber, element);

    //stretch y axis
    auto axisModelVector = mLayoutModel->getVisibleAxisModelVector();
    plotLayout()->setRowStretchFactor(axisRowNumber,
                                      axisModelVector[axisNumber]->stretchFactor());
}

void PlotWidget::addLegendRectToLayout(int /*legendNumber*/, QCPLayoutElement */*element*/)
{

}

void PlotWidget::addLegendToLayout()
{
//    if (mLayoutModel->getVisibleAxisModelVector().size() > 0) {
//        legend = new QCPLegend;
//        legend = new QCPLegend;
//        legend->setVisible(true);
//        defaultAxisRect->insetLayout()->addElement(legend, Qt::AlignRight|Qt::AlignTop);
//        defaultAxisRect->insetLayout()->setMargins(QMargins(12, 12, 12, 12));
//        return;
//    }

//        if (mLayoutModel->getVisibleAxisModelVector().size() > 0) {
//            QCPLegend *arLegend = new QCPLegend;
//            axisRect(0)->insetLayout()->addElement(arLegend, Qt::AlignRight|Qt::AlignTop);
//            auto vec = mLayoutModel->getVisibleAxisModelVector();

//            arLegend->addItemVer(new QCPPlottableLegendItem(arLegend, mGraphVector[0]));

//            arLegend->setVisible(true);
//            return;
//        }
//    return;

    if (mLayoutModel->isLegendVisible()) {
        int legendRowNumber = (mLayoutModel->title() == "" ? 0 : 1);
        int legendColumnNumber = -1;
        int legendRowNumberIncrement = 1;
        switch (mLayoutModel->legendLayout()) {
            case PlotLayoutModel::LegendLocation::Right:
            case PlotLayoutModel::LegendLocation::Floating:
                 legendColumnNumber = 1;
                 legendRowNumberIncrement = 1;
                 break;
            case PlotLayoutModel::LegendLocation::Left:
                 legendColumnNumber = 0;
                 legendRowNumberIncrement = 1;
                 break;
            case PlotLayoutModel::LegendLocation::TopSeparate:
                 legendColumnNumber = 0;
                 legendRowNumberIncrement = 2;
                 break;
            case PlotLayoutModel::LegendLocation::BottomSeparate:
                 legendRowNumber++;
                 legendColumnNumber = 0;
                 legendRowNumberIncrement = 2;
                 break;
            case PlotLayoutModel::LegendLocation::TopCombined:
                 legendColumnNumber = 0;
                 legendRowNumberIncrement = 0;
                 break;
            case PlotLayoutModel::LegendLocation::BottomCombined:
                 legendRowNumber += mLayoutModel->getVisibleAxisModelVector().size();
                 legendColumnNumber = 0;
                 legendRowNumberIncrement = 0;
                 break;
            default:
                 break;
        }

        int currentGraphIndex = 0;

        QCPLegend *arLegend = NULL;
        auto axisModelVector = mLayoutModel->getVisibleAxisModelVector();
        for (int axisIndex = 0; axisIndex < axisModelVector.size(); ++axisIndex) {
            auto graphModelVector = axisModelVector[axisIndex]->getGraphModelVector();

            if (mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::TopCombined
                    || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::BottomCombined) {
                if (axisIndex == 0) {
                    arLegend = new QCPLegend;
                }
            } else {
                arLegend = new QCPLegend;
            }

            arLegend->setFont(mLayoutModel->legendFont());
            arLegend->setTextColor(global::gDefaultPlotFontColor);

            QCPLayoutGrid *legendLayout = new QCPLayoutGrid;

            if (mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Floating) {
                axisRect(axisIndex)->insetLayout()->addElement(arLegend, Qt::AlignRight|Qt::AlignTop);
                axisRect(axisIndex)->insetLayout()->setMargins(QMargins(12, 12, 12, 12));
            } else {
                plotLayout()->addElement(legendRowNumber, legendColumnNumber, legendLayout);
            }

            switch (mLayoutModel->legendLayout()) {
                case PlotLayoutModel::LegendLocation::Right:
                case PlotLayoutModel::LegendLocation::Left:
                    {
                    QCPLayoutElement *dummyElement = new QCPLayoutElement;
                    legendLayout->addElement(0,0,dummyElement);
                    legendLayout->addElement(1,0,arLegend);
                    legendLayout->addElement(2,0,dummyElement);
                    legendLayout->setRowStretchFactor(1, 0.01);
                    if (mLayoutModel->isLegendFrameVisible()) {
                        arLegend->setLayer(QLatin1String("legend"));
                    }
                    }
                    break;
                case PlotLayoutModel::LegendLocation::TopSeparate:
                case PlotLayoutModel::LegendLocation::BottomSeparate:
                    {
                    QCPLayoutElement *dummyElement = new QCPLayoutElement;
                    legendLayout->addElement(0,0,dummyElement);
                    legendLayout->addElement(0,1,arLegend);
                    legendLayout->addElement(0,2,dummyElement);
                    legendLayout->setColumnStretchFactor(1, 0.01);
                    plotLayout()->setRowStretchFactor(legendRowNumber /*+ (mLayoutModel->isDimensionVisible() ? 1: 0)*/, 0.01);
                    if (mLayoutModel->isLegendFrameVisible()) {
                        arLegend->setLayer(QLatin1String("legend"));
                    }
                    }
                     break;
                case PlotLayoutModel::LegendLocation::TopCombined:
                case PlotLayoutModel::LegendLocation::BottomCombined:
                    if (axisIndex == 0){
                        QCPLayoutElement *dummyElement = new QCPLayoutElement;
                        legendLayout->addElement(0,0,dummyElement);
                        legendLayout->addElement(0,1,arLegend);
                        legendLayout->addElement(0,2,dummyElement);
                        legendLayout->setColumnStretchFactor(1, 0.01);
                        plotLayout()->setRowStretchFactor(legendRowNumber /*+ (mLayoutModel->isDimensionVisible() ? 1: 0)*/, 0.01);
                        //To make a frame outside legend uncomment text below
                        //NOTE: for TopCombined and BottomCombined this code should be called only once!
                        if (mLayoutModel->isLegendFrameVisible()) {
                            arLegend->setLayer(QLatin1String("legend"));
                        }
                    }
                    break;
                case PlotLayoutModel::LegendLocation::Floating:
                    if (mLayoutModel->isLegendFrameVisible()) {
                        arLegend->setLayer(QLatin1String("legend"));
                    }
                     break;
            }

            if (axisIndex == axisModelVector.size() - 1) {
                mBottomRightLegendLayout = legendLayout;
            }




            for (int modelIndex = 0; modelIndex < graphModelVector.size(); ++modelIndex) {
    //            QCPGraph *newGraph = createGraphFromModelInAxes(graphModelVector[modelIndex], axisRect);
                QCPGraph *newGraph = mGraphVector[currentGraphIndex++];
                const auto &model = graphModelVector[modelIndex];

                //NOTE: newGraph properties (pen, brush...) should already be defined
                    if (model->isLegendable()) {
                        switch (mLayoutModel->legendLayout()) {
                            case PlotLayoutModel::LegendLocation::Right:
                            case PlotLayoutModel::LegendLocation::Left:
                            case PlotLayoutModel::LegendLocation::Floating:
                                 arLegend->addItemVer(new QCPPlottableLegendItem(arLegend, newGraph));
                                 break;
                            case PlotLayoutModel::LegendLocation::TopSeparate:
                            case PlotLayoutModel::LegendLocation::BottomSeparate:
                                 arLegend->addItemHor(new QCPPlottableLegendItem(arLegend, newGraph));
                                 break;
                            case PlotLayoutModel::LegendLocation::TopCombined:
                            case PlotLayoutModel::LegendLocation::BottomCombined:
                                 arLegend->addElement(axisIndex, modelIndex, new QCPPlottableLegendItem(arLegend, newGraph));
                                 break;

                        }
//                        arLegend->addItemVer(new QCPPlottableLegendItem(arLegend, newGraph));
    //                    arLegend->addItemHor(new QCPPlottableLegendItem(arLegend, newGraph));
                    }
            }
            legendRowNumber += legendRowNumberIncrement;

        }

        //shrinking legend size
        if (axisModelVector.size() > 0
           && (mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Right
              || mLayoutModel->legendLayout() == PlotLayoutModel::LegendLocation::Left)) {
            plotLayout()->setColumnStretchFactor(legendColumnNumber /*+ (mLayoutModel->isDimensionVisible() ? 1: 0)*/, 0.01);
        }

    }
}

void PlotWidget::addTitleToLayout()
{
    if (mLayoutModel->title() != "") {
        QCPPlotTitle *plotTitle = new QCPPlotTitle(this, mLayoutModel->title());
        plotTitle->setTextColor(global::gDefaultPlotFontColor);
        plotTitle->setFont(mLayoutModel->titleFont());
        plotTitle->setMinimumMargins(QMargins(0, 0, 0 ,10));
        if (mLayoutModel->areEventsVisible()) {
//            double verticalGap = mLayoutModel->commandFont().pointSize();
//            if (mLayoutModel->commandLabelRotation() != 0.) {
//                verticalGap += mLayoutModel->getMaximumCommandWidth()*abs(sin(mLayoutModel->commandLabelRotation()/57.3));
//            }
//            qDebug() << "vertical gap--------->" << verticalGap;
//            plotTitle->setMinimumMargins(QMargins(0, 0, 0 ,15 + verticalGap));

            plotTitle->setMinimumMargins(QMargins(0, 0, 0 ,15 + mLayoutModel->commandFont().pointSize()));
        }

        switch (mLayoutModel->legendLayout()) {
            case PlotLayoutModel::LegendLocation::Left:
                plotLayout()->addElement(0, 1, plotTitle);
                break;
            default:
                plotLayout()->addElement(0, 0, plotTitle);
                break;
        }

    }
}



