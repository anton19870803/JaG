#include "jagmainwindow.h"
#include "ui_jagmainwindow.h"
#include "datasource/datachoicedialog.h"
#include "basic/basic.h"
#include <QFileDialog>
#include <QToolBar>
#include <QMessageBox>
#include "commonWidgets/preferencesdialog.h"
#include <QSettings>
#include "datasource/datasourcebasic.h"
#include <QClipboard>
#include <QMimeData>
#include <QDebug>
#include <QUrl>
#include <QList>
#include <QSplashScreen>
#include <QPrinter>
#include <QPrintDialog>
#include <algorithm>
#include "global/global_definitions.h"
#include "commonWidgets/tipdialog.h"
#include <QTcpSocket>
#include <QLabel>
#include "datasource/datasourceadapter.h"
#include "event/eventsourceadapter.h"
#include "event/eventsource.h"
#include "datasource/simpledatasource.h"
#include "commonWidgets/aboutjag.h"
#include "datasource/datasourcecontroldialog.h"
#include <QToolButton>
#include "commonWidgets/noncriticalerrorsdialogwithinfo.h"
#include "tablePrinter/tableprinter.h"


JagMainWindow::JagMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::JagMainWindow), mPreferencesDialog(NULL), mDockAnchorViewWidget(NULL), mIsMeasuringValueTableVisible(false),
    mDataSourceEditingDialog(NULL), mDefaultDataSourceDir(".")
{

    mListeningServer =  new QTcpServer(this);
    if (!mListeningServer->listen(QHostAddress("127.0.0.1"), global::gExternalDataSourcePortNumber)) {
        qDebug() << "can't create mListeningServer" ;
    }
    qDebug() << "QTcpServer was established in JagMainWindow";
    connect(mListeningServer, SIGNAL(newConnection()),
            this,  SLOT(slotNewConnection()));



    setAcceptDrops(true);
    ui->setupUi(this);

    ui->actionLoad_data_source->setText(tr("Load data source"));
    ui->actionEdit_data_sources->setText(tr("Edit data sources"));



//    QToolBar *plotToolBar = new QToolBar;
//    plotToolBar->addAction(ui->actionAdd_graph);
//    plotToolBar->addAction(ui->actionAdd_event);
//    plotToolBar->addAction(ui->actionCombine_graphs);
//    plotToolBar->addAction(ui->actionSeparate_graphs);
//    plotToolBar->addAction(ui->actionIntellectual_layout);
//    addToolBar(plotToolBar);


//    QToolBar *zoomToolBar = new QToolBar;
//    zoomToolBar->addAction(ui->actionNormal_mode);
//    zoomToolBar->addAction(ui->actionMeasuring);
//    zoomToolBar->addAction(ui->actionShow_value_table);
//    zoomToolBar->addAction(ui->actionDraw_level);
//    zoomToolBar->addAction(ui->actionArrow_with_text);
//    zoomToolBar->addAction(ui->actionFast_secondary_processing);
//    zoomToolBar->addAction(ui->actionZoom_X);
//    zoomToolBar->addAction(ui->actionZoom_Y);
//    zoomToolBar->addAction(ui->actionZoom_XY);
//    zoomToolBar->addAction(ui->actionMotion);
//    zoomToolBar->addAction(ui->actionPrevious_scope);
//    zoomToolBar->addAction(ui->actionNext_scope);
//    zoomToolBar->addAction(ui->actionAutoscale);
//    addToolBar(zoomToolBar);

//    QToolBar *processingToolBar = new QToolBar;
//    processingToolBar->addAction(ui->actionSecondary_processing);
//    processingToolBar->addAction(ui->actionAuto_units);
//    processingToolBar->addAction(ui->actionFull_screen);
//    addToolBar(processingToolBar);

//    QToolBar *optionsToolBar = new QToolBar;
//    optionsToolBar->addAction(ui->actionPreferences);
//    addToolBar(optionsToolBar);

//    QToolBar *helpToolBar = new QToolBar;
//    helpToolBar->addAction(ui->actionTips);
//    addToolBar(helpToolBar);



//    auto newGraphTool = addToolBar("New Graph");
//    newGraphTool->addAction(ui->actionAdd_graph);
//    auto autoscaleTool = addToolBar("Autoscale");
//    autoscaleTool->addAction(ui->actionAutoscale);



    connect(ui->tabWidget, SIGNAL(tabCloseRequested(int)),
            this, SLOT(treatTabCloseRequest(int)));
    connect(ui->tabWidget, SIGNAL(tabMoved(int, int)),
            this, SLOT(treatTabMotionRequest(int, int)));
    connect(ui->tabWidget, SIGNAL(currentChanged(int)),
            this, SLOT(treatCurrentTabChanged(int)));


    connect(ui->actionLoad_data_source, SIGNAL(triggered()),
            this, SLOT(addDataSource()));
    connect(ui->actionEdit_data_sources, SIGNAL(triggered()),
            this, SLOT(editDataSources()));
    connect(ui->actionSave_global_command_source_as, SIGNAL(triggered()),
            this, SLOT(saveGlobalEventSourceAs()));
    ui->actionSave_global_command_source_as->setEnabled(false);
    connect(ui->actionSave_data_in_table, SIGNAL(triggered()),
            this, SLOT(saveDataInTable()));

    connect(ui->actionNew_plot, SIGNAL(triggered()),
            this, SLOT(addNewPlot()));
    connect(ui->actionAdd_graph, SIGNAL(triggered()),
            this, SLOT(addNewGraph()));
    connect(ui->actionAdd_event, SIGNAL(triggered()),
            this, SLOT(addNewEvent()));
    connect(ui->actionCombine_graphs, SIGNAL(triggered()),
            this, SLOT(combineGraphs()));
    connect(ui->actionSeparate_graphs, SIGNAL(triggered()),
            this, SLOT(separateGraphs()));
    connect(ui->actionIntellectual_layout, SIGNAL(triggered()),
            this, SLOT(composeIntellectually()));


    connect(ui->actionSave_as, SIGNAL(triggered()),
            this, SLOT(saveAs()));
    connect(ui->actionQuick_save, SIGNAL(triggered()),
            this, SLOT(quickSave()));
    connect(ui->actionCopy, SIGNAL(triggered()),
            this, SLOT(copyToClipBoard()));
    connect(ui->actionPrint, SIGNAL(triggered()),
            this, SLOT(print()));


    connect(ui->actionMeasuring, SIGNAL(triggered()),
            this, SLOT(setMeasuring()));
    connect(ui->actionArrow_with_text, SIGNAL(triggered()),
            this, SLOT(setArrowDrawingMode()));
    connect(ui->actionZoom_X, SIGNAL(triggered()),
            this, SLOT(setZoomX()));
    connect(ui->actionZoom_Y, SIGNAL(triggered()),
            this, SLOT(setZoomY()));
    connect(ui->actionZoom_XY, SIGNAL(triggered()),
            this, SLOT(setZoomXY()));
    connect(ui->actionAutoscale, SIGNAL(triggered()),
            this, SLOT(autoscale()));
    connect(ui->actionNormal_mode, SIGNAL(triggered()),
            this, SLOT(setNormalMode()));
    connect(ui->actionMotion, SIGNAL(triggered()),
            this, SLOT(setMotionMode()));
    connect(ui->actionDraw_level, SIGNAL(triggered()),
            this, SLOT(setLevelDrawing()));
    connect(ui->actionFast_secondary_processing, SIGNAL(triggered()),
            this, SLOT(setFastSecondaryProcessingMode()));
    connect(ui->actionPrevious_scope, SIGNAL(triggered()),
            this, SLOT(prevScope()));
    connect(ui->actionNext_scope, SIGNAL(triggered()),
            this, SLOT(nextScope()));

    connect(ui->actionShow_value_table, SIGNAL(toggled ( bool )),
            this, SLOT(showMeasuringTable(bool)));


    connect(ui->actionSecondary_processing, SIGNAL(triggered()),
            this, SLOT(secondaryProcessing()));
    connect(ui->actionAuto_units, SIGNAL(triggered()),
            this, SLOT(setAutoUnits()));
    connect(ui->actionCopy_names_to_clipboard, SIGNAL(triggered()),
            this, SLOT(copyNamesToInternalClipboard()));
    connect(ui->actionFull_screen, SIGNAL(triggered()),
            this, SLOT(toogleFullScreen()));


    connect(ui->actionPreferences, SIGNAL(triggered()),
            this, SLOT(setPreferences()));

    connect(ui->actionAbout_Qt, SIGNAL(triggered()),
            this, SLOT(aboutQt()));
    connect(ui->actionAbout_JaG, SIGNAL(triggered()),
            this, SLOT(aboutJag()));
    connect(ui->actionTips, SIGNAL(triggered()),
            this, SLOT(showHelpInformation()));

    connect(ui->tabWidget, SIGNAL(tabCloseRequested(int)),
            this, SLOT(setPlotEditingEnabled()));

    setPlotEditingEnabled();

    ui->actionNormal_mode->setChecked(true);
    mStatusMessageLabel = new MessageLabel;
    statusBar()->insertWidget (0, mStatusMessageLabel);

    global::gGlobalDataSource = new SimpleDataSource();
    connect(global::gGlobalDataSource, SIGNAL(simpleDataSourceChanged()),
            this, SLOT(treatGlobalDataSourceChanging()));

    global::gGlobalEventSource = new EventSource();
    connect(global::gGlobalEventSource, SIGNAL(eventSourceChanged()),
            this, SLOT(treatGlobalEventSourceChanging()));

    loadSettings();

    mDockAnchorViewWidget = new DockAnchorViewWidget(this);
    mDockAnchorViewWidget->setVisible(mIsMeasuringValueTableVisible );
    ui->actionShow_value_table->setChecked(mIsMeasuringValueTableVisible);
    mDockAnchorViewWidget->setFloating(true);
    connect(mDockAnchorViewWidget, SIGNAL(anchorViewWidgetBecameInvisible()),
            this, SLOT(treatAnchorViewWidgetInvisibility()));


    QToolButton * newPlotButton = new QToolButton();
    newPlotButton->setDefaultAction(ui->actionNew_plot);
    newPlotButton->setFixedSize(34, 34);
    newPlotButton->setIconSize(QSize(30, 30));
    ui->tabWidget->setCornerWidget(newPlotButton, Qt::TopLeftCorner);

    if (mDataSourceEditingDialog && mDataSourceEditingDialog->isVisible()) {
        mDataSourceEditingDialog->raise();
        mDataSourceEditingDialog->activateWindow();
    }

    mStatusMessageLabel->connectToJagMainWindow(this);
}

JagMainWindow::~JagMainWindow()
{

    delete ui;
    if (mDataSourceVector.indexOf(global::gGlobalDataSource) == -1) {
        delete global::gGlobalDataSource;
    }
    // make deleteing of all dataSources (when to addDataSource(DataSource *dataSource) will be transitted only dataSources allocated with new
    foreach (auto dataSource, mDataSourceVector) {
        dataSource->setParent(NULL);
        delete dataSource;
    }
    foreach (auto eventSource, mEventSourceVector) {
        eventSource->setParent(NULL);
        delete eventSource;
    }

}

int JagMainWindow::numberOfDataSources() const
{
    return mDataSourceVector.size();
}

QMap<QString, QAction*> JagMainWindow::actions() const
{
    QMap<QString, QAction*> ret;

    ret["actionAdd_graph"] = ui->actionAdd_graph;
    ret["actionAdd_event"] = ui->actionAdd_event;
    ret["actionCombine_graphs"] = ui->actionCombine_graphs;
    ret["actionSeparate_graphs"] = ui->actionSeparate_graphs;
    ret["actionIntellectual_layout"] = ui->actionIntellectual_layout;

    ret["actionNormal_mode"] = ui->actionNormal_mode;
    ret["actionMeasuring"] = ui->actionMeasuring;
    ret["actionShow_value_table"] = ui->actionShow_value_table;
    ret["actionDraw_level"] = ui->actionDraw_level;
    ret["actionArrow_with_text"] = ui->actionArrow_with_text;
    ret["actionFast_secondary_processing"] = ui->actionFast_secondary_processing;
    ret["actionZoom_X"] = ui->actionZoom_X;
    ret["actionZoom_Y"] = ui->actionZoom_Y;
    ret["actionZoom_XY"] = ui->actionZoom_XY;
    ret["actionMotion"] = ui->actionMotion;
    ret["actionPrevious_scope"] = ui->actionPrevious_scope;
    ret["actionNext_scope"] = ui->actionNext_scope;
    ret["actionAutoscale"] = ui->actionAutoscale;


    ret["actionSecondary_processing"] = ui->actionSecondary_processing;
    ret["actionAuto_units"] = ui->actionAuto_units;
    ret["actionFull_screen"] = ui->actionFull_screen;

    ret["actionPreferences"] = ui->actionPreferences;
    ret["actionTips"] = ui->actionTips;

    return ret;

}

void JagMainWindow::addNewPlot()
{
    PlotShowEditWidget *newPlot = new PlotShowEditWidget(this);
    newPlot->setPreferences(mPreferences);


    QVector<AbstractDataSourceAdapter*> dataSourceAdapterVector;
    for (auto dataSource : mDataSourceVector) {
        DataSourceAdapter *dataSourceAdapter = new DataSourceAdapter();
        dataSourceAdapter->setDataSource(dataSource);
        dataSourceAdapterVector.push_back(dataSourceAdapter);
    }


    DataChoiceDialog *dataChoiseDialog = new DataChoiceDialog(newPlot, dataSourceAdapterVector/*mDataSourceVector*/);
    if ((dataChoiseDialog->exec()) == QDialog::Accepted) {
        mPlotVector.push_back(newPlot);
        ui->tabWidget->addTab(newPlot, QString("Plot ") + QString::number(mPlotVector.size()));
        ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);
        connect(newPlot, SIGNAL(infoStringEmitted(QString)),
                this, SLOT(showInfoString(QString)));
        ui->actionNormal_mode->setChecked(true);
        ui->actionZoom_X->setChecked(false);
        ui->actionZoom_Y->setChecked(false);
        ui->actionZoom_XY->setChecked(false);
        ui->actionMeasuring->setChecked(false);

        setPlotEditingEnabled();
    } else {
        newPlot->setParent(NULL);
        delete newPlot;
    }
    dataChoiseDialog->setParent(NULL);
    delete dataChoiseDialog;

    for (auto dataSourceAdapter : dataSourceAdapterVector) {
        delete dataSourceAdapter;
    }


}

void JagMainWindow::addNewGraph()
{
    if (ui->tabWidget->count() >= 1) {
//        DataChoiseDialog *dataChoiseDialog = new DataChoiseDialog(mPlotVector[ui->tabWidget->currentIndex()], mDataSourceVector);

        //        DataSourceAdapter *dataSourceAdapter = new DataSourceAdapter();
        //        dataSourceAdapter->setDataSource(dataSource);
        QVector<AbstractDataSourceAdapter*> dataSourceAdapterVector;
        for (auto dataSource : mDataSourceVector) {
            DataSourceAdapter *dataSourceAdapter = new DataSourceAdapter();
            dataSourceAdapter->setDataSource(dataSource);
            dataSourceAdapterVector.push_back(dataSourceAdapter);
        }

        DataChoiceDialog *dataChoiseDialog = new DataChoiceDialog(mPlotVector[ui->tabWidget->currentIndex()], dataSourceAdapterVector/*mDataSourceVector*/);
        dataChoiseDialog->exec();

        for (auto dataSourceAdapter : dataSourceAdapterVector) {
            delete dataSourceAdapter;
        }

        dataChoiseDialog->setParent(NULL);
        delete dataChoiseDialog;
    }
}

void JagMainWindow::addNewEvent()
{
    if (ui->tabWidget->count() >= 1) {
        QVector<AbstractDataSourceAdapter*> dataSourceAdapterVector;
//        for (auto dataSource : mDataSourceVector) {
//            DataSourceAdapter *dataSourceAdapter = new DataSourceAdapter();
//            dataSourceAdapter->setDataSource(dataSource);
//            dataSourceAdapterVector.push_back(dataSourceAdapter);
//        }

        //temp
//        EventSource commandSource(QString("data.jef"));
//        EventSourceAdapter *eventSourceAdapter = new EventSourceAdapter();
//        eventSourceAdapter->setEventSource(&commandSource);
//        dataSourceAdapterVector.push_back(eventSourceAdapter);
        //temp


        for (auto eventSource : mEventSourceVector) {
            EventSourceAdapter *eventSourceAdapter = new EventSourceAdapter();
            eventSourceAdapter->setEventSource(eventSource);
            dataSourceAdapterVector.push_back(eventSourceAdapter);
        }


        DataChoiceDialog *dataChoiseDialog = new DataChoiceDialog(mPlotVector[ui->tabWidget->currentIndex()], dataSourceAdapterVector/*mDataSourceVector*/);
        dataChoiseDialog->exec();

        for (auto dataSourceAdapter : dataSourceAdapterVector) {
            delete dataSourceAdapter;
        }
    }
}

void JagMainWindow::addDataSource()
{
    QStringList fileNamesList = QFileDialog::getOpenFileNames(
                             this,
                             "Select one or more files to open",
                             mDefaultDataSourceDir
                                                             );
    foreach (QString fileName, fileNamesList) {
        addDataSource(fileName);
    }

}

void JagMainWindow::addDataSource(DataSource *dataSource)
{
    if (dataSource != NULL) {
        if (dataSource->isValid()) {
            connect(dataSource, SIGNAL(errorDuringLoading(QString)),
                    this, SLOT(treatErrorDuringDataSourceLoading(QString)));
            connect(dataSource, SIGNAL(nonCriticalErrorsDuringLoading()),
                    this, SLOT(treatNonCriticalErrorDuringDataSourceLoading()));

            mDataSourceVector.push_back(dataSource);
            mStatusMessageLabel->setMessage("<h3>Data source    "+ dataSource->getDataSourceName()  + "    was added <font color=green> successfully</font></h3>", 3000);
        } else {
            treatErrorDuringDataSourceLoading("Error during " + dataSource->getDataSourceName() + " loading");
        }
    }
}

void JagMainWindow::removeDataSource(DataSource *dataSource)
{
    if (dataSource != NULL) {
        int index = mDataSourceVector.indexOf(dataSource);
        if (dataSource == global::gGlobalDataSource) {
            if (index != -1) {
                mDataSourceEditingDialog->deleteDataSourceTab(dataSource);

                mDataSourceVector.remove(index);
                global::gGlobalDataSource->clearData();
                connect(global::gGlobalDataSource, SIGNAL(simpleDataSourceChanged()),
                        this, SLOT(treatGlobalDataSourceChanging()));

                return;
            }
        }
        if (index != -1) {
                mDataSourceEditingDialog->deleteDataSourceTab(dataSource);

                if (!dataSource->isFinished()) {
                    qDebug() << "Termination of an unfinished thread";
                    dataSource->terminate();
                    dataSource->wait();
                }
                mStatusMessageLabel->setMessage("<h3>Data source    "+ dataSource->getDataSourceName()  + "    was removed</h3>", 3000);
                dataSource->setParent(NULL);
                delete dataSource;
                mDataSourceVector.remove(index);
        }
    }
}

void JagMainWindow::addDataSource(QString fileName)
{
    QFileInfo file(fileName);
    if (file.exists()) {
        mDefaultDataSourceDir = file.absolutePath();
    }

    try {
        if (fileName.endsWith(".jef", Qt::CaseInsensitive)) {
            EventSource *eventSource = new EventSource(fileName);
            mEventSourceVector.push_back(eventSource);
        } else {
            auto dataSource = createDataSourceFromFileName(fileName);
            addDataSource(dataSource);

            std::shared_ptr<GraphStyle> style = dataSource->graphStyle();
            QPen pen = style->pen();
            pen.setWidthF(mPreferences.mLineWidth);
            style->setPen(pen);
            QCPScatterStyle scatStyle = style->scatterStyle();
            scatStyle.setDecimation(mPreferences.mScatterDecimation);
            scatStyle.setSize(mPreferences.mScatterSize);
            style->setScatterStyle(scatStyle);
            style->setLineInterpolation(mPreferences.mLineInterpolation);
            style->setSpecifiedProperty(false);

            emit newDataSourceCreated(dataSource);

            if (global::gShowDataSourceEditingDialog/* && DataSourceControlDialog::isDataSourceControlDialogActive == false*/) {
                editDataSources();
            }


            //DEBUG
//            static int i = 0;
//            if (i++ %2) {
//                QPen pen = style->pen();
//                pen.setWidthF(7);
//                style->setPen(pen);

//                QCPScatterStyle scatStyle = style->scatterStyle();
//                scatStyle.setDecimation(60);
//                scatStyle.setSize(20);
//                scatStyle.setShape(QCPScatterStyle::ssSquare);
//                style->setScatterStyle(scatStyle);
//                style->setSpecifiedProperty(true);
//            }
            //DEBUG

//            mDataSourceVector.push_back(dataSource);
//            mStatusMessageLabel->setMessage("<h3>Data source    "+ fileName + "    was loaded <font color=green> successfully</font></h3>", 3000);
        }
    } catch(...) {
        showCriticalMessage("Data source " + fileName + " can't be loaded.");
        mStatusMessageLabel->setMessage("<h3><font color=red>Data source    "+ fileName + "    was  not loaded </font></h3>", 3000);
    }

}

void JagMainWindow::editDataSources()
{

//    DataSourceControlDialog cw(mDataSourceVector);
//    connect(this, SIGNAL(newDataSourceCreated(DataSource*)),
//            &cw, SLOT(addDataSource(DataSource*)));
//    cw.exec();
//    disconnect(this, SIGNAL(newDataSourceCreated(DataSource*)),
//            &cw, SLOT(addDataSource(DataSource*)));


    if (mDataSourceEditingDialog == NULL) {
        mDataSourceEditingDialog = new DataSourceControlDialog(mDataSourceVector, this);
        connect(this, SIGNAL(newDataSourceCreated(DataSource*)),
                mDataSourceEditingDialog, SLOT(addDataSource(DataSource*)));
    }
//    mDataSourceEditingDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
    mDataSourceEditingDialog->show();
    mDataSourceEditingDialog->raise();
    mDataSourceEditingDialog->activateWindow();
//    disconnect(this, SIGNAL(newDataSourceCreated(DataSource*)),
    //            cw, SLOT(addDataSource(DataSource*)));
}

void JagMainWindow::saveGlobalEventSourceAs()
{
    if (mEventSourceVector.indexOf(global::gGlobalEventSource) != -1) {
        QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                    global::gDefaultDirectory.endsWith("/") || global::gDefaultDirectory.endsWith("\\") ?
                                    global::gDefaultDirectory + "untitled.jef" : global::gDefaultDirectory + "/untitled.jef" ,
                                    tr("*.jef"));
        if (fileName != "") {
            global::gGlobalEventSource->saveAs(fileName);
        }
    }


}

void JagMainWindow::saveDataInTable()
{
        QVector<AbstractDataSourceAdapter*> dataSourceAdapterVector;
        for (auto dataSource : mDataSourceVector) {
            DataSourceAdapter *dataSourceAdapter = new DataSourceAdapter();
            dataSourceAdapter->setDataSource(dataSource);
            dataSourceAdapterVector.push_back(dataSourceAdapter);
        }

        TablePrinter tablePrinter;
        DataChoiceDialog *dataChoiseDialog = new DataChoiceDialog(&tablePrinter, dataSourceAdapterVector);
        dataChoiseDialog->exec();

        for (auto dataSourceAdapter : dataSourceAdapterVector) {
            delete dataSourceAdapter;
        }
        tablePrinter.saveData();

}

void JagMainWindow::treatTabCloseRequest(int tabIndex)
{
    qDebug() << "Tab index to delete" << tabIndex;
    qDebug() << "Tabs quantity before deleting " << ui->tabWidget->count();
    qDebug() << "plots quantity before deleting" << mPlotVector.size();
    if (tabIndex < 0 || tabIndex >= mPlotVector.size()) {
        errorExit("Unexpected situation in %s\n", __FUNCTION__);
    }
    disconnect(mPlotVector[tabIndex], SIGNAL(infoStringEmitted(QString)),
            this, SLOT(showInfoString(QString)));
    mPlotVector[tabIndex]->setParent(NULL);  //this will automatically remove corresponding tab in tabWidget
    delete mPlotVector[tabIndex];
    mPlotVector.remove(tabIndex);

    mDockAnchorViewWidget->setPlotWidget(NULL);

    qDebug() << "Tabs quantity after deleting " << ui->tabWidget->count();
    qDebug() << "plots quantity after deleting" << mPlotVector.size();
}

void JagMainWindow::treatTabMotionRequest(int to , int from) // WTF? parameter order doesn't agree with documentation
{
    if (from < 0 || from >= mPlotVector.size()) {
        errorExit("Unexpected situation in %s\n", __FUNCTION__);
    }
    if (to < 0 || to >= mPlotVector.size()) {
        errorExit("Unexpected situation in %s\n", __FUNCTION__);
    }
    if (from > to) {
        auto plot = mPlotVector[from];
        mPlotVector.remove(from);
        mPlotVector.insert(to, plot);
    } else if (from < to ) {
        auto plot = mPlotVector[from];
        mPlotVector.remove(from);
        mPlotVector.insert(to - 1, plot);
    } else {
        errorExit("Unexpected situation in %s\n", __FUNCTION__);
    }
}

void JagMainWindow::treatCurrentTabChanged(int newTab)
{
    if ((ui->tabWidget->count()) && (newTab < ui->tabWidget->count())) {
       setStatesChecked(false);

       switch (mPlotVector[newTab]->plotWidgetState()) {
           case PlotWidget::PlotWidgetMode::Common:
               ui->actionNormal_mode->setChecked(true);
               break;
           case PlotWidget::PlotWidgetMode::Measuring:
               ui->actionMeasuring->setChecked(true);
               break;
           case PlotWidget::PlotWidgetMode::ZoomXY:
               ui->actionZoom_XY->setChecked(true);
               break;
           case PlotWidget::PlotWidgetMode::ZoomX:
               ui->actionZoom_X->setChecked(true);
               break;
           case PlotWidget::PlotWidgetMode::ZoomY:
               ui->actionZoom_Y->setChecked(true);
               break;
           case PlotWidget::PlotWidgetMode::Motion:
               ui->actionMotion->setChecked(true);
               break;
           case PlotWidget::PlotWidgetMode::ArrowDrawing:
               ui->actionArrow_with_text->setChecked(true);
               break;
           case PlotWidget::PlotWidgetMode::FastSecondaryProcessing:
               ui->actionArrow_with_text->setChecked(true);
               break;
           case PlotWidget::PlotWidgetMode::LevelDrawing:
               ui->actionDraw_level->setChecked(true);
               break;
            //NOTE: Default clause is intentionally commented to raise errors in case of forgotten PlotWidget states
//           default:
//               ui->actionNormal_mode->setChecked(true);
//               break;
       }
       mDockAnchorViewWidget->setPlotWidget(mPlotVector[ui->tabWidget->currentIndex()]->plotWidget());
    }
}

void JagMainWindow::combineGraphs()
{
    if (ui->tabWidget->count() >= 1) {
       mPlotVector[ui->tabWidget->currentIndex()]->combineGraphs();
    }
}

void JagMainWindow::setZoomX()
{
    if (ui->tabWidget->count() >= 1) {
       setStatesChecked(false);
       ui->actionZoom_X->setChecked(true);
       mPlotVector[ui->tabWidget->currentIndex()]->setPlotWidgetState(PlotWidget::PlotWidgetMode::ZoomX);
    }
}

void JagMainWindow::setZoomY()
{
    if (ui->tabWidget->count() >= 1) {
        setStatesChecked(false);
        ui->actionZoom_Y->setChecked(true);
        mPlotVector[ui->tabWidget->currentIndex()]->setPlotWidgetState(PlotWidget::PlotWidgetMode::ZoomY);
    }
}

void JagMainWindow::setZoomXY()
{
    if (ui->tabWidget->count() >= 1) {
        setStatesChecked(false);
        ui->actionZoom_XY->setChecked(true);
        mPlotVector[ui->tabWidget->currentIndex()]->setPlotWidgetState(PlotWidget::PlotWidgetMode::ZoomXY);
    }
}

void JagMainWindow::setArrowDrawingMode()
{
    if (ui->tabWidget->count() >= 1) {
        setStatesChecked(false);
        ui->actionArrow_with_text->setChecked(true);
        mPlotVector[ui->tabWidget->currentIndex()]->setPlotWidgetState(PlotWidget::PlotWidgetMode::ArrowDrawing);
    }
}

void JagMainWindow::setLevelDrawing()
{
    if (ui->tabWidget->count() >= 1) {
        setStatesChecked(false);
        ui->actionDraw_level->setChecked(true);
        mPlotVector[ui->tabWidget->currentIndex()]->setPlotWidgetState(PlotWidget::PlotWidgetMode::LevelDrawing);
    }
}

void JagMainWindow::setNormalMode()
{
    if (ui->tabWidget->count() >= 1) {
        setStatesChecked(false);
        ui->actionNormal_mode->setChecked(true);
        mPlotVector[ui->tabWidget->currentIndex()]->setPlotWidgetState(PlotWidget::PlotWidgetMode::Common);
    }
}

void JagMainWindow::setMeasuring()
{
    if (ui->tabWidget->count() >= 1) {
        setStatesChecked(false);
        ui->actionMeasuring->setChecked(true);
        mPlotVector[ui->tabWidget->currentIndex()]->setPlotWidgetState(PlotWidget::PlotWidgetMode::Measuring);
    }
}

void JagMainWindow::setMotionMode()
{
    if (ui->tabWidget->count() >= 1) {
        setStatesChecked(false);
        ui->actionMotion->setChecked(true);
        mPlotVector[ui->tabWidget->currentIndex()]->setPlotWidgetState(PlotWidget::PlotWidgetMode::Motion);
    }
}

void JagMainWindow::setFastSecondaryProcessingMode()
{
    if (ui->tabWidget->count() >= 1) {
        setStatesChecked(false);
        ui->actionFast_secondary_processing->setChecked(true);
        mPlotVector[ui->tabWidget->currentIndex()]->setPlotWidgetState(PlotWidget::PlotWidgetMode::FastSecondaryProcessing);
    }
}

void JagMainWindow::setStatesChecked(bool checked)
{
    if (ui->tabWidget->count() >= 1) {
        ui->actionNormal_mode->setChecked(checked);
        ui->actionZoom_X->setChecked(checked);
        ui->actionZoom_Y->setChecked(checked);
        ui->actionZoom_XY->setChecked(checked);
        ui->actionDraw_level->setChecked(checked);
        ui->actionMeasuring->setChecked(checked);
        ui->actionMotion->setChecked(checked);
        ui->actionArrow_with_text->setChecked(checked);
        ui->actionFast_secondary_processing->setChecked(checked);
    }
}

void JagMainWindow::prevScope()
{
    if (ui->tabWidget->count() >= 1) {
        mPlotVector[ui->tabWidget->currentIndex()]->stepBackInHistory();
    }
}

void JagMainWindow::nextScope()
{
    if (ui->tabWidget->count() >= 1) {
        mPlotVector[ui->tabWidget->currentIndex()]->stepForwardInHistory();
    }
}

void JagMainWindow::showMeasuringTable(bool visibility)
{
    mIsMeasuringValueTableVisible = visibility;
    if (mIsMeasuringValueTableVisible)
        mDockAnchorViewWidget->show();
    else
        mDockAnchorViewWidget->hide();
}

void JagMainWindow::autoscale()
{
    if (ui->tabWidget->count() >= 1) {
        mPlotVector[ui->tabWidget->currentIndex()]->autoscale();
    }
}

void JagMainWindow::secondaryProcessing()
{
    if (ui->tabWidget->count() >= 1) {
        mPlotVector[ui->tabWidget->currentIndex()]->secondaryProcessing();
    }
}

void JagMainWindow::setAutoUnits()
{
    if (ui->tabWidget->count() >= 1) {
       mPlotVector[ui->tabWidget->currentIndex()]->setAutoUnits();
    }
}

void JagMainWindow::copyNamesToInternalClipboard()
{
    if (ui->tabWidget->count() >= 1) {
       QVector<QString> graphNames = mPlotVector[ui->tabWidget->currentIndex()]->getGraphNames();
       for (int i = 0; i < graphNames.size(); ++ i)
           graphNames[i] = global::getPrameterNameFromComplexName(graphNames[i]);
       qSort(graphNames.begin(), graphNames.end());
       auto it = std::unique(graphNames.begin(), graphNames.end());
       graphNames.erase(it, graphNames.end());

       if (graphNames.size() != 0) {
           global::gParametersSearchClipboard = graphNames;
           mStatusMessageLabel->setMessage("<h3><font color=green>Parameters'  names  were  copied  to  dataChoiseDialog  internal  clipboard</font></h3>", 3000);
       } else {
           mStatusMessageLabel->setMessage("<h3><font color=red>No parameter names to copy to the clippoard</font></h3>", 3000);
       }
    }
}

void JagMainWindow::toogleFullScreen()
{
    if (ui->tabWidget->count() >= 1) {
       mPlotVector[ui->tabWidget->currentIndex()]->toogleFullScreen();
    }
}

void JagMainWindow::setPreferences()
{
    if (mPreferencesDialog == NULL) {
        mPreferencesDialog = new PreferencesDialog(&mPreferences, this);
        connect(mPreferencesDialog, SIGNAL(preferenceSavingRequest()),
                this, SLOT(saveSettings()));
    }
    mPreferencesDialog->exec();
}

void JagMainWindow::aboutQt()
{
    QApplication::aboutQt () ;
    //    QMessageBox::aboutQt(this).exec();
}

void JagMainWindow::aboutJag()
{
    AboutJag aboutJag(this);
    aboutJag.exec();
}


void JagMainWindow::showHelpInformation()
{

    TipDialog tipDialog;
    QStringList tipsList;
    tipsList <<    "Useful shortcuts:<br><br>"
                   "press 1, 2 .. 9 to choose editing of 1, 2 .. 9 graph<br><br>"
                   "press Alt-1, Alt-2 .. Alt-9 to choose editing of 1, 2 .. 9 axis"

              << "You can move graphs, axes, join axes using drag & drop in plot"
                 " hierarchy widget. <br> Also you can move graphs to an existing or new axis by "
                 "dragging them in the main plotting widget."

              << "To add command to an existing graph use left button mouse click in measuring mode."

              << "To add existing graph or command to global data or command source use context menu on "
                 "items in hierarchy widget."

              << "There are 3 quick means to set layout:<br>"
                 "Ctrl - -    to combine all graphs in one axis<br>"
                 "Ctrl - |    to put each graph in separate axis<br>"
                 "Ctrl - i    to group parameters with related names in one axis<br>"

              << "You can move current axes or current graph with Ctrl-Up and Ctrl-Down"

              << "JaG can load not only data sources but also \"command sources\" - ordinary csv files "
                 "with *.jef (jag command file) extension. Each string of such files must match a pattern: \"COMMAND_NAME, COMMAND_TIME, "
                 "[COMMAND_DESCRIPTION]\"."

              << "Useful shortcuts for axis and graph manipulation:<br><br>"
                 "Use \"-\" and \"=\" to shrink and stretch current axis<br>"
                 "r - to remove current axis or graph<br>"
                 "w - to increase current graph line width<br>"
                 "Shift + w - to decrease current graph line width<br>"
                 "m - to switch marker shape<br>"
                 "Shift + m - to switch marker shape in opposite direction<br>"
                 "c - to switch line color<br>"
                 "Shift + c - to switch line color in opposite direction<br>"
                 "d - increase marker decimation<br>"
                 "Shift + c - decrease marker decimation<br>"
                 "i - change line interpolation (only line and  step left are available<br>"
                 "Shift + i - change line interpolation in opposite direction<br>"

              << "In zoom modes use left button of your mouse to implement limited (by visible boundaries) zoom "
                 "and right button to implement unlimited zoom<br>"

              << "JaG can automatically set dimensions to the parameters (you can use button in thetoolbar or /plot/options/Auto dimension in the menubar).<br>"
                 "You should preliminarily set rules "
                 "in preferences so that JaG knows what dimension should be set to a particular parameter."

              << "To copy names of the graphs in the current plot to dataChoiseDialog internal clipboard (you can use it to quickly select the same parameters "
                 "from another data source) pres Ctrl-Shift-C"

              << "To add command to the current plot you can use left button click in measuring mode"

              << "By default JaG considers that all command sourced files (*.jef) have UTF-8 encoding.<br>"
                 "If you use another encoding, you should explicitly mark it by placing a commentary "
                 "at the beginning of *.jef file. Example:<br>"
                 "<i>#encoding=Windows-1251</i>"

              << "To add an anchor to the current plot you can use right button click in measuring mode."

              << "You can copy graph properties from one graph and implement them to another. Use buttons "
                 "\"Copy prop.\" and \"Paste prop\" at the bottom of graph editing widget."

              << "To retrieve quickly certain bits from a parameter use context menu (choose graph in Plot hierarchy widget, "
                 "click right button and choose what bit you want to retrieve)."

              << "You can add data from text files with data in table form. <br>Format of table: <br>1 line - name of the parameters "
                 "(e.g. \"time x1 y2 z3\"). <br>2 line(optional) -format string (e.g %f %f %x %f) (available formats -%f for double values, "
                 "%x for unsigned values in hex form.) Without format string all data will be loaded as double. <br>3 and following lines - data.<br>You  shouldn't use different separators(\",\", \";\") in "
                 "such files."
                ;


    tipDialog.setTips(tipsList);
    tipDialog.exec();


}

void JagMainWindow::loadSettings()
{
    using namespace global;
    QSettings settings;
    QVariant varSetting;

    if ((varSetting = settings.value("/Settings/gDoubleToStringConversionPrecision")).isValid()) {
        gDoubleToStringConversionPrecision = varSetting.toInt();
    }

    if ((varSetting = settings.value("/Settings/gAreExtraWidgetsVisibleInDataChoiseDialog")).isValid()) {
        gAreExtraWidgetsVisibleInDataChoiseDialog = varSetting.toBool();
    }


    if ((varSetting = settings.value("/Settings/gIsJaGMainWindowInitialMoveEnabled")).isValid()) {
        gIsJaGMainWindowInitialMoveEnabled = varSetting.toBool();
    }
    if ((varSetting = settings.value("/Settings/gPreviousSessionXCoord")).isValid()) {
        gPreviousSessionXCoord = varSetting.toInt();
    }
    if ((varSetting = settings.value("/Settings/gPreviousSessionYCoord")).isValid()) {
        gPreviousSessionYCoord = varSetting.toInt();
    }

    if ((varSetting = settings.value("/Settings/gPreviousSecProcDialogWidth")).isValid()) {
        gPreviousSecProcDialogWidth = varSetting.toInt();
    }
    if ((varSetting = settings.value("/Settings/gPreviousSecProcDialogHeight")).isValid()) {
        gPreviousSecProcDialogHeight = varSetting.toInt();
    }


    if (gIsJaGMainWindowInitialMoveEnabled)
        move(QPoint(gPreviousSessionXCoord, gPreviousSessionYCoord));

    //loading window size
    if ((varSetting = settings.value("/Settings/gPreviousSessionWidth")).isValid()) {
        gPreviousSessionWidth = varSetting.toInt();
        resize(gPreviousSessionWidth, height());
    } else {
        gPreviousSessionWidth = width();
    }
    if ((varSetting = settings.value("/Settings/gPreviousSessionHeight")).isValid()) {
        gPreviousSessionHeight = varSetting.toInt();
        resize(width(), gPreviousSessionHeight);
    } else {
        gPreviousSessionHeight = height();
    }





    if ((varSetting = settings.value("/Settings/gPrevSecondProcesFunctionList")).isValid()) {
        gPrevSecondProcesFunctionVector.clear();
        foreach (QVariant prevFunction, varSetting.toList())
            gPrevSecondProcesFunctionVector.push_back(prevFunction.toString());
    }


    if ((varSetting = settings.value("/Settings/gDataDialogWidth")).isValid()) {
        gDataDialogWidth = varSetting.toInt();
    }
    if ((varSetting = settings.value("/Settings/gDataDialogHeight")).isValid()) {
        gDataDialogHeight = varSetting.toInt();
    }
    if ((varSetting = settings.value("/Settings/gAutoParametersFilteringDuringSearchIsEnabled")).isValid()) {
        gAutoParametersFilteringDuringSearchIsEnabled = varSetting.toBool();
    }

    if ((varSetting = settings.value("/Settings/gEventLineWidth")).isValid()) {
        gEventLineWidth = varSetting.toDouble();
    }

    if ((varSetting = settings.value("/Settings/gApplicationGUIStyle")).isValid()) {
        gApplicationGUIStyle = varSetting.toString();
        QApplication::setStyle(gApplicationGUIStyle);
    }

    if ((varSetting = settings.value("/Settings/gFindStringErasingTimerEnabled")).isValid()) {
        gFindStringErasingTimerEnabled = varSetting.toBool();
    }
    if ((varSetting = settings.value("/Settings/gFindStringErasingTimerInterval")).isValid()) {
        gFindStringErasingTimerInterval = varSetting.toDouble();
    }

    if ((varSetting = settings.value("/Settings/gSearchByDescriptionIsEnabled")).isValid()) {
        gSearchByDescriptionIsEnabled = varSetting.toBool();
    }

    if ((varSetting = settings.value("/Settings/gAutoDimFillOnlyForEmpty")).isValid()) {
        gAutoDimFillOnlyForEmpty = varSetting.toBool();
    }

    if ((varSetting = settings.value("/Settings/gDimensionlessParameterUnitLabel")).isValid()) {
        gDimensionlessParameterUnitLabel = varSetting.toString();
    }
    if ((varSetting = settings.value("/Settings/gUnitsForUnitList")).isValid()) {
        gUnitsForUnitList.clear();
        foreach (QVariant unit, varSetting.toList())
            gUnitsForUnitList.push_back(unit.toString());
    }
    if ((varSetting = settings.value("/Settings/gParametersForDimensionList")).isValid()) {
        gParametersForDimensionList.clear();
        foreach (QVariant parameter, varSetting.toList())
            gParametersForDimensionList.push_back(parameter.toString());
    }


    if ((varSetting = settings.value("/Settings/gDefaultDirectory")).isValid())
        gDefaultDirectory = varSetting.toString();

    if ((varSetting = settings.value("/Settings/gIsBootTipShowingEnabled")).isValid())
        gIsBootTipShowingEnabled = varSetting.toBool();

    if ((varSetting = settings.value("/Settings/gIsExtraSecondaryProcessingFunctionsEnabled")).isValid())
        gIsExtraSecondaryProcessingFunctionsEnabled = varSetting.toBool();

    if ((varSetting = settings.value("/Settings/mLineWidth")).isValid())
        mPreferences.mLineWidth = varSetting.toDouble();
    if ((varSetting = settings.value("/Settings/mLineInterpolation")).isValid())
        mPreferences.mLineInterpolation = static_cast<QCPGraph::LineStyle>(varSetting.toInt());

    if ((varSetting = settings.value("/Settings/gParameterNamingPolicy")).isValid())
        global::gParameterNamingPolicy = static_cast<global::ParamNamingPolicy>(varSetting.toInt());

    if ((varSetting = settings.value("/Settings/gLegendLocation")).isValid())
        global::gLegendLocation = static_cast<PlotLayoutModel::LegendLocation>(varSetting.toInt());



    if ((varSetting = settings.value("/Settings/mScatterSize")).isValid())
        mPreferences.mScatterSize = varSetting.toDouble();
    if ((varSetting = settings.value("/Settings/mScatterDecimation")).isValid())
        mPreferences.mScatterDecimation = varSetting.toDouble();

    if ((varSetting = settings.value("/Settings/mLegendVisibility")).isValid())
        mPreferences.mLegendVisibility = varSetting.toBool();

    if ((varSetting = settings.value("/Settings/mLegendFont")).isValid())
        mPreferences.mLegendFont = varSetting.toString();
    if ((varSetting = settings.value("/Settings/mLegendFontSize")).isValid())
        mPreferences.mLegendFont.setPointSize(varSetting.toInt());




    if ((varSetting = settings.value("/Settings/mUnitsVisibility")).isValid())
        mPreferences.mUnitsVisibility = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/mDimensionFont")).isValid())
        mPreferences.mDimensionFont = varSetting.toString();
    if ((varSetting = settings.value("/Settings/mDimensionFontSize")).isValid())
        mPreferences.mDimensionFont.setPointSize(varSetting.toInt());

    if ((varSetting = settings.value("/Settings/mXAxisFont")).isValid())
        mPreferences.mXAxisFont = varSetting.toString();
    if ((varSetting = settings.value("/Settings/mXAxisFontSize")).isValid())
        mPreferences.mXAxisFont.setPointSize(varSetting.toInt());

    if ((varSetting = settings.value("/Settings/mYAxisFont")).isValid())
        mPreferences.mYAxisFont = varSetting.toString();
    if ((varSetting = settings.value("/Settings/mYAxisFontSize")).isValid())
        mPreferences.mYAxisFont.setPointSize(varSetting.toInt());

    if ((varSetting = settings.value("/Settings/mXLabel")).isValid())
        mPreferences.mXLabel = varSetting.toString();
    if ((varSetting = settings.value("/Settings/mXLabelFont")).isValid())
        mPreferences.mXLabelFont = varSetting.toString();
    if ((varSetting = settings.value("/Settings/mXLabelFontSize")).isValid())
        mPreferences.mXLabelFont.setPointSize(varSetting.toInt());

    if ((varSetting = settings.value("/Settings/mTitleFont")).isValid())
        mPreferences.mTitleFont = varSetting.toString();
    if ((varSetting = settings.value("/Settings/mTitleFontSize")).isValid())
        mPreferences.mTitleFont.setPointSize(varSetting.toInt());

    if ((varSetting = settings.value("/Settings/mEventFont")).isValid())
        mPreferences.mEventFont = varSetting.toString();
    if ((varSetting = settings.value("/Settings/mEventFontSize")).isValid())
        mPreferences.mEventFont.setPointSize(varSetting.toInt());

    if ((varSetting = settings.value("/Settings/mArrowedTextFont")).isValid())
        mPreferences.mArrowedTextFont = varSetting.toString();
    if ((varSetting = settings.value("/Settings/mArrowedTextFontSize")).isValid())
        mPreferences.mArrowedTextFont.setPointSize(varSetting.toInt());

    if ((varSetting = settings.value("/Settings/mIsLegendFrameVisible")).isValid())
        mPreferences.mIsLegendFrameVisible = varSetting.toBool();


    if ((varSetting = settings.value("/Settings/gDefaultColorScheme")).isValid())
        global::gDefaultColorScheme = static_cast<global::ColorScheme::Scheme>(varSetting.toInt());

    if ((varSetting = settings.value("/Settings/gWheelZoomEnabled")).isValid())
        gWheelZoomEnabled = varSetting.toBool();

    if ((varSetting = settings.value("/Settings/gIsDataSourceCheckingStrict")).isValid())
        gIsDataSourceCheckingStrict = varSetting.toBool();

    if ((varSetting = settings.value("/Settings/gIsAutoNanTo0ConversionEnabled")).isValid())
        gIsAutoNanTo0ConversionEnabled = varSetting.toBool();

    if ((varSetting = settings.value("/Settings/gXTickSpacing")).isValid())
        gXTickSpacing = varSetting.toDouble();

    if ((varSetting = settings.value("/Settings/gYTickSpacing")).isValid())
        gYTickSpacing = varSetting.toDouble();

    if ((varSetting = settings.value("/Settings/gTracerSize")).isValid())
        gTracerSize = varSetting.toInt();

    if ((varSetting = settings.value("/Settings/gGraphCaptureDistance")).isValid())
        gGraphCaptureDistance = varSetting.toInt();

    if ((varSetting = settings.value("/Settings/gShowDataSourceLoadingProgress")).isValid())
        gShowDataSourceLoadingProgress = varSetting.toBool();


    if ((varSetting = settings.value("/Settings/gDrawValuesInMeasuringMode")).isValid())
        gDrawValuesInMeasuringMode = varSetting.toBool();


    if ((varSetting = settings.value("/Settings/gErrorsListCapacity")).isValid())
        gErrorsListCapacity = varSetting.toInt();

    if ((varSetting = settings.value("/Settings/gLineWidthDeviationForExternalPlotting")).isValid())
        gLineWidthDeviationForExternalPlotting = varSetting.toDouble();


    if ((varSetting = settings.value("/Settings/gGridPenWidth")).isValid())
        gGridPenWidth = varSetting.toDouble();
    if ((varSetting = settings.value("/Settings/gSubGridPenWidth")).isValid())
        gSubGridPenWidth = varSetting.toDouble();
    if ((varSetting = settings.value("/Settings/gAxisPenWidth")).isValid())
        gAxisPenWidth = varSetting.toDouble();
    if ((varSetting = settings.value("/Settings/gWidthDeviationForExternalPlotting")).isValid())
        gWidthDeviationForExternalPlotting = varSetting.toDouble();

    if ((varSetting = settings.value("/Settings/gIsDimensionCopyable")).isValid())
        gIsDimensionCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsLineColorCopyable")).isValid())
        gIsLineColorCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsLineWidthCopyable")).isValid())
        gIsLineWidthCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsLineStyleCopyable")).isValid())
        gIsLineStyleCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsInterpolationCopyable")).isValid())
        gIsInterpolationCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsScatterShapeCopyable")).isValid())
        gIsScatterShapeCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsScatterSizeCopyable")).isValid())
        gIsScatterSizeCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsScatterDecimationCopyable")).isValid())
        gIsScatterDecimationCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsBrushStyleCopyable")).isValid())
        gIsBrushStyleCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsBrushColorCopyable")).isValid())
        gIsBrushColorCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsBrushBasisCopyable")).isValid())
        gIsBrushBasisCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsTransformationCopyable")).isValid())
        gIsTransformationCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsFiltersCopyable")).isValid())
        gIsFiltersCopyable = varSetting.toBool();
    if ((varSetting = settings.value("/Settings/gIsStringRepresentationCopyable")).isValid())
        gIsStringRepresentationCopyable = varSetting.toBool();

    if ((varSetting = settings.value("/Settings/gShowDataSourceEditingDialog")).isValid())
        gShowDataSourceEditingDialog = varSetting.toBool();

    if ((varSetting = settings.value("/Settings/gDefaultPlotBackgroundColor")).isValid())
        gDefaultPlotBackgroundColor = varSetting.value<QColor>();
    if ((varSetting = settings.value("/Settings/gDefaultGridColor")).isValid())
        gDefaultGridColor = varSetting.value<QColor>();
    if ((varSetting = settings.value("/Settings/gDefaultSubGridColor")).isValid())
        gDefaultSubGridColor = varSetting.value<QColor>();
    if ((varSetting = settings.value("/Settings/gDefaultAxisRectColor")).isValid())
        gDefaultAxisRectColor = varSetting.value<QColor>();
    if ((varSetting = settings.value("/Settings/gDefaultPlotFontColor")).isValid())
        gDefaultPlotFontColor = varSetting.value<QColor>();
    if ((varSetting = settings.value("/Settings/gDefaultPlotGraphicalPrimitivesColor")).isValid())
        gDefaultPlotGraphicalPrimitivesColor = varSetting.value<QColor>();



    if ((varSetting = settings.value("/Settings/favouriteList")).isValid()) {
        gFavouriteParameters.clear();
        foreach (QVariant favourite, varSetting.toList())
            gFavouriteParameters.push_back(favourite.toString());
    }

    if ((varSetting = settings.value("/Settings/hateList")).isValid()) {
        gHateParameters.clear();
        foreach (QVariant hate, varSetting.toList())
            gHateParameters.push_back(hate.toString());
    }


    if ((varSetting = settings.value("/Settings/gGraphColorList")).isValid()) {
        gGraphColorList.clear();
        foreach (QVariant color, varSetting.toList())
            gGraphColorList.push_back(color.toString());
    }


    if ((varSetting = settings.value("/Settings/gPreviouslySavedParametersNames")).isValid()) {
        gPreviouslySavedParametersNames.clear();
        QMap<QString, QVariant>	bufMap = varSetting.toMap();
        foreach (QString key, bufMap.keys()) {
            QVariant bufStringList = bufMap[key];
            gPreviouslySavedParametersNames.insert(key, bufStringList.toStringList());
        }
    }

//    extern QMap<QString, QStringList> gPreviouslySavedParametersNames;  //QMap<stringlist name, stringlist of saved names>
}

void JagMainWindow::saveSettings()
{
    QSettings settings;
    using namespace global;


    settings.setValue("/Settings/gRussianLanguage", gRussianLanguage);

    settings.setValue("/Settings/gDoubleToStringConversionPrecision", gDoubleToStringConversionPrecision);

    settings.setValue("/Settings/mLineWidth", mPreferences.mLineWidth);
    settings.setValue("/Settings/mLineInterpolation", static_cast<int>(mPreferences.mLineInterpolation));
    settings.setValue("/Settings/mLegendVisibility", mPreferences.mLegendVisibility);
    settings.setValue("/Settings/mLegendFont", mPreferences.mLegendFont);
    settings.setValue("/Settings/mLegendFontSize", mPreferences.mLegendFont.pointSize());
    settings.setValue("/Settings/mXAxisFont", mPreferences.mXAxisFont);
    settings.setValue("/Settings/mXAxisFontSize", mPreferences.mXAxisFont.pointSize());
    settings.setValue("/Settings/mYAxisFont", mPreferences.mYAxisFont);
    settings.setValue("/Settings/mYAxisFontSize", mPreferences.mYAxisFont.pointSize());
    settings.setValue("/Settings/mScatterSize", mPreferences.mScatterSize);
    settings.setValue("/Settings/mScatterDecimation", mPreferences.mScatterDecimation);



    settings.setValue("/Settings/mXLabel", mPreferences.mXLabel);
    settings.setValue("/Settings/mXLabelFont", mPreferences.mXLabelFont);
    settings.setValue("/Settings/mXLabelFontSize", mPreferences.mXLabelFont.pointSize());

    settings.setValue("/Settings/mTitleFont", mPreferences.mTitleFont);
    settings.setValue("/Settings/mTitleFontSize", mPreferences.mTitleFont.pointSize());

    settings.setValue("/Settings/mUnitsVisibility", mPreferences.mUnitsVisibility);
    settings.setValue("/Settings/mDimensionFont", mPreferences.mDimensionFont);
    settings.setValue("/Settings/mDimensionFontSize", mPreferences.mDimensionFont.pointSize());

    settings.setValue("/Settings/mEventFont", mPreferences.mEventFont);
    settings.setValue("/Settings/mEventFontSize", mPreferences.mEventFont.pointSize());

    settings.setValue("/Settings/mArrowedTextFont", mPreferences.mArrowedTextFont);
    settings.setValue("/Settings/mArrowedTextFontSize", mPreferences.mArrowedTextFont.pointSize());

    settings.setValue("/Settings/mIsLegendFrameVisible", mPreferences.mIsLegendFrameVisible);



    settings.setValue("/Settings/gDefaultColorScheme", static_cast<int>(gDefaultColorScheme.scheme()));

    settings.setValue("/Settings/gWheelZoomEnabled", gWheelZoomEnabled);

    settings.setValue("/Settings/gApplicationGUIStyle", gApplicationGUIStyle);

    settings.setValue("/Settings/gFindStringErasingTimerInterval", gFindStringErasingTimerInterval);

    settings.setValue("/Settings/gIsJaGMainWindowInitialMoveEnabled", gIsJaGMainWindowInitialMoveEnabled);

    settings.setValue("/Settings/gGraphCaptureDistance", gGraphCaptureDistance);

    settings.setValue("/Settings/gAutoDimFillOnlyForEmpty", gAutoDimFillOnlyForEmpty);

    settings.setValue("/Settings/gDrawValuesInMeasuringMode", gDrawValuesInMeasuringMode);


    settings.setValue("/Settings/gIsDimensionCopyable", gIsDimensionCopyable);
    settings.setValue("/Settings/gIsLineColorCopyable", gIsLineColorCopyable);
    settings.setValue("/Settings/gIsLineWidthCopyable", gIsLineWidthCopyable);
    settings.setValue("/Settings/gIsLineStyleCopyable", gIsLineStyleCopyable);
    settings.setValue("/Settings/gIsInterpolationCopyable", gIsInterpolationCopyable);
    settings.setValue("/Settings/gIsScatterShapeCopyable", gIsScatterShapeCopyable);
    settings.setValue("/Settings/gIsScatterSizeCopyable", gIsScatterSizeCopyable);
    settings.setValue("/Settings/gIsScatterDecimationCopyable", gIsScatterDecimationCopyable);
    settings.setValue("/Settings/gIsBrushStyleCopyable", gIsBrushStyleCopyable);
    settings.setValue("/Settings/gIsBrushColorCopyable", gIsBrushColorCopyable);
    settings.setValue("/Settings/gIsBrushBasisCopyable", gIsBrushBasisCopyable);
    settings.setValue("/Settings/gIsTransformationCopyable", gIsTransformationCopyable);
    settings.setValue("/Settings/gIsFiltersCopyable", gIsFiltersCopyable);
    settings.setValue("/Settings/gIsStringRepresentationCopyable", gIsStringRepresentationCopyable);

    settings.setValue("/Settings/gShowDataSourceEditingDialog", gShowDataSourceEditingDialog);

    settings.setValue("/Settings/gLineWidthDeviationForExternalPlotting", gLineWidthDeviationForExternalPlotting);


    settings.setValue("/Settings/gXTickSpacing", gXTickSpacing);
    settings.setValue("/Settings/gYTickSpacing", gYTickSpacing);

    settings.setValue("/Settings/gErrorsListCapacity", gErrorsListCapacity);

    settings.setValue("/Settings/gShowDataSourceLoadingProgress", gShowDataSourceLoadingProgress);


    settings.setValue("/Settings/gIsDataSourceCheckingStrict", gIsDataSourceCheckingStrict);

    settings.setValue("/Settings/gGridPenWidth", gGridPenWidth);
    settings.setValue("/Settings/gSubGridPenWidth", gSubGridPenWidth);
    settings.setValue("/Settings/gAxisPenWidth", gAxisPenWidth);
    settings.setValue("/Settings/gWidthDeviationForExternalPlotting", gWidthDeviationForExternalPlotting);

    settings.setValue("/Settings/gDefaultPlotBackgroundColor", gDefaultPlotBackgroundColor);
    settings.setValue("/Settings/gDefaultGridColor", gDefaultGridColor);
    settings.setValue("/Settings/gDefaultSubGridColor", gDefaultSubGridColor);
    settings.setValue("/Settings/gDefaultAxisRectColor", gDefaultAxisRectColor);
    settings.setValue("/Settings/gDefaultPlotFontColor", gDefaultPlotFontColor);
    settings.setValue("/Settings/gDefaultPlotGraphicalPrimitivesColor", gDefaultPlotGraphicalPrimitivesColor);



    settings.setValue("/Settings/gDimensionlessParameterUnitLabel", gDimensionlessParameterUnitLabel);
    QList<QVariant> parameterList;
    foreach (auto elem, gParametersForDimensionList)
        parameterList.push_back(elem);
    settings.setValue("/Settings/gParametersForDimensionList", parameterList);
    QList<QVariant> unitsList;
    foreach (auto elem, gUnitsForUnitList)
        unitsList.push_back(elem);
    settings.setValue("/Settings/gUnitsForUnitList", unitsList);

    QList<QVariant> favouriteList;
    foreach (auto elem, gFavouriteParameters)
        favouriteList.push_back(elem);
    settings.setValue("/Settings/favouriteList", favouriteList);

    QList<QVariant> hateList;
    foreach (auto elem, gHateParameters)
        hateList.push_back(elem);
    settings.setValue("/Settings/hateList", hateList);

    settings.setValue("/Settings/gDefaultDirectory", gDefaultDirectory);

    QList<QVariant> graphColorList;
    foreach (auto elem, gGraphColorList)
        graphColorList.push_back(elem);
    settings.setValue("/Settings/gGraphColorList", graphColorList);




    settings.setValue("/Settings/gParameterNamingPolicy", static_cast<int>(global::gParameterNamingPolicy));
    settings.setValue("/Settings/gLegendLocation", static_cast<int>(global::gLegendLocation));

}

void JagMainWindow::showInfoString(QString info)
{
//    QStatusBar *status_bar =	statusBar();
//    status_bar->clearMessage();
//    status_bar->showMessage(info);
    mStatusMessageLabel->setMessage(info, 10000);

}

void JagMainWindow::slotNewConnection()
{
    qDebug() << "slotNewConnection";
    QTcpSocket * clientSocket = mListeningServer->nextPendingConnection();
    if (clientSocket) {
//        qDebug() << "mClientSocket ----------------------------->" << &mClientSocket;

        mClientSocketVector.push_back(clientSocket);
        connect(clientSocket, SIGNAL(disconnected()),
                this, SLOT(treatSocketDisconnection()));
        connect(clientSocket, SIGNAL(readyRead()),
                this, SLOT(slotReadClient()));
    //    sendToClient(pClientSocket, "Server Response: Connected!");
    }
}

void JagMainWindow::slotReadClient()
{

    if (QTcpSocket * clientSocket = qobject_cast<QTcpSocket *>(sender())) {
        qDebug() << "slotReadClient in JagMainWindow";
        quint16 blockSize = 0;
        QDataStream in(clientSocket);
        in.setVersion(QDataStream::Qt_4_5);
        for (;;) {
            if (clientSocket->bytesAvailable()  < static_cast<qint64>(sizeof(quint16)))
                continue;
            in >> blockSize;
            break;
        }

        quint16 numberOfStrings = 0;
        for (;;) {
              if (clientSocket->bytesAvailable()  < blockSize)
                 continue;
              in >> numberOfStrings;
              for (int i = 0; i < numberOfStrings; ++i) {
                  QString dataSourceName;
                  in >> dataSourceName;
                  qDebug() << "string in message " << dataSourceName;
                  addDataSource(dataSourceName);
              }
              break;
        }

        //sending data to sender to close it
        QByteArray arrBlock;
        QDataStream outStream(&arrBlock, QIODevice::WriteOnly);
        outStream.setVersion(QDataStream::Qt_4_5);
        outStream << quint16(666);
        clientSocket->write(arrBlock);
        qDebug() << "Data send to client from server";
    }

}

void JagMainWindow::treatSocketDisconnection()
{
    if (QTcpSocket * clientSocket = qobject_cast<QTcpSocket *>(sender())) {
        int index = mClientSocketVector.indexOf(clientSocket);
        if (index != -1) {
            mClientSocketVector.remove(index);
            clientSocket->deleteLater();
        }
    }

}

void JagMainWindow::treatGlobalDataSourceChanging()
{
    if (mDataSourceVector.indexOf(global::gGlobalDataSource) == -1) {
        mDataSourceVector.push_back(global::gGlobalDataSource);
        disconnect(global::gGlobalDataSource, SIGNAL(simpleDataSourceChanged()),
                this, SLOT(treatGlobalDataSourceChanging()));
        global::gGlobalDataSource->setParent(NULL);
        global::gGlobalDataSource->setDataSourceName("Global");
        emit newDataSourceCreated(global::gGlobalDataSource);
    }
}

void JagMainWindow::treatGlobalEventSourceChanging()
{
    if (mEventSourceVector.indexOf(global::gGlobalEventSource) == -1) {
        mEventSourceVector.push_back(global::gGlobalEventSource);
        disconnect(global::gGlobalEventSource, SIGNAL(eventSourceChanged()),
                this, SLOT(treatGlobalEventSourceChanging()));
        global::gGlobalEventSource->setParent(NULL);
        global::gGlobalEventSource->setName("Global");
        ui->actionSave_global_command_source_as->setEnabled(true);
    }
}

void JagMainWindow::treatAnchorViewWidgetInvisibility()
{
    if (mIsMeasuringValueTableVisible) {
        mIsMeasuringValueTableVisible = false;
        mDockAnchorViewWidget->hide();
        ui->actionShow_value_table->setChecked(false);
    }
}

void JagMainWindow::treatErrorDuringDataSourceLoading(QString errorMessage)
{

    DataSource *dataSource = qobject_cast<DataSource *>(sender());
    if (dataSource) {
        errorMessage = "Error during " + dataSource->getDataSourceName() + " loading: " + errorMessage;
//        int index = -1;
//        if ((index = mDataSourceVector.indexOf(dataSource)) != -1) {
//            mDataSourceVector.remove(index);
//            mDataSourceEditingDialog->deleteDataSource(dataSource);
//        }
        if (mDataSourceVector.indexOf(dataSource) != -1) {
            removeDataSource(dataSource);
        }
    }
    showCriticalMessage(errorMessage);
}

void JagMainWindow::treatNonCriticalErrorDuringDataSourceLoading()
{
    DataSource *dataSource = qobject_cast<DataSource *>(sender());
    if (dataSource) {
        NonCriticalErrorsDialogWithInfo messageDialog;
        messageDialog.showShowItNextTimeCheckBox(false);
        messageDialog.setMessage(dataSource->getDataSourceName() + " contains incorrect data.\nDo you really want to continue loading?");
        messageDialog.setDetails(dataSource->errorsList());
//        messageDialog.setRejectText("No");
//        messageDialog.setAcceptText("Yes");
        if (messageDialog.exec() == QDialog::Rejected) {
//            int index = -1;
//            if ((index = mDataSourceVector.indexOf(dataSource)) != -1) {
//                mDataSourceVector.remove(index);
//                removeDataSource(dataSource);
//                mDataSourceEditingDialog->deleteDataSource(dataSource);
//            }
            if (mDataSourceVector.indexOf(dataSource) != -1) {
                removeDataSource(dataSource);
            }
        }
    }
}

void JagMainWindow::show()
{
    QMainWindow::show();
    if (mDataSourceEditingDialog && mDataSourceEditingDialog->isVisible()) {
        mDataSourceEditingDialog->raise();
        mDataSourceEditingDialog->activateWindow();
    }
}

void JagMainWindow::setPlotEditingEnabled()
{
    bool enabled = (ui->tabWidget->count() == 0 ? false : true);

    ui->actionAdd_graph->setEnabled(enabled);
    ui->actionAdd_event->setEnabled(enabled);
    ui->actionCombine_graphs->setEnabled(enabled);
    ui->actionSeparate_graphs->setEnabled(enabled);
    ui->actionIntellectual_layout->setEnabled(enabled);

    ui->actionSave_as->setEnabled(enabled);
    ui->actionQuick_save->setEnabled(enabled);
    ui->actionCopy->setEnabled(enabled);
    ui->actionPrint->setEnabled(enabled);

    ui->actionArrow_with_text->setEnabled(enabled);
    ui->actionMeasuring->setEnabled(enabled);
    ui->actionZoom_X->setEnabled(enabled);
    ui->actionZoom_Y->setEnabled(enabled);
    ui->actionZoom_XY->setEnabled(enabled);
    ui->actionAutoscale->setEnabled(enabled);
    ui->actionNormal_mode->setEnabled(enabled);
    ui->actionMotion->setEnabled(enabled);
    ui->actionNext_scope->setEnabled(enabled);
    ui->actionPrevious_scope->setEnabled(enabled);
    ui->actionDraw_level->setEnabled(enabled);
    ui->actionFast_secondary_processing->setEnabled(enabled);

    ui->actionShow_value_table->setEnabled(enabled);

    ui->actionSecondary_processing->setEnabled(enabled);
    ui->actionAuto_units->setEnabled(enabled);
    ui->actionCopy_names_to_clipboard->setEnabled(enabled);
    ui->actionFull_screen->setEnabled(enabled);


}




void JagMainWindow::dragEnterEvent(QDragEnterEvent *event)
{
//    qDebug() << "dragEnterEvent";
    if (event->mimeData()->hasFormat("text/uri-list")) {
//        qDebug() << "Accepted as uri";
        event->accept();
    }
//    if (event->mimeData()->hasFormat("text/plain")) {
//        qDebug() << "Accepted";
//        event->accept();
//    }
}

void JagMainWindow::dropEvent(QDropEvent *event)
{
    const QMimeData *mimeData = event->mimeData();
//    qDebug() << "dropEvent";
//    qDebug() << mimeData->text();

    if (mimeData->hasFormat("text/uri-list")) {
        QList<QUrl> urlList = mimeData->urls();
        foreach (QUrl url, urlList) {
            QString fileName = url.toLocalFile();
            qDebug() << "file name to load" << fileName;
            addDataSource(fileName);
        }
    }

    QMainWindow::dropEvent(event);
}

void JagMainWindow::closeEvent(QCloseEvent *event)
{
    //saving window sizes
    using namespace global;
    QSettings settings;
    gPreviousSessionWidth = width();
    gPreviousSessionHeight = height();
    settings.setValue("/Settings/gPreviousSessionWidth", gPreviousSessionWidth);
    settings.setValue("/Settings/gPreviousSessionHeight", gPreviousSessionHeight);


    settings.setValue("/Settings/gPreviousSecProcDialogWidth", gPreviousSecProcDialogWidth);
    settings.setValue("/Settings/gPreviousSecProcDialogHeight", gPreviousSecProcDialogHeight);


    gPreviousSessionXCoord = x();
    gPreviousSessionYCoord = y();
    settings.setValue("/Settings/gPreviousSessionXCoord", gPreviousSessionXCoord);
    settings.setValue("/Settings/gPreviousSessionYCoord", gPreviousSessionYCoord);

    settings.setValue("/Settings/gIsBootTipShowingEnabled", gIsBootTipShowingEnabled);

    settings.setValue("/Settings/gDataDialogWidth", gDataDialogWidth);
    settings.setValue("/Settings/gDataDialogHeight", gDataDialogHeight);
    settings.setValue("/Settings/gAutoParametersFilteringDuringSearchIsEnabled", gAutoParametersFilteringDuringSearchIsEnabled);

    settings.setValue("/Settings/gAreExtraWidgetsVisibleInDataChoiseDialog", gAreExtraWidgetsVisibleInDataChoiseDialog);

    settings.setValue("/Settings/gIsAutoNanTo0ConversionEnabled", gIsAutoNanTo0ConversionEnabled);

    settings.setValue("/Settings/gTracerSize", gTracerSize);


    settings.setValue("/Settings/gSearchByDescriptionIsEnabled", gSearchByDescriptionIsEnabled);

    QList<QVariant> prevFunctionsList;
    foreach (auto elem, gPrevSecondProcesFunctionVector)
        prevFunctionsList.push_back(elem);
    settings.setValue("/Settings/gPrevSecondProcesFunctionList", prevFunctionsList);


    settings.setValue("/Settings/gEventLineWidth", gEventLineWidth);
    settings.setValue("/Settings/gIsExtraSecondaryProcessingFunctionsEnabled", gIsExtraSecondaryProcessingFunctionsEnabled);

    settings.setValue("/Settings/gFindStringErasingTimerEnabled", gFindStringErasingTimerEnabled);

    QMap<QString, QVariant>	bufMap;
    foreach(QString key, gPreviouslySavedParametersNames.keys()) {
        bufMap.insert(key, QVariant(gPreviouslySavedParametersNames[key]));
    }
    settings.setValue("/Settings/gPreviouslySavedParametersNames", bufMap);


    //checking for unfinished threads
    for (DataSource* dataSource : mDataSourceVector) {
        if (!dataSource->isFinished()) {
            qDebug() << "Termination of an unfinished thread";
            dataSource->terminate();
            dataSource->wait();
        }
    }


    QMainWindow::closeEvent(event);

}

void JagMainWindow::keyPressEvent(QKeyEvent *event)
{
    if (ui->tabWidget->count() == 0) {
        QMainWindow::keyPressEvent(event);
        return;
    }

    if (event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_Up) {
        mPlotVector[ui->tabWidget->currentIndex()]->moveCurrentObject(AxisModel::AxisMovement::Up);
    } else if (event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_Down) {
        mPlotVector[ui->tabWidget->currentIndex()]->moveCurrentObject(AxisModel::AxisMovement::Down);
    } else if ( event->key() == Qt::Key_Minus) {
        mPlotVector[ui->tabWidget->currentIndex()]->stretchCurrentAxis(0.85);
    } else if ( event->key() == Qt::Key_Equal) {
        mPlotVector[ui->tabWidget->currentIndex()]->stretchCurrentAxis(1./0.85);
    } else if ( event->key() == Qt::Key_R) {
        mPlotVector[ui->tabWidget->currentIndex()]->deleteCurrentObject();
    } else if ( event->key() == Qt::Key_W) {
        if (event->modifiers() == Qt::ShiftModifier)
            mPlotVector[ui->tabWidget->currentIndex()]->increaseWidthOfCurrentGraph(1/1.15);
        else
            mPlotVector[ui->tabWidget->currentIndex()]->increaseWidthOfCurrentGraph(1.15);
    } else if ( event->key() == Qt::Key_M) {
        if (event->modifiers() == Qt::ShiftModifier)
            mPlotVector[ui->tabWidget->currentIndex()]->switchLineScatterOfCurrentGraph(-1);
        else
            mPlotVector[ui->tabWidget->currentIndex()]->switchLineScatterOfCurrentGraph(1);
    } else if ( event->key() == Qt::Key_C) {
        if (event->modifiers() == Qt::ShiftModifier)
            mPlotVector[ui->tabWidget->currentIndex()]->switchLineColorOfCurrentGraph(-1);
        else
            mPlotVector[ui->tabWidget->currentIndex()]->switchLineColorOfCurrentGraph(1);
    } else if ( event->key() == Qt::Key_I) {
        if (event->modifiers() == Qt::ShiftModifier)
            mPlotVector[ui->tabWidget->currentIndex()]->switchLineInterpolationOfCurrentGraph(-1);
        else
            mPlotVector[ui->tabWidget->currentIndex()]->switchLineInterpolationOfCurrentGraph(1);
    } else if ( event->key() == Qt::Key_D) {
        if (event->modifiers() == Qt::ShiftModifier)
            mPlotVector[ui->tabWidget->currentIndex()]->increaseDecimationOfCurrentGraph(1./2.);
        else
            mPlotVector[ui->tabWidget->currentIndex()]->increaseDecimationOfCurrentGraph(2.);
    } else if (event->key() == Qt::Key_1) {
        if (ui->tabWidget->count() >= 1) {
            if (event->modifiers() == Qt::AltModifier)
               mPlotVector[ui->tabWidget->currentIndex()]->activateAxis(0);
            else
               mPlotVector[ui->tabWidget->currentIndex()]->activateGraph(0);
        }
    } else if (event->key() == Qt::Key_2) {
        if (ui->tabWidget->count() >= 1) {
            if (event->modifiers() == Qt::AltModifier)
               mPlotVector[ui->tabWidget->currentIndex()]->activateAxis(1);
            else
               mPlotVector[ui->tabWidget->currentIndex()]->activateGraph(1);
        }
    } else if (event->key() == Qt::Key_3) {
        if (ui->tabWidget->count() >= 1) {
            if (event->modifiers() == Qt::AltModifier)
               mPlotVector[ui->tabWidget->currentIndex()]->activateAxis(2);
            else
               mPlotVector[ui->tabWidget->currentIndex()]->activateGraph(2);
        }
    } else if (event->key() == Qt::Key_4) {
        if (ui->tabWidget->count() >= 1) {
            if (event->modifiers() == Qt::AltModifier)
               mPlotVector[ui->tabWidget->currentIndex()]->activateAxis(3);
            else
               mPlotVector[ui->tabWidget->currentIndex()]->activateGraph(3);
        }
    } else if (event->key() == Qt::Key_5) {
        if (ui->tabWidget->count() >= 1) {
            if (event->modifiers() == Qt::AltModifier)
               mPlotVector[ui->tabWidget->currentIndex()]->activateAxis(4);
            else
               mPlotVector[ui->tabWidget->currentIndex()]->activateGraph(4);
        }
    } else if (event->key() == Qt::Key_6) {
        if (ui->tabWidget->count() >= 1) {
            if (event->modifiers() == Qt::AltModifier)
               mPlotVector[ui->tabWidget->currentIndex()]->activateAxis(5);
            else
               mPlotVector[ui->tabWidget->currentIndex()]->activateGraph(5);
        }
    } else if (event->key() == Qt::Key_7) {
        if (ui->tabWidget->count() >= 1) {
            if (event->modifiers() == Qt::AltModifier)
               mPlotVector[ui->tabWidget->currentIndex()]->activateAxis(6);
            else
               mPlotVector[ui->tabWidget->currentIndex()]->activateGraph(6);
        }
    } else if (event->key() == Qt::Key_8) {
        if (ui->tabWidget->count() >= 1) {
            if (event->modifiers() == Qt::AltModifier)
               mPlotVector[ui->tabWidget->currentIndex()]->activateAxis(7);
            else
               mPlotVector[ui->tabWidget->currentIndex()]->activateGraph(7);
        }
    } else if (event->key() == Qt::Key_9) {
        if (ui->tabWidget->count() >= 1) {
            if (event->modifiers() == Qt::AltModifier)
               mPlotVector[ui->tabWidget->currentIndex()]->activateAxis(8);
            else
               mPlotVector[ui->tabWidget->currentIndex()]->activateGraph(8);
        }
    } else {
        QMainWindow::keyPressEvent(event);
    }
}

void JagMainWindow::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    emit widthChanged(width());
}

void JagMainWindow::separateGraphs()
{
    if (ui->tabWidget->count() >= 1) {
       mPlotVector[ui->tabWidget->currentIndex()]->separateGraphs();
    }
}

void JagMainWindow::composeIntellectually()
{
    if (ui->tabWidget->count() >= 1) {
       mPlotVector[ui->tabWidget->currentIndex()]->composeIntellectually();
    }
}

void JagMainWindow::saveAs()
{
    if (ui->tabWidget->count() >= 1) {
        using global::gDefaultDirectory;

        QDir defaultDirectory(gDefaultDirectory);
        bool dirExists = defaultDirectory.exists ();
        if (dirExists == false)
            dirExists = defaultDirectory.mkpath(gDefaultDirectory);

        if (dirExists) {
            QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                        gDefaultDirectory.endsWith("/") || gDefaultDirectory.endsWith("\\") ?
                                        gDefaultDirectory + "untitled.png" : gDefaultDirectory + "/untitled.png" ,
                                        tr("Images (*.png *.bmp *.jpg)"));

            if (fileName.endsWith(".png", Qt::CaseInsensitive)) {
                mPlotVector[ui->tabWidget->currentIndex()]->savePng(fileName);
                mStatusMessageLabel->setMessage("<h3><font color=green> Plot was saved as " + fileName  + "</font></h3>", 3000);
            } else if (fileName.endsWith(".bmp", Qt::CaseInsensitive)) {
                mPlotVector[ui->tabWidget->currentIndex()]->saveBmp(fileName);
                mStatusMessageLabel->setMessage("<h3><font color=green> Plot was saved as " + fileName  + "</font></h3>", 3000);
            } else if (fileName.endsWith(".jpg", Qt::CaseInsensitive)) {
                mPlotVector[ui->tabWidget->currentIndex()]->saveJpg(fileName);
                mStatusMessageLabel->setMessage("<h3><font color=green> Plot was saved as " + fileName  + "</font></h3>", 3000);
            } else {
                mStatusMessageLabel->setMessage("<h3><font color=red> Can't save plot as " + fileName  + "</font></h3>", 3000);
            }
        }

    }
}

void JagMainWindow::quickSave()
{

    if (ui->tabWidget->count() >= 1) {
        using global::gDefaultDirectory;
        using global::gApplicationStartTime;
        static int quickSavesPlotNumber = 0;

        static QString quickSaveDirectoryPath
                = gDefaultDirectory.endsWith("/") || gDefaultDirectory.endsWith("\\") ?
                    gDefaultDirectory + gApplicationStartTime + "/" : gDefaultDirectory + "/" + gApplicationStartTime + "/";

        QDir quickSaveDirectory(quickSaveDirectoryPath);
        bool dirExists = quickSaveDirectory.exists ();
        if (dirExists == false)
            dirExists = quickSaveDirectory.mkpath(quickSaveDirectoryPath);

        if (dirExists) {
            QString fileName = quickSaveDirectoryPath + "plot_" + QString::number(quickSavesPlotNumber) + ".png" ;

            mPlotVector[ui->tabWidget->currentIndex()]->savePng(fileName);
            quickSavesPlotNumber++;
            mStatusMessageLabel->setMessage("<h3><font color=blue> Plot was saved as " + fileName  + "</font></h3>", 3000);

        }

    }
}

void JagMainWindow::copyToClipBoard()
{
//    QClipboard* pcb = QApplication::clipboard();
    if (ui->tabWidget->count() >= 1) {
       QApplication::clipboard()->setPixmap(mPlotVector[ui->tabWidget->currentIndex()]->toPixmap());
       mStatusMessageLabel->setMessage("<h3><font color=blue> Plot was saved to the clipboard</font></h3>", 3000);


    }
}

void JagMainWindow::print()
{
    if (ui->tabWidget->count() >= 1) {
        QPrinter printer;
        QPrintDialog printDialog(&printer, this);
        if (printDialog.exec() == QDialog::Accepted) {
//            QPainter painter(&printer);
//            QPixmap picture = mPlotVector[ui->tabWidget->currentIndex()]->toPixmap();
//            double ratio = std::max(
//                                    double(picture.height()) / printer.height(),
//                                    double(picture.width()) / printer.width()
//                                    );
//            picture = mPlotVector[ui->tabWidget->currentIndex()]->toPixmap(picture.width()/ratio, picture.height()/ratio);
//            painter.drawPixmap(0, 0, picture, 0, 0, picture.width(), picture.height());

            QCPPainter painter(&printer);
//            mPlotVector[ui->tabWidget->currentIndex()]->toPainter(&painter);

            QPixmap picture = mPlotVector[ui->tabWidget->currentIndex()]->toPixmap();
            double ratio = std::max(
                                    double(picture.height()) / printer.height(),
                                    double(picture.width()) / printer.width()
                                    );

            mPlotVector[ui->tabWidget->currentIndex()]->toPainter(&painter, picture.width()/ratio, picture.height()/ratio);
//            picture = mPlotVector[ui->tabWidget->currentIndex()]->toPixmap(picture.width()/ratio, picture.height()/ratio);
//            painter.drawPixmap(0, 0, picture, 0, 0, picture.width(), picture.height());


        }
    }
}




