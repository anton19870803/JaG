#include "arrowedtext.h"
#include "cmath"
#include "global/global_definitions.h"

ArrowedText::ArrowedText(QObject *parent) :
    QObject(parent), mLineWidth(1.), mBackgroundColor(global::gDefaultPlotBackgroundColor),
    mArrowStyle(QCPLineEnding::esSpikeArrow)
{

}

QString ArrowedText::text() const
{
    return mText;
}

QPointF ArrowedText::textCenter() const
{
    return mTextCenter;
}

QPointF ArrowedText::arrowFinish() const
{
    return mArrowFinish;
}

ArrowedText::ArrowBasis ArrowedText::arrowBasisInRanges(double xRange, double yRange) const
{
    //TODO: algorithm doesn't seem to be correct, do it better
    if (xRange == 0. || yRange == 0)
        return ArrowBasis::Bottom;
    QPointF direction = mArrowFinish - mTextCenter;
    double angle_deg = std::atan2(direction.y()/yRange, direction.x()/xRange) * 180. / M_PI;
    if (angle_deg <= 45. && angle_deg > -45) {
        return ArrowBasis::Right;
    } else if (angle_deg <= -45. && angle_deg > -135) {
        return ArrowBasis::Bottom;
    } else if (angle_deg <= 135. &&  angle_deg > 45.){
        return ArrowBasis::Top;
    } else {
        return ArrowBasis::Left;
    }
}

double ArrowedText::lineWidthF() const
{
    return mLineWidth;
}

QColor ArrowedText::backgroundColor() const
{
    return mBackgroundColor;
}



void ArrowedText::setText(const QString &text)
{
    if (mText != text) {
        mText = text;
        emit arrowedTextChanged();
    }
}

void ArrowedText::setTextCenter(const QPointF &textCenter)
{
    if (mTextCenter != textCenter) {
        mTextCenter = textCenter;
        emit arrowedTextChanged();
    }
}

void ArrowedText::setArrowFinish(const QPointF &arrowFinish)
{
    if (mArrowFinish != arrowFinish) {
        mArrowFinish = arrowFinish;
        emit arrowedTextChanged();
    }
}

void ArrowedText::setLineWidthF(double width)
{
    if (mLineWidth != width) {
        mLineWidth = width;
        emit arrowedTextChanged();
    }
}

void ArrowedText::setBackgroundColor(QColor color)
{
    if (mBackgroundColor != color) {
        mBackgroundColor = color;
        emit arrowedTextChanged();
    }
}

void ArrowedText::setArrowStyle(QCPLineEnding::EndingStyle arrowStyle)
{
    if (mArrowStyle != arrowStyle) {
        mArrowStyle = arrowStyle;
        emit arrowedTextChanged();
    }

}


