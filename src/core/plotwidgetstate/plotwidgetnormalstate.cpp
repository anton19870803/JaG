#include "plotwidgetnormalstate.h"
#include "global/global_definitions.h"

PlotWidgetNormalState::PlotWidgetNormalState(PlotWidget *parent):
        PlotWidgetState(parent),
        mDraggedGraphIndex(-1), mDraggedArrowedTextIndex(-1),  mDraggedArrowEndIndex(-1),
        mGraphDragState(State::Off), mArrowedTextDragState(State::Off), mArrowEndDragState(State::Off)
{
}

void PlotWidgetNormalState::treatMousePressEvent(QMouseEvent *event)
{
    mDraggedGraphIndex = -1;

    if (mPlotWidget->axisRectCount() != 0) {
        if (event->buttons() & Qt::LeftButton) {

            mPlotWidget->setSelectionTolerance(global::gGraphCaptureDistance);
            for (int i = 0; i < mPlotWidget->mTextLabelsVector.size(); ++i) {
                if (mPlotWidget->mTextLabelsVector[i]->selectTest(event->pos(), false) < global::gGraphCaptureDistance
                        && mPlotWidget->mTextLabelsVector[i]->selectTest(event->pos(), false) >= 0) {
    //                qDebug() << "distance to" << i << " is" << mGraphVector[i]->selectTest(event->pos(), true);
                    mTextStartDragPos = event->pos();
                    mDraggedArrowedTextIndex = i;
                    break;
                }
            }
            mArrowedTextDragState = State::Off;

            for (int i = 0; i < mPlotWidget->mArrowsVector.size(); ++i) {
                int axisRectIndex = mPlotWidget->getIndexOfAxisRectByPoint(event->pos());
                QPointF arrowEnd(mPlotWidget->axisRect(axisRectIndex)->axis(QCPAxis::atBottom)->coordToPixel(mPlotWidget->mArrowsVector[i]->end->key()),
                                 mPlotWidget->axisRect(axisRectIndex)->axis(QCPAxis::atLeft)->coordToPixel(mPlotWidget->mArrowsVector[i]->end->value()));
                if ((arrowEnd - event->posF()).manhattanLength() < 5) {
    //                qDebug() << "distance to" << i << " is" << (arrowEnd - event->posF()).manhattanLength();
                    mArrowEndStartDragPos = event->pos();
                    mDraggedArrowEndIndex = i;
                    break;
                }
            }
            mArrowEndDragState = State::Off;

            if (mDraggedArrowedTextIndex < 0 && mDraggedArrowEndIndex < 0) {
                for (int i = 0; i < mPlotWidget->mGraphVector.size(); ++i) {
                    if (mPlotWidget->mGraphVector[i]->selectTest(event->pos(), true) < global::gGraphCaptureDistance
                            && mPlotWidget->mGraphVector[i]->selectTest(event->pos(), true) >= 0) {
                        mStartDragPos = event->pos();
                        mDraggedGraphIndex = i;
        //                qDebug() << "distance to" << i << " is" << mGraphVector[i]->selectTest(event->pos(), true);
                        break;
                    }
                }
                mGraphDragState = State::Off;
            }
        }
    }
}

void PlotWidgetNormalState::treatMouseReleaseEvent(QMouseEvent *event)
{

    if (mPlotWidget->axisRectCount() != 0) {
        if (mGraphDragState == State::On) {
            mGraphDragState = State::Off;
    //        qDebug() << "drop";//startDrag();

            QApplication::setOverrideCursor(Qt::ArrowCursor);

            //finding dragged graph
            GraphModel *draggedGraph = NULL;
            int graphIndex = 0;
            for (auto axis : mPlotWidget->mLayoutModel->getVisibleAxisModelVector()) {
                if (draggedGraph)
                    break;
                for (auto graph : axis->getGraphModelVector()) {
                    if (graphIndex == mDraggedGraphIndex) {
                        draggedGraph = const_cast<GraphModel*>(graph);
                        break;
                    }
                    graphIndex++;
                }
            }

            //trying to drop into existing axis
            bool dropWasImplemented = false;
            int axisRectIndex = mPlotWidget->getIndexOfAxisRectByPoint(event->pos());
            if (axisRectIndex != -1) {
                QVector<AxisModel *> axisModelVector = mPlotWidget->mLayoutModel->getVisibleAxisModelVector();
                AxisModel *destinationAxis = axisModelVector[axisRectIndex];
                if (draggedGraph) {
                    const_cast<PlotLayoutModel*>(mPlotWidget->mLayoutModel)->moveGraphToAxis(draggedGraph, destinationAxis);
                    dropWasImplemented = true;
                }
            }

            //drop to a new axis if it was not dropped to an existing one
            if (dropWasImplemented == false) {
                const_cast<PlotLayoutModel*>(mPlotWidget->mLayoutModel)->moveGraphToNewAxis(draggedGraph, event->y() < mPlotWidget->height()/2 ? 0 : 1);
            }
        }


        if (mArrowedTextDragState == State::On) {
    //        qDebug() << "drop";//startDrag();

            QApplication::setOverrideCursor(Qt::ArrowCursor);

            //finding dragged text
            ArrowedText *draggedArrowedText = NULL;
            int axisIndex = -1;
            int textIndex = 0;
            for (auto axis : mPlotWidget->mLayoutModel->getVisibleAxisModelVector()) {
                axisIndex++;
                if (draggedArrowedText) {
                    axisIndex--;
                    break;
                }
                for (auto arrText : axis->arrowedTextVector()) {
                    if (textIndex == mDraggedArrowedTextIndex) {
                        draggedArrowedText = arrText;
                        break;
                    }
                    textIndex++;
                }
            }

            if (draggedArrowedText) {
                draggedArrowedText->setTextCenter(QPointF(
                                                      mPlotWidget->axisRect(axisIndex)->axis(QCPAxis::atBottom)->pixelToCoord(event->x()),
                                                      mPlotWidget->axisRect(axisIndex)->axis(QCPAxis::atLeft)->pixelToCoord(event->y())
                                                      ));
            }
            mArrowedTextDragState = State::Off;
            mDraggedArrowedTextIndex = -1;
        }

        if (mArrowEndDragState == State::On) {
    //        qDebug() << "drop";//startDrag();

            QApplication::setOverrideCursor(Qt::ArrowCursor);

            //finding dragged text
            ArrowedText *draggedArrowedText = NULL;
            int axisIndex = -1;
            int textIndex = 0;
            for (auto axis : mPlotWidget->mLayoutModel->getVisibleAxisModelVector()) {
                axisIndex++;
                if (draggedArrowedText) {
                    axisIndex--;
                    break;
                }
                for (auto arrText : axis->arrowedTextVector()) {
                    if (textIndex == mDraggedArrowEndIndex) {
                        draggedArrowedText = arrText;
                        break;
                    }
                    textIndex++;
                }
            }

            if (draggedArrowedText) {
                draggedArrowedText->setArrowFinish(QPointF(
                                                      mPlotWidget->axisRect(axisIndex)->axis(QCPAxis::atBottom)->pixelToCoord(event->x()),
                                                      mPlotWidget->axisRect(axisIndex)->axis(QCPAxis::atLeft)->pixelToCoord(event->y())
                                                      ));
            }
            mArrowEndDragState = State::Off;
            mDraggedArrowEndIndex = -1;
        }
    }

}

void PlotWidgetNormalState::treatMouseMoveEvent(QMouseEvent *event)
{

    if (mPlotWidget->axisRectCount() != 0) {
        if (mGraphDragState == State::On) {
            QBrush brushOrigin;
            for (int i = 0; i < mPlotWidget->axisRectCount(); ++ i)
                mPlotWidget->axisRect(i)->setBackground(brushOrigin);
            int axisRectIndex = mPlotWidget->getIndexOfAxisRectByPoint(event->pos());
            if (axisRectIndex != -1) {
                QBrush brush;
                brush.setStyle(Qt::SolidPattern);
                brush.setColor(QColor(125, 233, 247, 55));
                mPlotWidget->axisRect(axisRectIndex)->setBackground(brush);
            }
            mPlotWidget->replot();
        }

        if (event->buttons() & Qt::LeftButton && mGraphDragState == State::Off
                && mDraggedGraphIndex >= 0) {
            int distance = (mStartDragPos - event->pos()).manhattanLength();
            if (distance > QApplication::startDragDistance()) {
    //            qDebug() << "start drag";//startDrag();
                mGraphDragState = State::On;
                QApplication::setOverrideCursor(Qt::DragMoveCursor);
            }
        }

        if ( event->buttons() & Qt::LeftButton && mArrowedTextDragState == State::Off
                && mDraggedArrowedTextIndex >= 0) {
            int distance = (mTextStartDragPos - event->pos()).manhattanLength();
            if (distance > QApplication::startDragDistance()) {
    //            qDebug() << "start drag";//startDrag();
                mArrowedTextDragState = State::On;
                QApplication::setOverrideCursor(Qt::DragMoveCursor);
            }
        }

        if (event->buttons() & Qt::LeftButton && mArrowEndDragState == State::Off
                && mDraggedArrowEndIndex >= 0) {
            int distance = (mArrowEndStartDragPos - event->pos()).manhattanLength();
            if (distance > QApplication::startDragDistance()) {
    //            qDebug() << "start drag";//startDrag();
                mArrowEndDragState = State::On;
                QApplication::setOverrideCursor(Qt::DragMoveCursor);
            }
        }
    }
}
