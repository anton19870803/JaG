#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QFont>
#include <QString>
#include "qcustomplot.h"

class Preferences
{
public:
    Preferences();

    bool operator==(const Preferences & arg) const {
        if (mLegendVisibility == arg.mLegendVisibility
                && mUnitsVisibility == arg.mUnitsVisibility
                && mLegendFont == arg.mLegendFont
                && mDimensionFont == arg.mDimensionFont
                && mXAxisFont == arg.mXAxisFont
                && mYAxisFont == arg.mYAxisFont
                && mXLabel == arg.mXLabel
                && mXLabelFont == arg.mXLabelFont
                && mTitleFont == arg.mTitleFont
                && mLineWidth == arg.mLineWidth
                && mLineInterpolation == arg.mLineInterpolation
                && mEventFont == arg.mEventFont
                && mArrowedTextFont == arg.mArrowedTextFont
                && mTimeAxisType == arg.mTimeAxisType
                && mScatterSize == arg.mScatterSize
                && mScatterDecimation == arg.mScatterDecimation
                && mXTickRotation == arg.mXTickRotation
                && mEventLabelRotation == arg.mEventLabelRotation
                && mIsLegendFrameVisible == arg.mIsLegendFrameVisible
            )
            return true;
        else
            return false;
    }

    bool operator!=(const Preferences & arg) const {
        return !(*this==arg);
    }

    bool mLegendVisibility;
    bool mUnitsVisibility;
    QFont mLegendFont;
    QFont mDimensionFont;
    QFont mXAxisFont;
    QFont mYAxisFont;
    QString mXLabel;
    QFont mXLabelFont;
    QFont mTitleFont;
    QFont mEventFont;
    QFont mArrowedTextFont;
    double mLineWidth;
    QCPGraph::LineStyle mLineInterpolation;
    QCPAxis::LabelType mTimeAxisType;
    double mScatterSize;
    int mScatterDecimation;
    double mXTickRotation;
    double mEventLabelRotation;
    bool mIsLegendFrameVisible;
};

#endif // PREFERENCES_H
