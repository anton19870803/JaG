#include "graphmodel.h"
#include <algorithm>
#include <vector>
#include "basic/bassic.h"
#include <QDebug>
#include <dataconverter/affineconverter.h>
#include <dataconverter/borderfilter.h>
#include <dataconverter/windowfilter.h>
#include  <algorithm>
#include "basic/basic.h"
#include <float.h>
#include "global/global_definitions.h"
#include <QIcon>


//int GraphModel::numberOfCreatedGraphModels = 0;

//std::tuple<QVector<double>, QVector<double>>
//GraphModel::identityConverter(const QVector<double>& x, const QVector<double> & y)
//{
//    return std::tuple<QVector<double>, QVector<double>> (x, y) ;
//}

GraphModel::GraphModel(QObject *parent) :
    QObject(parent), pen(Qt::blue), name("Undefined"), visibility(true),
    scatterStyle(), lineInterpolation(QCPGraph::LineStyle::lsLine), affineConverter(std::make_shared<AffineConverter>()),
    mUnit(""), mBasisFillType(QCPGraph::BasisFillType::ZeroBasis), mIsLegendable(true),
    mStringRepresentation(StringRepresentation::Decimal)
{

//    setAffineConverter(std::make_shared<AffineConverter>(0, 1, 0, 20));
//    auto windowFilter = std::make_shared<WindowFilter> ();
//    windowFilter->setWindowWidth(50);
//    addFilter(windowFilter);
//    auto border = std::make_shared<BorderFilter> ();
//        addFilter(border);
}

GraphModel::~GraphModel()
{

}

QPen GraphModel::getPen() const
{
    return pen;
}

QString GraphModel::getName() const
{
    return name;
}

QVector<double> GraphModel::getXData() const
{
    return xData;
}

QVector<double> GraphModel::getYData() const
{
    return yData;
}

QCPScatterStyle GraphModel::getScatterStyle() const
{
    return scatterStyle;
}

bool GraphModel::isVisible() const
{
    return visibility;
}

QCPGraph::LineStyle GraphModel::getLineInterpolation() const
{
    return lineInterpolation;
}

std::tuple<double, double> GraphModel::getXRange() const
{
    if (xData.size() != 0) {
        auto xMin = findMinInQVector(xData);
        auto xMax = findMaxInQVector(xData);
        return std::tuple<double, double>(xMin, xMax);
    } else {
        return std::tuple<double, double>(-10, 10);
    }
}

std::tuple<double, double> GraphModel::getYRange() const
{
    if (xData.size() != 0) {
        auto yMin = findMinInQVector(yData);
        auto yMax = findMaxInQVector(yData);

//        qDebug() << "in GraphModel::getYRange" << yMin << " -- " << yMax;
        return std::tuple<double, double>(yMin, yMax);
    } else {
        return std::tuple<double, double>(-10, 10);
    }
}

std::tuple<double, double> GraphModel::getYRange(double xRangeBegin, double xRangeEnd) const
{
     if (xData.size() != 0) {
//         auto yMin = findMinInQVectorInRange(yData, xData, xRangeBegin, xRangeEnd);
//         auto yMax = findMaxInQVectorInRange(yData, xData, xRangeBegin, xRangeEnd);


         auto yMin = findMinInQVectorInRange(yData, xData, xRangeBegin, xRangeEnd);
         yMin = std::min({
                          !std::isnan(yMin) ? yMin : std::numeric_limits<double>::max(),
                           getValue(xRangeBegin),
                           getValue(xRangeEnd),
                          }
                          );
         auto yMax = findMaxInQVectorInRange(yData, xData, xRangeBegin, xRangeEnd);
         yMax = std::max({
                           !std::isnan(yMax) ? yMax : std::numeric_limits<double>::lowest(),
                           getValue(xRangeBegin),
                           getValue(xRangeEnd),
                          }
                          );

         return std::tuple<double, double>(yMin, yMax);
     }  else {
         return std::tuple<double, double>(-10, 10);
     }
}

QBrush GraphModel::brush() const
{
    return mBrush;
}

QCPGraph::BasisFillType GraphModel::basisFillType() const
{
    return mBasisFillType;
}

std::shared_ptr<AffineConverter> GraphModel::getAffineConverter() const
{
    return affineConverter;
}

QVector<std::shared_ptr<DataConverter> > GraphModel::getFilterVector() const
{
    return mFilterVector;
}

double GraphModel::getValue(double x) const
{
    if (x < xData[0] || x > xData[xData.size() - 1]) {
        return NAN;
    } else {
//        auto it = std::find_if(xData.begin(), xData.end(), [x](double elem) {return elem >= x;});
        //lower_bound must be faster for ordered sequence than find_if
        auto it = std::lower_bound(xData.begin(), xData.end(), x);


        int right = it - xData.begin();
        int left = right != 0 ? right - 1 : right;
        double div;
        if (left == right)
            div = 0;
        else
            div = (x - xData[left]) / (xData[right] - xData[left]);

        switch (lineInterpolation) {
            case QCPGraph::LineStyle::lsImpulse:
            case QCPGraph::LineStyle::lsNone:
                if (x == xData[left])
                    return yData[left];
                else if (x == xData[right])
                    return yData[right];
                else
                    return 0;
                break;
            case QCPGraph::LineStyle::lsLine:
                return yData[right] * div + yData[left] * (1. - div);
                break;
            case QCPGraph::LineStyle::lsStepLeft:
                if (x == xData[right])
                    return yData[right];
                else
                    return yData[left];
                break;
            case QCPGraph::LineStyle::lsStepRight:
                if (x == xData[left])
                    return yData[left];
                else
                    return yData[right];
                break;
            case QCPGraph::LineStyle::lsStepCenter:
                if (x > 0.5*(xData[left]+xData[right]))
                    return yData[right];
                else
                    return yData[left];
                break;
            default:
                errorExit("Unexpected situation in %s", __FUNCTION__);
                return NAN;
                break;
        }
    }
    return NAN;
}

QString GraphModel::unit() const
{
    return mUnit;
}

bool GraphModel::isLegendable() const
{
    return mIsLegendable;
}

GraphModel::StringRepresentation GraphModel::stringRepresentation() const
{
    return mStringRepresentation;
}



void GraphModel::setOriginData(const QVector<double> &newXOriginData, const QVector<double> &newYOriginData)
{
    xDataOrigin = newXOriginData;
    yDataOrigin = newYOriginData;
    convertDataWithConverter();
    emit dataChanged();
}

void GraphModel::setData(const QVector<double> &newXData, const QVector<double> &newYData)
{
    xData = newXData;
    yData = newYData;
    emit dataChanged();
}

void GraphModel::setPen(const QPen &newPen)
{
    if (pen != newPen) {
        pen = newPen;
        emit appearanceChanged();
    }
}

void GraphModel::setWidth(double newWidth)
{
    if (pen.widthF() != newWidth) {
        pen.setWidthF(newWidth);
        emit appearanceChanged();
    }
}

void GraphModel::setColor(const QColor &newColor)
{
    if (pen.color() != newColor) {
        pen.setColor(newColor);
        emit appearanceChanged();
    }
}

void GraphModel::setPenBrush(const QBrush &newBrush)
{
    if(pen.brush() != newBrush) {
        pen.setBrush(newBrush);
        emit appearanceChanged();
    }
}

void GraphModel::setBrushColor(QColor color)
{
    if (mBrush.color() != color) {
        mBrush.setColor(color);
        emit appearanceChanged();
    }
}

void GraphModel::setBrushStyle(Qt::BrushStyle style)
{
    if (mBrush.style() != style) {
        mBrush.setStyle(style);
        emit appearanceChanged();
    }
}

void GraphModel::setBrushTexture(const QPixmap &texture)
{
    mBrush.setStyle(Qt::TexturePattern);
    mBrush.setTexture(texture);
    emit appearanceChanged();
}

void GraphModel::setStringRepresentation(GraphModel::StringRepresentation representaion)
{
    if (mStringRepresentation != representaion) {
        mStringRepresentation = representaion;
        //at the moment there is no need to emit signals, as plotWidget shouldn't use this property
    }
}

void GraphModel::removeFilter(int index)
{
    if (index >= 0 && index < mFilterVector.size()) {
        mFilterVector.remove(index);
        convertDataWithConverter();
        emit dataChanged();
    }
}

void GraphModel::updateOutputData()
{
    convertDataWithConverter();
    emit dataChanged();
}

void GraphModel::setFillBasis(QCPGraph::BasisFillType newBasisFillType)
{
    if (mBasisFillType != newBasisFillType) {
        mBasisFillType = newBasisFillType;
        emit appearanceChanged();
    }
}

void GraphModel::copyPropertiesFromGraph(const GraphModel *graph)
{
    using namespace global;
    if (graph) {
        QPen pen = graph->getPen();
        if (gIsLineWidthCopyable)
            setWidth(pen.widthF());
        if (gIsLineColorCopyable)
            setColor(pen.color());
        if (gIsLineStyleCopyable)
            setLineStyle(pen.style());
        if (gIsInterpolationCopyable)
            setLineInterpolation(graph->getLineInterpolation());

    //    if (gIsLineColorCopyable)
    //        setPenBrush(pen.brush());
        if (gIsDimensionCopyable)
            setUnit(graph->unit());


        QBrush brush = graph->brush();
        if (gIsBrushColorCopyable)
            setBrushColor(brush.color());
        if (gIsBrushStyleCopyable)
            setBrushStyle(brush.style());
        if (gIsBrushBasisCopyable)
            setFillBasis(graph->basisFillType());

        QCPScatterStyle scatterStyle = graph->getScatterStyle();
        if (gIsScatterShapeCopyable)
            setScatterShape(scatterStyle.shape());
        if (gIsScatterSizeCopyable)
            setScatterSize(scatterStyle.size());
        if (gIsScatterDecimationCopyable)
            setScatterDecimation(scatterStyle.decimation());

        if (gIsTransformationCopyable)
            setAffineConverter(graph->getAffineConverter());

        if (gIsStringRepresentationCopyable)
            setStringRepresentation(graph->stringRepresentation());

        if (gIsFiltersCopyable) {
            mFilterVector.clear();
            for(const auto& filter : graph->mFilterVector) {
                mFilterVector.push_back(filter->clone());
            }
        }
        updateOutputData();

    }


}

void GraphModel::convertDataWithConverter()
{
    if (xDataOrigin.size() != 0 && yDataOrigin.size() != 0) {
        if (*affineConverter == AffineConverter()) {
            xData = xDataOrigin;
            yData = yDataOrigin;
        } else {
            auto newData = (*affineConverter)(xDataOrigin, yDataOrigin);
            xData = std::get<0>(newData);
            yData = std::get<1>(newData);
        }

        foreach (auto filter, mFilterVector) {
            if (filter == nullptr) {
                //pass
            } else {
                auto newData = (*filter)(xData, yData);
                xData = std::get<0>(newData);
                yData = std::get<1>(newData);
            }
        }
    }
}






void GraphModel::setName(const QString &newName)
{
    if (name != newName) {
        name = newName;
        emit appearanceChanged();
    }
}

void GraphModel::setUnit(const QString &newUnit)
{
    if (mUnit != newUnit) {
        mUnit = newUnit;
        emit appearanceChanged();
    }
}

void GraphModel::setAffineConverter(std::shared_ptr<AffineConverter> newConverter)
{
    if (*affineConverter != *newConverter) {
        affineConverter = newConverter;
        qDebug() << "affineConverterChangeis______";
        convertDataWithConverter();
        emit dataChanged();
    }
}


void GraphModel::addFilter(std::shared_ptr<DataConverter> newFilter)
{
    if (newFilter != nullptr) {
        mFilterVector.push_back(newFilter);
        convertDataWithConverter();
        emit dataChanged();
    }
}

void GraphModel::setLineStyle(Qt::PenStyle newPenStyle)
{
    if (pen.style() != newPenStyle) {
        pen.setStyle(newPenStyle);
        emit appearanceChanged();
    }
}

void GraphModel::setVisibility(bool newVisibility)
{
    if (visibility != newVisibility) {
        visibility = newVisibility;
        emit dataChanged();  // to force possible layout reconstruction
    }
}

void GraphModel::setScatterStyle(const QCPScatterStyle &newScatterStyle)
{
   // if (!(scatterStyle == const_cast<QCPScatterStyle &>(newScatterStyle))) {
    if (scatterStyle.pen() != newScatterStyle.pen()
            || scatterStyle.brush() != newScatterStyle.brush()
            || scatterStyle.decimation() != newScatterStyle.decimation()
            || scatterStyle.shape() != newScatterStyle.shape()
            || scatterStyle.size() != newScatterStyle.size()) {
                scatterStyle = newScatterStyle;
                emit appearanceChanged();
    }
        // TODO operator == is not defined for QCPScatterStyle  !!!
    //}
}

void GraphModel::setScatterShape(QCPScatterStyle::ScatterShape newScatterShape)
{
    if (scatterStyle.shape() != newScatterShape) {
        scatterStyle.setShape(newScatterShape);
        emit appearanceChanged();
    }
}

//void GraphModel::setScatterPixmap(const QPixmap &pixmap)
//{
//    //NOTE: There is no equality operator for pixmaps.
////    if (scatterStyle.pixmap() != pixmap) {
//       scatterStyle.setShape(QCPScatterStyle::ScatterShape::ssPixmap);
//       scatterStyle.setPixmap(pixmap);
//        emit appearanceChanged();
//       //    }
//}

void GraphModel::setScatterIcon(const QIcon &icon)
{
    scatterStyle.setShape(QCPScatterStyle::ScatterShape::ssPixmap);
    scatterStyle.setIcon(icon);
    emit appearanceChanged();
}



void GraphModel::setScatterSize(double newScatterSize)
{
    if (scatterStyle.size() != newScatterSize) {
        scatterStyle.setSize(newScatterSize);
//        if (scatterStyle.shape() == QCPScatterStyle::ScatterShape::ssPixmap) {
//            QIcon icon(scatterStyle.pixmap());
//            scatterStyle.setPixmap(icon.pixmap(newScatterSize, newScatterSize));
//            scatterStyle.setPixmap(scatterStyle.pixmap().scaled(newScatterSize, newScatterSize));
//        }
        emit appearanceChanged();
    }
}

void GraphModel::setScatterDecimation(int newDecimation)
{

    if (scatterStyle.decimation() != newDecimation) {
        scatterStyle.setDecimation(newDecimation);
        emit appearanceChanged();
    }
}

void GraphModel::setLineInterpolation(QCPGraph::LineStyle newLineInterpolation)
{
    if (lineInterpolation != newLineInterpolation) {
        lineInterpolation = newLineInterpolation;
        emit appearanceChanged();
    }
}

void GraphModel::setLegendability(bool legendability)
{
    if (mIsLegendable != legendability) {
        mIsLegendable = legendability;
        emit appearanceChanged();
    }
}

void GraphModel::makeMovement(GraphModel::GraphMovement movement)
{
    emit movementRequest(this, movement);
}

void GraphModel::treatDeletingRequest()
{
    emit deletingRequest(this);
}

void GraphModel::setBrush(QBrush brush)
{
    if (mBrush != brush) {
        mBrush = brush;
        emit appearanceChanged();
    }
}


