#include "preferences.h"

Preferences::Preferences()
    :mLegendVisibility(true), mUnitsVisibility(false), mXLabel("time, s"), mLineWidth(1),
      mLineInterpolation(QCPGraph::LineStyle::lsLine), mTimeAxisType(QCPAxis::ltNumber),
      mScatterSize(8), mScatterDecimation(1), mXTickRotation(0), mEventLabelRotation(0),
      mIsLegendFrameVisible(true)
{

}
