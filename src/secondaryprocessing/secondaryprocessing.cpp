#include <QDebug>
#include "secondaryprocessing/datacontainer.h"
#include "secondaryprocessing/secondaryprocessing.h"
#include "core/graphmodel.h"
#include "global/global_definitions.h"

DummyDouble operator-(DummyDouble arg) {
    DummyDouble res(-arg.d);
    return res;
}

DummyDouble operator+(DummyDouble arg) {
    return arg;
}

bool operator==(const DummyDouble& arg1, const DummyDouble& arg2) {
    return arg1.d == arg2.d;
}

bool operator<(const DummyDouble& arg1, const DummyDouble& arg2) {
    return arg1.d < arg2.d;
}

bool operator>(const DummyDouble& arg1, const DummyDouble& arg2) {
    return arg1.d > arg2.d;
}




DummyDouble sqrt(const DummyDouble &arg)
{
    return sqrt(arg.d);
}

DummyDouble pow(const DummyDouble &arg1, const DummyDouble &arg2)
{
    return DummyDouble(pow(arg1.d, arg2.d));
}

DummyDouble max(const DummyDouble &arg1, const DummyDouble &arg2)
{
    return DummyDouble(std::max(arg1.d, arg2.d));
}

DummyDouble min(const DummyDouble &arg1, const  DummyDouble &arg2)
{
    return DummyDouble(std::min(arg1.d, arg2.d));
}



