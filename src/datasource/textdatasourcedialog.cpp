#include "textdatasourcedialog.h"
#include "ui_textdatasourcedialog.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QScrollBar>

static const char sDecimalMark = '.';


TextDataSourceDialog::TextDataSourceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextDataSourceDialog), mFieldSeparator(',')
{
    ui->setupUi(this);
    connect(ui->fieldSeparatorLineEdit, SIGNAL(editingFinished()),
            this, SLOT(treatFieldSeparatorChanging()));

    ui->parameterNamingComboBox->addItem("Use first line", ParameterNaming::UseFirstLine);
    ui->parameterNamingComboBox->addItem("Generate automatically", ParameterNaming::GenerateAutomatically);

}

TextDataSourceDialog::~TextDataSourceDialog()
{
    delete ui;
}

void TextDataSourceDialog::loadFile(const QString &fileName)
{
    mFileName = fileName;
    QFile inputFile(mFileName);

    if (!inputFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return;  //TODO: Add normal treatment

    QTextStream in(&inputFile);
    int nLines = 0;

    QMap<QChar, int> symbolsCount;
    QStringList fileContents;
    while (!in.atEnd() && nLines < 10) {
        QString line = in.readLine();
        fileContents << line;
        ui->textBrowser->append("<font color=\"blue\"><b><i>" + QString::number(++nLines)
                                + ":</i></b></font>&nbsp;&nbsp;&nbsp;" + line);
        for (auto ch : line) {
            if (symbolsCount.find(ch) == symbolsCount.end()) {
                symbolsCount[ch] = 0;
            }
            symbolsCount[ch]++;
        }
    }
    if (!in.atEnd()) {
        ui->textBrowser->append("<font color=\"blue\"><b><i> ...</i></b></font> " );
    }
    ui->textBrowser->verticalScrollBar()->setValue(0);

    //determine separator
    auto isValidSeparator = [] (QChar ch) {
      return !ch.isLetterOrNumber() && !ch.isSpace() && (ch != sDecimalMark)
              && (ch != '-') && (ch != '+');
    };
    int separatorCounter = 0;
    for (const auto& sym : symbolsCount.keys()) {
        if (isValidSeparator(sym)) {
            if (symbolsCount[sym] > separatorCounter) {
                separatorCounter = symbolsCount[sym];
                mFieldSeparator = sym.toLatin1();
            }
        }
    }
    ui->fieldSeparatorLineEdit->setText(QString(mFieldSeparator));

}

char TextDataSourceDialog::fieldSeparator() const
{
    return mFieldSeparator;
}

void TextDataSourceDialog::treatFieldSeparatorChanging()
{
    QString fieldSeparatorString = ui->fieldSeparatorLineEdit->text();
    if (fieldSeparatorString.size() != 0) {
        mFieldSeparator = fieldSeparatorString[0].toLatin1();
    }
    ui->fieldSeparatorLineEdit->setText(QString(mFieldSeparator));

}

void TextDataSourceDialog::treatParameterNamingChanging()
{

}
