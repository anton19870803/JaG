#ifndef GRAPHSTYLE_H
#define GRAPHSTYLE_H

#include "core/qcustomplot.h"

class GraphStyle
{
public:
    GraphStyle();

    void setPen(QPen newPen);
    void setScatterStyle(QCPScatterStyle newStyle);
    void setLineInterpolation(QCPGraph::LineStyle interpolation);
    void setSpecifiedProperty(bool specified);


    bool isSpecified() const { return mIsSpecified;  }
    double lineWidth() const { return mPen.widthF(); }
//    QColor lineColor() const { return mPen.color();  }
    Qt::PenStyle lineStyle() const { return mPen.style(); }
    QPen pen() const { return mPen; }

    QCPScatterStyle::ScatterShape scatterShape() const { return mScatterStyle.shape(); }
    double scatterSize() const { return mScatterStyle.size(); }
    int scatterDecimation() const { return mScatterStyle.decimation(); }
    QCPScatterStyle scatterStyle() const { return mScatterStyle; }

    QCPGraph::LineStyle lineInterpolation() const { return mLineInterpolation; }

private:
    QPen mPen;
    QCPScatterStyle mScatterStyle;
    QCPGraph::LineStyle mLineInterpolation;

    bool mIsSpecified;
};

#endif // GRAPHSTYLE_H
