#ifndef TEXTDATASOURCEDIALOG_H
#define TEXTDATASOURCEDIALOG_H

#include <QDialog>

namespace Ui {
class TextDataSourceDialog;
}

class TextDataSourceDialog : public QDialog
{
    Q_OBJECT

public:
    enum class ParameterNaming {UseFirstLine,
                                GenerateAutomatically
                               };

    explicit TextDataSourceDialog(QWidget *parent = 0);
    ~TextDataSourceDialog();

    void loadFile(const QString &fileName);
    char fieldSeparator() const;
    ParameterNaming parameterNaming() const;

public slots:
    void treatFieldSeparatorChanging();
    void treatParameterNamingChanging();

private:
    Ui::TextDataSourceDialog *ui;
    QString mFileName;
    char mFieldSeparator;
    ParameterNaming mParameterNaming;
};

#endif // TEXTDATASOURCEDIALOG_H
