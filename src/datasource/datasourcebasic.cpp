#include "datasource/datasourcebasic.h"
#include <iostream>
#include <string.h>
#include <fstream>
#include "datasource/csvdatasource.h"
#include "datasource/tabledatasource.h"

#ifndef GPLv3
#include "bin_data_formats/bin_data_formats.h"
#endif

namespace {

bool isTTFile(QString fileName);
bool isBinFile(QString fileName);
bool isCSVFile(QString fileName);
}

DataSource *createDataSourceFromFileName(QString fileName)
{

#ifndef GPLv3
    if (is_bin_format_file(fileName)) {
        return  createBinDataSourceFromFileName(fileName);
    };
#endif


    if (isCSVFile(fileName)){
        return new CSVDataSource(fileName);
    } else {
        return new TableDataSource(fileName);
    }

}






namespace {




bool isCSVFile(QString fileName_)
{
    if (fileName_.toLower().endsWith(".csv")) {
        return true;
    }
    return false;
}

}
