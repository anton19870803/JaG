#include "graphstyle.h"
#include "global/global_definitions.h"
#include <QDebug>


GraphStyle::GraphStyle()
    :mLineInterpolation(QCPGraph::LineStyle::lsLine), mIsSpecified(false)
{

}

void GraphStyle::setPen(QPen newPen)
{
    mPen = newPen;
    mIsSpecified = true;
}

void GraphStyle::setScatterStyle(QCPScatterStyle newStyle)
{
    mScatterStyle = newStyle;
    mIsSpecified = true;
}

void GraphStyle::setLineInterpolation(QCPGraph::LineStyle interpolation)
{
    mLineInterpolation = interpolation;
    mIsSpecified = true;
}

void GraphStyle::setSpecifiedProperty(bool specified)
{
    mIsSpecified = specified;
}
