#include "widgetwrapperwithheader.h"
#include "ui_widgetwrapperwithheader.h"
#include "global/global_definitions.h"

WidgetWrapperWithHeader::WidgetWrapperWithHeader(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidgetWrapperWithHeader)
{
    ui->setupUi(this);
}

WidgetWrapperWithHeader::~WidgetWrapperWithHeader()
{
    delete ui;
}

void WidgetWrapperWithHeader::setHeaderText(const QString &header)
{
    ui->headerLabel->setText(header);
}

void WidgetWrapperWithHeader::setHeader(bool visible)
{
    if (visible) {
        ui->headerLabel->show();
    } else {
        ui->headerLabel->hide();
    }
}

void WidgetWrapperWithHeader::setExpandable(bool)
{

}

void WidgetWrapperWithHeader::addWidget(QWidget *widget)
{
    mWidget = widget;
    ui->verticalLayout->addWidget(widget);
}

void WidgetWrapperWithHeader::paintEvent(QPaintEvent *event)
{
    switch (global::gDefaultColorScheme.scheme()) {
        case global::ColorScheme::Scheme::System:
            ui->headerLabel->setStyleSheet("QLabel { color: white;  background:   rgb(6, 150, 224);    border-top-left-radius: 9px;"
                               "border-top-right-radius: 9px;"
                               "font: bold ;"
                               "font-size: 16px;  }");
            break;
        case global::ColorScheme::Scheme::DarkScheme:
            ui->headerLabel->setStyleSheet("QLabel { color: white;  background:   rgb(6, 150, 224);    border-top-left-radius: 9px;"
                               "border-top-right-radius: 9px;"
                               "color:  #333333;"
                               "font: bold ;"
                               "font-size: 16px;  }");
            break;
        case global::ColorScheme::Scheme::DarkOrangeScheme:
            ui->headerLabel->setStyleSheet("QLabel { color: white;  background:   #ffa02f;    border-top-left-radius: 9px;"
                               "border-top-right-radius: 9px;"
                               "color:  #444444;"
                               "font: bold ;"
                               "font-size: 16px;  }");
            break;
    }

    QWidget::paintEvent(event);
}
