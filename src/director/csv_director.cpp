#include "csv_director.h"
#include <sstream>
#include <vector>
#include "data/data_sample_types.h"
#include <stdlib.h>
#include "basic/minicsv.h"
#include "basic/basic.h"
#include <boost/lexical_cast.hpp>
//#include <QString>
#include <boost/algorithm/string/trim.hpp>
#include <QFile>
#include <cstdlib>

using namespace std;
using boost::algorithm::trim;

CSVDirector::CSVDirector(string fName, char delim)
    : fileName(fName), fIn(fName.c_str()), delimeter(delim), mFileSize(0), mCurrentFilePosition(0)
{

        if (!fIn.is_open()) {
    //            std::cerr << "Can't open file " << fileName << std::endl;
    //            exit(EXIT_FAILURE);
            throwRuntimeError(std::string("Can't open file " + fileName).c_str(), NULL);

        }
        fIn.set_delimiter(delimeter, "##");

        QFile qFile(QString::fromLocal8Bit(fName.c_str()));
        mFileSize = qFile.size();
}



void CSVDirector::process()
{
        processMagic();
        processDataDefinitions();
        processDataSamples();
}

int CSVDirector::progress() const
{
    if (fIn.is_open()) {
//        qDebug() << "asking for progress in TTDirector::progress()";
//        qDebug() << "mCurrentFilePosition =" << mCurrentFilePosition << "mFileSize =" << mFileSize;
        return 100. * (double)(mCurrentFilePosition) / mFileSize;
    } else {
        return 100;
    }
}

//-------------------------------

void CSVDirector::processMagic()
{
    //nothing
}

//-------------------------------

void CSVDirector::processDataDefinitions()
{

   fIn.read_line();
   std::string parameterName = "";
   size_t i = 0;
   while ((parameterName = fIn.get_delimited_str()) != "") {
       shared_ptr<DataAttribute> da = make_shared<DataAttribute>();
       da->setId(i++);
       da->setName(parameterName);
       da->setSize(4);
       da->setDataType(DataAttribute::DataType::FLOAT);
       da->setShowType(DataAttribute::ShowType::DECIMAL);
       da->setMultiplier(1.);
       da->setDimension({1});
       da->setDataSource(fileName);
       dataDefinitions.push_back(da);
   }
   for (auto definition : dataDefinitions)
       builder->addDataDefinition(definition) ;
}
//
//-------------------------------


void CSVDirector::processDataSamples()
{
    double currentTime=0.0;
    int64_t lineNumber = 0;
    std::string line;

    auto old_locale = std::setlocale (LC_NUMERIC, NULL);
    std::setlocale(LC_NUMERIC, "en_US.UTF-8");
    DoAtScopeExit(std::setlocale(LC_NUMERIC, old_locale););

    while ( fIn.read_line()) {
        lineNumber++;
        line = fIn.get_line();
        try {
            for (size_t i = 0; i < dataDefinitions.size(); ++i) {
                std::string dataString = fIn.get_delimited_str();
                //trimming spaces to provide correct treatment
                trim(dataString);
                if (i == 0) {
                    if ( dataString == "") {
    //                    errorExit("Unexpected situation in %s in simple_director", __FUNCTION__);
                        throwRuntimeError("Unexpected situation in %s in simple_director", __FUNCTION__);
                    }
//                    currentTime = boost::lexical_cast<double>( dataString );
                    // atof is faster than boost::lexical_cast
                    currentTime = std::atof(dataString.c_str());

                }

                if (dataString != "") {
//                    double datum = boost::lexical_cast<double>( dataString );
                    // atof is faster than boost::lexical_cast
                    double datum = std::atof(dataString.c_str());



//                    shared_ptr<DataSample> sample = make_shared<FloatDataSample> (
//                                            dataDefinitions[i], currentTime, std::initializer_list<float>{static_cast<float>(datum)}
//                                            );
                    std::unique_ptr<DataSample> sample ( new FloatDataSample (
                                            dataDefinitions[i], currentTime, std::initializer_list<float>{static_cast<float>(datum)}
                                            ));
                    builder->addDataSample(std::move(sample));
                }
            }
        } catch (std::exception &error) {
            if (mIsDataSourceCheckingStrict) {
               throw ;
            } else {
                setCorrect(false);
                QString errorString = QString::fromStdString(error.what()) + ". <b>Error at line: "
                        + QString::number(lineNumber) + ":</b><i> " + QString::fromStdString(line) + "</i>";
                addErrorToErrorsList(errorString);
            }
        }
        if (lineNumber % 100) {
            mCurrentFilePosition = (int64_t)fIn.tellg();
        }
    }

}

