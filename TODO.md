You can contribute to JaG by helping in following areas:
- [ ] Redraw icons in src/resources (particularly awful main JaG icon)
- [ ] Add new functions for data containers to secondary processing. Help of qualified people is needed especially in signal processing (fft ...) 

